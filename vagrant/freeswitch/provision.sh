#!/usr/bin/env bash

export USER=vagrant
export DEBIAN_FRONTEND noninteractive

sudo apt-get update -y
sudo apt-get install apt-transport-https ca-certificates -y
sudo apt-get install git openssl -y



git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-syprunner.git
cd restop-syprunner/docker/images/freeswitch
docker build -t cocoon/freeswitch .

