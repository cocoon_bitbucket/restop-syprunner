a restop platform for B2gas

with
* master
* syprunner
* pjterm
* freeswitch



overview
========

* based on debian + docker + docker-compose
* install this repository on it


configuration
=============
public network bridge dhcp
RAM 2048
login: vagrant/vagrant
sudo password: vagrant


import the vm in virtualbox
===========================

b2gas_default_*.ova



configure the vm
================

launch the vm and log in (vagrant/vagrant)

 note the ip address of the vm
 
    sudo ifconfig   
    
 perform ip configuration
    
    cd ${HOME}/restop-syprunner/docker/platforms/b2gas
    ./configure.py <vm_ip>
    
    



start the restop server ( localhost:5000 )
==========================================
    
    cd restop-syprunner/docker/platforms/b2gas
    ./start.sh


you should have now a running restop server at localhost:5000


test it with restop_client
==========================

    cd restop-syprunner/docker/platforms/b2gas/client/files
    /env/bin/pybot demo.robot



