#!/usr/bin/env python


"""

    customize each file in files directory with the actual ip of the vm


    replace 192.168.99.100 with the actual ip




"""

import os
import logging


source_path = "./files"

root_destination = "/home/vagrant/restop-syprunner"

destinations = {
    "platform.yml" :    "docker/platforms/b2gas/master/platform.yml",
    "vars.xml":         "docker/platforms/b2gas/freeswitch/conf/vars.xml",
    "demo.robot":       "docker/platforms/b2gas/client/files/demo.robot"
}



def substitute_in_content(fp,ip,ip_ref="192.168.99.100"):
    """



    :param file:
    :param ip:
    :return:
    """
    result = ""
    for line in fp:
        if ip_ref in line:
            line= line.replace(ip_ref,ip)
            logging.debug("replace in line: %s" % line.strip())
        result = result + line
    return result


def configure(ip):
    """

    :return:
    """
    for name in os.listdir(source_path):

        fname = os.path.join(source_path,name)

        logging.info("configure file: %s" % fname)

        content = substitute_in_content(open(fname,"r"),ip)

        # find destination
        try:
            destination= os.path.join(root_destination , destinations[name])
        except KeyError:
            logging.warning("no destination for file:%s" % name)

        #print(destination)
        #print(content)

        # overwrite files
        logging.info("overwrite file: %s" % destination)
        try:
            with open(destination,"w") as fout:
                r= fout.write(content)
        except IOError as err:
            logging.error(err)

        continue


def load_images():
    """

        docker pull praekeltfoundation/freeswitch
        docker pull redis:alpine

        cd ${home}/restop-syp^unner/docker/platforms/b2gas/client
        ./build.sh



    :return:
    """




if __name__=="__main__":
    """
    
    
    """
    import argparse

    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='configure restop files')
    parser.add_argument('ip', type=str, help='ip address of the vm')

    args = parser.parse_args()


    ip = args.ip


    logging.info("customize files to actual ip:%s" % ip)

    # configure ip in file platform.yml, vars.xml , demo.robot
    configure(ip)


    load_images()


    logging.info("Done.")