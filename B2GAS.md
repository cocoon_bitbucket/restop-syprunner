Install b2gas syprunner platform



# install


## pre-requisite:

* an internet access because we need access to 
  * docker-hub 
  * pip 
  * apt-get
  * git


## get the repository

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-syprunner.git

## go to b2gas platform description

    cd restop-syprunner/docker/platforms/b2gas
    
## generate local base docker images 
This will take a while...

    ./setup.sh
    
    
## customize the configuration file platform.yml

    vi master/platform.yml
    
localize the parameter  profiles:b2gas:options:--ip-addr
and set it to the ip the target platform ()

the platform.yml is the main configuration file 
* to add devices (sip terminals)
* add profiles
* ...


## configure the test freeswitch server
if you need to play the demo script you will have to configure the freeswitch server

edit file 

    vi restop-syprunner/docker/platforms/b2gas/freeswitch/conf/vars.xml
    
and change every occurence off
* 192.168.99.100
* with the ip of your platform



## start the server 

    cd restop-syprunner/docker/platforms/b2gas
    ./start.sh
    
    

## use the restop robotframework client library to access the platform 

a sample of a robotframework script is at 

    restop-syprunner/docker/platforms/b2gas/client/files/demo.robot
    
    
adapt the ${platform_url}=  http://192.168.99.100:5000/restop/api/v1 
to the ip of the server.





 


