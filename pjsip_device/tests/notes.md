testing pjterm_adapter
======================


prerequisites
-------------


###pjterm server

a running docker container of cocoon/pjterm

    cd restop-syprunner/docker/images/pjterm
    ./run.sh

###freeswitch server

    cd blueswitch/freeswitch_vm
    vagrant up
    vagrant ssh
    cd /usr/local/freeswitch/bin
    sudo ./freeswitch
    
    
