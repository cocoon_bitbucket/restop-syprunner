import time
import redis
from pjsip_device.adapter import  PjtermAdapter, PjtermSessionListener

from pjsip_device.api.pjterm_api import PjtermApi
"""

    low level test , direct call between 2 terminals  
    * 192.168.99.100:5061
    * 192.168.99.100:5062
    
    
    needs a cocoon/pjterm manager and a redis running at at 192.168.99.100 
    
    


"""




REDIS_HOST = "redis"
REDIS_HOST = "192.168.99.100"

ROOT = "pjterm"


#PJTERM_COMMON = " --stdout-refresh=4 --null-audio --no-tcp --no-vad --registrar=sip:192.168.1.51 --realm=* --ip-addr=192.168.99.100 --log-level 5 --app-log-level 5 --password=1234"
PJTERM_COMMON = " --stdout-refresh=4 --null-audio --no-tcp --no-vad --realm=* --ip-addr=192.168.99.100 --log-level 5 --app-log-level 5 --password=1234 --norefersub"

# PJTERM_ALICE = " --local-port 5061 --rtp-port 4000 --id=sip:1001@192.168.99.100 --username=1001"
# PJTERM_BOB = " --local-port 5062 --rtp-port 4008 --id=sip:1002@192.168.99.100 --username=1002"

PJTERM_ALICE = " --local-port 5061 --rtp-port 4000 --id=sip:alice@local --username=alice"
PJTERM_BOB = " --local-port 5062 --rtp-port 4008 --id=sip:bob@local --username=bob"


"""
--local-port 5062 \
--rtp-port 4012 \
--id=sip:1002@192.168.1.51 \
--ip-addr=192.168.99.100 \
--username=1002 \
--password=1234 \
--realm=* \
--registrar=sip:192.168.1.51 \
--log-level 5 --app-log-level 5 \
--null-audio \
--no-tcp \
--auto-answer=200
"""

# IP_ADDRESS= REDIS_HOST


cnx = redis.Redis(host=REDIS_HOST)
cnx.ping()

# create pjterm session listener
session = PjtermSessionListener(cnx, root="pjterm")
#session_exit_channel = session.exit_channel
session.start()

# create session
members = "alice bob"
i = session.api.open_session( members)
#i = cnx.publish("pjterm/-/open", "alice bob")
time.sleep(1)

# start alice terminal
# i = r.publish("pjterm/alice/ctrl", "OPEN --log-level=5 --local-port=5061")
# time.sleep(1)


# create pjtermApi
a1 = PjtermAdapter(cnx, alias="alice", root=ROOT)
alice = PjtermApi("userA", "alice", adapter=a1)

# create pjtermApi
a2 = PjtermAdapter(cnx, alias="bob", root=ROOT)
bob = PjtermApi("userB", "bob", adapter=a2)

# i = alice.open("--null-audio --no-tcp --ip-addr=%s --log-level=5 --local-port=5061" % IP_ADDRESS)
# i = bob.open  ("--null-audio --no-tcp --ip-addr=%s --log-level=5 --local-port=5062" % IP_ADDRESS)



print("Start alice with: %s %s" % (PJTERM_COMMON ,PJTERM_ALICE))
i = alice.open(PJTERM_COMMON + PJTERM_ALICE)
print("Start bob with: %s %s" % (PJTERM_COMMON ,PJTERM_BOB))
i = bob.open(PJTERM_COMMON + PJTERM_BOB)
# i = bob.open( PJTERM_COMMON + PJTERM_BOB + " --auto-answer=200" )


time.sleep(1)

try:

    #r = alice.wait_register()
    #r = bob.wait_register()

    #
    # alice.send(cmd = "d")
    # r = alice.expect(pattern= "Dump complete")
    #
    # bob.send(cmd = "d")
    # r = bob.expect(pattern = "Dump complete")

    # time.sleep(5)


    r = alice.call(destination="sip:bob@192.168.99.100:5062")

    # time.sleep(4)

    r = bob.wait_incoming_call(timeout=20)
    r = bob.answer_call(code=200, timeout=60)

    r = alice.check_call()

    print("wait 5s ...")
    time.sleep(5)

    r = alice.send_dtmf("1234")
    time.sleep(2)
    r = bob.check_dtmf(("1234"))
    r = bob.watch_log(duration=5)

    #r = alice.dump_call_quality_status()
    r = alice.check_call_quality()

    r = alice.hold()
    r = alice.watch_log(duration=5)

    r = bob.watch_log(duration=1)

    print("wait 5s ...")
    time.sleep(5)

    r = alice.unhold()
    r = alice.watch_log(duration=5)

    r = bob.watch_log(duration=1)

    r = alice.hangup()
    print("wait 5s ...")
    time.sleep(5)

    alice.send("d")
    r = alice.expect("Dump complete")

    bob.send("d")
    r = bob.expect("Dump complete")

    print("wait 5s ...")
    time.sleep(5)

    r = alice.shutdown()
    r = bob.shutdown()


except Exception as error:

    print("Interrupted on error: %s" % error)
    pass

finally:
    # close session
    print("close session pjterm/-/close")
    i = cnx.publish("pjterm/-/close", "alice bob")

    print("wait 5s ...")
    time.sleep(5)

    # shutdown session
    print("shutdown session server")
    session.send_kill()

time.sleep(2)

print("Done")




