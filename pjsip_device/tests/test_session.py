import time

import redis

from pjsip_device.session import PjtermSession

# from pjsip_device.adapter.pjterm_session import PjtermSessionListener, PjtermSessionApi
# from pjsip_device.adapter.pjterm_adapter import PjtermAdapter
# from pjsip_device.api import  PjtermLevelOne

"""

    some higher level tests with pjsip_device.session , direct call between 2 terminals  
    * 192.168.99.100:5061
    * 192.168.99.100:5062
    
    
     needs a cocoon/pjterm manager and a redis running at at 192.168.99.100 
    
"""



# REDIS_HOST = "redis"
REDIS_HOST = "192.168.99.100"
#REDIS_HOST = "127.0.0.1"

ROOT = "pjterm"


PJTERM_COMMON = " --stdout-refresh=4 --null-audio --no-tcp --no-vad --realm=* --ip-addr=192.168.99.100 --log-level 5 --app-log-level 5 --password=1234"
PJTERM_ALICE = " --local-port 5061 --rtp-port 4000  --username=1001"
PJTERM_BOB = " --local-port 5062 --rtp-port 4008 --username=1002"




def test_simple_call():
    """


    :return:
    """
    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()
    cnx.flushall()

    # create the global session listener
    PjtermSession.create_session_listener(cnx)

    # open a session
    try:

        # open session and start terminals
        s = PjtermSession(cnx)
        s.open_session(members="alice bob")
        time.sleep(3)
        r1= s.start_terminal("userA","alice",options= PJTERM_COMMON + PJTERM_ALICE)
        r2= s.start_terminal("userB","bob",options=PJTERM_COMMON + PJTERM_BOB)


        # alice send a dump command
        #s.alice.send("d")
        s = PjtermSession(cnx)
        alice= s.get_terminal( "alice")
        #a1 = PjtermAdapter(cnx, alias="alice", root=ROOT)
        #alice = PjtermLevelOne("userA", "alice", adapter=a1)
        alice.send("d")
        alice.expect("Dump complete")


        # Bob send a Dump command
        s = PjtermSession(cnx)
        bob = s.get_terminal("bob")


        bob.send("d")
        bob.expect("Dump complete")

        time.sleep(5)


        r=alice.call("sip:1002@192.168.99.100:5062")

        #time.sleep(4)

        r= bob.wait_incoming_call(timeout=20)
        r= bob.answer_call(code=200,timeout=60)

        r= alice.check_call()

        r = bob.get_last_received_sip_request(sip_method="INVITE")

        print("wait 5s ...")
        time.sleep(5)

        r = alice.hangup()
        r = bob.wait_hangup()
        time.sleep(2)

        r = bob.get_last_received_sip_request(sip_method="BYE")



    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        s = PjtermSession(cnx)
        s.close_session()



        # shutdown session listener
        print("shutdown session server")
        PjtermSession.shutdown_session_listener(cnx)

    time.sleep(1)
    return


def test_check_incoming_call():
    """


    :return:
    """
    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()
    cnx.flushall()

    # create the global session listener
    PjtermSession.create_session_listener(cnx)

    # open a session
    try:

        # open session and start terminals
        s = PjtermSession(cnx)
        s.open_session(members="alice bob")
        time.sleep(3)
        r1= s.start_terminal("userA","alice",options= PJTERM_COMMON + PJTERM_ALICE)
        r2= s.start_terminal("userB","bob",options=PJTERM_COMMON + PJTERM_BOB)


        # alice send a dump command
        #s.alice.send("d")
        s = PjtermSession(cnx)
        alice= s.get_terminal( "alice")
        #a1 = PjtermAdapter(cnx, alias="alice", root=ROOT)
        #alice = PjtermLevelOne("userA", "alice", adapter=a1)
        alice.send("d")
        alice.expect("Dump complete")


        # Bob send a Dump command
        s = PjtermSession(cnx)
        bob = s.get_terminal("bob")

        time.sleep(2)


        r=alice.call("sip:1002@192.168.99.100:5062")


        r= bob.wait_incoming_call(timeout=20)
        r= bob.answer_call(code=200,timeout=60)

        r= alice.check_call()

        r = bob.check_incoming_call_from("")


        print("wait 5s ...")
        time.sleep(5)

        r = alice.hangup()
        r = bob.wait_hangup()
        time.sleep(2)


    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        s = PjtermSession(cnx)
        s.close_session()



        # shutdown session listener
        print("shutdown session server")
        PjtermSession.shutdown_session_listener(cnx)

    time.sleep(1)
    return






def test_call_dtmf():
    """

    :return:
    """
    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()
    cnx.flushall()
    # create the global session listener
    PjtermSession.create_session_listener(cnx)

    # open a session



    try:

        # open session and start terminals
        s = PjtermSession(cnx)
        s.open_session(members="alice bob")
        time.sleep(3)
        r1 = s.start_terminal("userA", "alice", options=PJTERM_COMMON + PJTERM_ALICE)
        r2 = s.start_terminal("userB", "bob", options=PJTERM_COMMON + PJTERM_BOB)

        # alice send a dump command
        # s.alice.send("d")
        s = PjtermSession(cnx)
        alice = s.get_terminal("alice")
        # a1 = PjtermAdapter(cnx, alias="alice", root=ROOT)
        # alice = PjtermLevelOne("userA", "alice", adapter=a1)
        # alice.send("d")
        # alice.expect("Dump complete")

        # Bob send a Dump command
        s = PjtermSession(cnx)
        bob = s.get_terminal("bob")

        # bob.send("d")
        # bob.expect("Dump complete")

        time.sleep(5)

        r = alice.call("sip:1002@192.168.99.100:5062")

        # time.sleep(4)

        r = bob.wait_incoming_call(timeout=20)
        r = bob.answer_call(code=200, timeout=60)

        r = alice.check_call()

        r = bob.get_last_received_sip_request(sip_method="INVITE")

        print("wait 5s ...")
        time.sleep(5)


        r= alice.send_dtmf("1234")
        time.sleep(2)
        r= bob.check_dtmf(("1234"))
        r = bob.watch_log(duration=5)

        r= alice.check_call_quality(channel="TX")
        r= bob.check_call_quality(channel="RX")



        # r= alice.dump_call_quality_status()
        # r= alice.check_call_quality()


        r= alice.hold()
        r= alice.watch_log(duration=5)

        r= bob.watch_log(duration=1)

        print("wait 5s ...")
        time.sleep(5)

        r= alice.unhold()
        r= alice.watch_log(duration=5)

        r= bob.watch_log(duration=1)



        # r=alice.hangup()
        # print("wait 5s ...")
        # time.sleep(5)
        #
        #
        # alice.send("d")
        # r= alice.expect("Dump complete")
        #
        # bob.send("d")
        # r = bob.expect("Dump complete")
        #
        # print("wait 5s ...")
        # time.sleep(5)


        # r= s.alice._stop()
        # r= s.bob._stop()


        r = alice.hangup()
        r = bob.wait_hangup()
        time.sleep(2)

        r = bob.get_last_received_sip_request(sip_method="BYE")


    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        s = PjtermSession(cnx)
        s.close_session()

        # shutdown session listener
        print("shutdown session server")
        PjtermSession.shutdown_session_listener(cnx)

    time.sleep(2)



def test_send_arbitrary():
    """


    :return:
    """
    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()
    cnx.flushall()

    # create the global session listener
    PjtermSession.create_session_listener(cnx)

    # open a session
    try:

        # open session and start terminals
        s = PjtermSession(cnx)
        s.open_session(members="alice bob")
        time.sleep(3)
        r1= s.start_terminal("userA","alice",options= PJTERM_COMMON + PJTERM_ALICE)
        r2= s.start_terminal("userB","bob",options=PJTERM_COMMON + PJTERM_BOB)

        # Alice send a Dump command
        s = PjtermSession(cnx)
        alice= s.get_terminal( "alice")
        alice.send("d")
        alice.expect("Dump complete")


        # Bob send a Dump command
        s = PjtermSession(cnx)
        bob = s.get_terminal("bob")
        bob.send("d")
        bob.expect("Dump complete")

        time.sleep(5)


        # alice call bob
        r=alice.call("sip:1002@192.168.1.51")
        r= bob.wait_incoming_call(timeout=20)
        r= bob.answer_call(code=200,timeout=60)
        r= alice.check_call()

        r = bob.get_last_received_sip_request(sip_method="INVITE")



        # alice send arbitrary message
        r= alice.send("S")
        #r= alice.watch_log(duration=1)
        # send method = INFO
        r= alice.send("INFO")
        #r = alice.watch_log(duration=1)
        # send url to bob  or 0 for current dialog
        r = alice.send("0")
        #r = alice.send("sip:1002@192.168.1.51")
        r = alice.watch_log(duration=1)
        r = alice.send("my arbitrary message")



        print("wait 5s ...")
        time.sleep(5)

        r = alice.hangup()
        r = bob.wait_hangup()
        time.sleep(2)

        r = bob.get_last_received_sip_request(sip_method="BYE")



    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        s = PjtermSession(cnx)
        s.close_session()



        # shutdown session listener
        print("shutdown session server")
        PjtermSession.shutdown_session_listener(cnx)

    time.sleep(1)
    return





if __name__== "__main__":
    """



    """

    test_check_incoming_call()

    test_simple_call()
    test_call_dtmf()



    #test_send_arbitrary()


    print("Done")



