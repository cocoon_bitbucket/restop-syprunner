"""




"""
import os


def get_pjterm_path(platform=None):
    """

    :return: the full name of  syprunner given the current platform

        can be syprunner-linux-x86_64 , syprunner-macosx-10.8-x86_64 ..
        or syprunner-fake

    """
    from distutils.util import get_platform

    if not platform:
        platform = get_platform()

    if platform.startswith('linux-'):
        # same exe for all linux platform
        platform= "linux-x86_64"

    pjterm= "syprunner-%s" % platform

    if platform == 'fake':
        # fake terminal for test purpose
        local= os.path.join( os.path.dirname(__file__),"bin",pjterm)
        pjpath= os.path.abspath(local)

    else:
        pjpath= pjterm

    return pjpath
