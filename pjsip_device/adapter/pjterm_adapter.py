
"""

    an adapter for PjtermApi


    provides

        adapter.readline()
        adapter.send()

        adapter.log.debug()
        adapter.log.info()
        adapter.log.error()
        adapter.log.warn()



# ./pjterm --null-audio --no-tcp --ip-addr=192.168.99.100 --log-level=5 --local-port=5062 --id sip:bob@192.168.99:5062 --auto-answer=200
# ./pjterm --null-audio --no-tcp --ip-addr=192.168.99.100 --log-level=5 --local-port=5061

# sip:192.168.99.100:5062
# sip:bob@192.168.99.100:5062
# sip:pjterm:5062
# sip:localhost:5062
#sip:127.0.0.1:5062
# sip:172.17.0.2:5062

./pjterm --null-audio --no-tcp --local-port=5062  --auto-answer=200
./pjterm --null-audio --no-tcp --local-port=5061



"""
import threading
import json
from pprint import pprint
import redis


class Logger(object):
    """
        a simple logger to redis   <root>:<alias>:stdlog   eg  pjterm:alias:stdlog

    """
    def __init__(self,redis_db,alias,root="pjterm"):
        """

        :param redis:
        :param db:
        :param alias:
        """
        self.redis_db = redis_db
        self.alias= alias
        self.root=root
        self.queue_name= "%s:%s:stdlog" %(self.root,alias)

    def _log(self,message):
        """

        :param message:
        :return:
        """
        self.redis_db.rpush(self.queue_name,message)
    debug= _log
    info= _log
    error= _log


def get_pjterm_observer(redis_db, alias , root="pjterm"):
    """
        objserver for redis channel pjterm/<alias>/* channels

    :param r:
    :return:
    """
    pubsub = redis_db.pubsub()
    pubsub.psubscribe("%s/%s/*" % (root,alias))

    def _observer():
        """

        :return:
        """
        for message in pubsub.listen():
            #pprint(message)
            channel= message["channel"]
            if "/" in channel:
                channel= channel.split("/")[-1]
            data= message["data"]
            if channel != "stdout":
                print(" (--%s--): %s" % (channel,data ))
            else:
                data = data.rstrip("\r\n")
                print("%s\n" % data)


    observer = threading.Thread(target=_observer, args=())
    observer.daemon = True
    return observer



class PjtermAdapter(object):
    """

        publish message to pjterm/<alias>/stdin, pjterm/alias/ctrl
        read message from pjterm/<alias>/stdout
        push log to pjterm/<alias>/stdlog


        save_state/get_state  to pjterm/<alias>/-state- key (json )

        read_log , log_len


    """
    def __init__(self,redis_db,alias,root="pjterm"):
        """

        :param redis_db:
        :param alias:
        :param root:
        """
        self.redis_db=redis_db
        self.alias=alias
        self.root= root

        self.pubsub=self.redis_db.pubsub()

        self.stdin_channel= "%s/%s/stdin" % (self.root,self.alias)
        self.ctrl_channel= "%s/%s/ctrl" % (self.root,self.alias)
        self.stdout_queue_name= "%s:%s:stdout" % (self.root,self.alias)
        self.stdlog_queue_name = "%s:%s:stdlog" % (self.root, self.alias)

        self.log=Logger(self.redis_db,alias,root)

    def channel(self,name):
        """

        :param name: string stdin , ctrl
        :return:
        """
        return "%s/%s/%s" % (self.root, self.alias,name)

    def queue(self,name):
        """

        :param name:
        :return:
        """
        return "%s:%s:%s" % (self.root,self.alias,name)



    def publish(self,channel,message):
        """

        :param channel: string  stdin,ctrl
        :param message:
        :return:
        """
        self.redis_db.publish(self.channel(channel), message)


    def send(self,command):
        """
            send command to redis channel pjterm/alias/stdin

        :param command:
        :return:
        """
        self.publish("stdin",command)

    def readline(self):
        """"
            readline from redis queue  pjterm:alias:stdout
        """
        if self.redis_db.llen(self.queue("stdout")):
            # there is a line
            line= self.redis_db.lpop(self.queue("stdout"))
            return line
        else:
            # no line available
            return None


    def save_state(self,state=None):
        """

        :param state:
        :return:
        """
        state= state or {}
        try:
            self.redis_db.set(self.queue("-state-"),json.dumps(state))
            self.log.debug("state saved")
        except Exception as error:
            self.log.error("cannot save state error:%s" % error)

    def get_state(self):
        """

        :return:
        """
        try:
            state= self.redis_db.get(self.queue("-state-"))
            state= json.loads(state)
            self.log.debug("get state")
        except Exception as error:
            self.log.debug("no state available: %s " % error)
            state= {}
        return state


    def log_len(self):
        """

        :return:
        """
        return self.redis_db.llen(self.queue("stdlog"))

    def read_log(self,offset=0):
        """
            read the  log from

        :param offset:
        :return:
        """
        log_queue= self.queue("stdlog")
        l= self.redis_db.llen(log_queue)
        logs= self.redis_db.lrange(log_queue,offset,l)
        return logs



if __name__== "__main__":
    """



    """
    import time
    from pjterm_api import PjtermApi
    from pjterm_session import PjtermSessionListener

    REDIS_HOST = "redis"
    REDIS_HOST = "192.168.99.100"
    REDIS_HOST = "192.168.1.21"

    ROOT = "pjterm"

    REGISTRAR_IP=  "192.168.1.51"

    PJTERM_COMMON= " --stdout-refresh=4 --null-audio --no-tcp --no-vad --registrar=sip:192.168.1.51 --realm=* --ip-addr=192.168.99.100 --log-level 5 --app-log-level 5 --password=1234"
    PJTERM_ALICE= " --local-port 5061 --rtp-port 4000 --id=sip:1001@192.168.1.51 --username=1001"
    PJTERM_BOB= " --local-port 5062 --rtp-port 4008 --id=sip:1002@192.168.1.51 --username=1002"

    """
    --local-port 5062 \
  --rtp-port 4012 \
  --id=sip:1002@192.168.1.51 \
  --ip-addr=192.168.99.100 \
  --username=1002 \
  --password=1234 \
  --realm=* \
  --registrar=sip:192.168.1.51 \
  --log-level 5 --app-log-level 5 \
  --null-audio \
  --no-tcp \
  --auto-answer=200
    """

    #IP_ADDRESS= REDIS_HOST


    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()

    # create pjterm session listener
    session = PjtermSessionListener(cnx, root="pjterm")
    session_exit_channel = session.exit_channel
    session.start()


    # create session
    i = cnx.publish("pjterm/-/open", "alice bob")
    time.sleep(1)

    # start alice terminal
    #i = r.publish("pjterm/alice/ctrl", "OPEN --log-level=5 --local-port=5061")
    #time.sleep(1)

    dummy_state= {"hello":"there"}

    # create pjtermApi
    a1= PjtermAdapter(cnx,alias="alice",root=ROOT)

    a1.save_state(dummy_state)
    r= a1.get_state()




    alice= PjtermApi("userA","alice",adapter=a1)

    # create pjtermApi
    a2= PjtermAdapter(cnx,alias="bob",root=ROOT)
    bob= PjtermApi("userB","bob",adapter=a2)



    #i = alice.open("--null-audio --no-tcp --ip-addr=%s --log-level=5 --local-port=5061" % IP_ADDRESS)
    #i = bob.open  ("--null-audio --no-tcp --ip-addr=%s --log-level=5 --local-port=5062" % IP_ADDRESS)

    i = alice.open( PJTERM_COMMON + PJTERM_ALICE )
    i = bob.open(PJTERM_COMMON + PJTERM_BOB )
    #i = bob.open( PJTERM_COMMON + PJTERM_BOB + " --auto-answer=200" )


    try:

        r= alice.wait_register()

        r= bob.wait_register()



        time.sleep(3)

        alice.send("d")
        r= alice.expect("Dump complete")

        bob.send("d")
        r = bob.expect("Dump complete")

        #time.sleep(5)


        r=alice.call("sip:1002@192.168.1.51")

        #time.sleep(4)

        r= bob.wait_incoming_call(timeout=20)
        r= bob.answer_call(code=200,timeout=60)

        r=alice.check_call()

        print("wait 5s ...")
        time.sleep(5)

        r= alice.send_dtmf("1234")
        time.sleep(2)
        r= bob.check_dtmf(("1234"))
        r = bob.watch_log(duration=5)


        r= alice.dump_call_quality_status()
        r= alice.check_call_quality()


        r= alice.hold()
        r= alice.watch_log(duration=5)

        r= bob.watch_log(duration=1)

        print("wait 5s ...")
        time.sleep(5)

        r= alice.unhold()
        r= alice.watch_log(duration=5)

        r= bob.watch_log(duration=1)



        r=alice.hangup()
        print("wait 5s ...")
        time.sleep(5)


        alice.send("d")
        r= alice.expect("Dump complete")

        bob.send("d")
        r = bob.expect("Dump complete")

        print("wait 5s ...")
        time.sleep(5)


        r= alice.shutdown()
        r= bob.shutdown()


    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        print("close session pjterm/-/close")
        i = cnx.publish("pjterm/-/close", "alice bob")

        print("wait 5s ...")
        time.sleep(5)


        # shutdown session
        print("shutdown session server")
        session.send_kill()


    time.sleep(2)


    print("Done")




