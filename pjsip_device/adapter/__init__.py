"""


    an adapter for a remote jterm server ( via redis channels )


    usage:
        see pjterm_adapter




"""

from pjterm_adapter import PjtermAdapter
from pjterm_session import PjtermSessionApi, PjtermSessionListener