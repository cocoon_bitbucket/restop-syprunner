
"""

    a simple api to pjsip_device.adapter.pjterm_adapter

"""

from pjterm_api import PjtermApi


class SimplePjterm(PjtermApi):
    """
        proxy to syprunner

        _start: launch a syprunner process and communicate with it via redis queues

        stop: stop the thread and syprunner process

        send:  send input to syprunner stdin queue
        read : read syprunner output via stdout queue

    """
    collection= 'pjterm_agents'

    def _start(self,options,register=True,**kwarg):
        """

        :param item:
        :param kwarg:
        :return:
        """
        r= self.open(options)

        # check process has started
        if not "--registrar" in options:
            register = False

        r= self._check_process(check_registration=register)

        return r


    def _stop(self,**kwargs):
        """

            shutdown a terminal:  send q,

        :param item:
        :param kwargs:
        :return:
        """
        r= self.shutdown()
        return r

