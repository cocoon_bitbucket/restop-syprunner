import time
import redis

from pjsip_device.adapter.pjterm_session import PjtermSessionListener, PjtermSessionApi
from pjsip_device.adapter.pjterm_adapter import PjtermAdapter

from pjsip_device.api import  PjtermLevelOne

# REDIS_HOST = "redis"
# REDIS_HOST = "192.168.99.100"
REDIS_HOST = "192.168.1.19"

ROOT = "pjterm"

REGISTRAR_IP = "192.168.1.51"

PJTERM_COMMON = " --stdout-refresh=4 --null-audio --no-tcp --no-vad --registrar=sip:192.168.1.51 --realm=* --ip-addr=192.168.99.100 --log-level 5 --app-log-level 5 --password=1234"
PJTERM_ALICE = " --local-port 5061 --rtp-port 4000 --id=sip:1001@192.168.1.51 --username=1001"
PJTERM_BOB = " --local-port 5062 --rtp-port 4008 --id=sip:1002@192.168.1.51 --username=1002"




class DummySession(object):
    """
        simulate a session

    """
    @classmethod
    def create_session_listener(self,cnx):
        """
        :param members:
        :return:
        """
        # create pjterm session listener
        session = PjtermSessionListener(cnx, root="pjterm")
        session.start()


    @classmethod
    def shutdown_session_listener(self,cnx):
        """

        :return:
        """
        #session = PjtermSessionListener(cnx, root="pjterm")
        session = PjtermSessionApi(cnx, root="pjterm")
        session.send_kill()

    def __init__(self,cnx,members="alice bob"):
        """

        :param cnx:
        :param members:
        """
        self.cnx= cnx
        self.alice= None
        self.bob= None



    def open_session(self,members= "alice bob"):
        """

        :param memebers:
        :return:
        """
        # get a session handler
        session = PjtermSessionApi(self.cnx, root="pjterm")
        # send clear
        session.send_clear()

        # create remote session
        i= session.open_session(members)
        time.sleep(1)

        # start alice terminal
        # i = r.publish("pjterm/alice/ctrl", "OPEN --log-level=5 --local-port=5061")
        # time.sleep(1)


        # create pjtermApi
        a1 = PjtermAdapter(self.cnx, alias="alice", root=ROOT)
        self.alice = PjtermLevelOne("userA", "alice", adapter=a1)
        i=self.alice._start(PJTERM_COMMON + PJTERM_ALICE)

        # create pjtermApi
        a2 = PjtermAdapter(self.cnx, alias="bob", root=ROOT)
        self.bob = PjtermLevelOne("userB", "bob", adapter=a2)
        i = self.bob._start(PJTERM_COMMON + PJTERM_BOB)



    def close_session(self):
        """

        :param members:
        :return:
        """
        # get a session handler
        #session = PjtermSessionListener(cnx, root="pjterm")

        print("close session pjterm/-/close")
        #i = self.cnx.publish("pjterm/-/close", "alice bob")

        session = PjtermSessionApi(self.cnx, root="pjterm")
        i = session.close_session()



        print("wait 5s ...")
        time.sleep(5)


if __name__== "__main__":
    """



    """


    #IP_ADDRESS= REDIS_HOST


    cnx = redis.Redis(host=REDIS_HOST)
    cnx.ping()
    cnx.flushdb()

    # create the global session listener
    DummySession.create_session_listener(cnx)


    s= DummySession(cnx,members="alice bob")
    # open a session



    try:

        s.open_session(members="alice bob")


        time.sleep(3)


        # send a dump command
        #s.alice.send("d")
        a1 = PjtermAdapter(cnx, alias="alice", root=ROOT)
        alice = PjtermLevelOne("userA", "alice", adapter=a1)
        alice.send("d")
        alice.expect("Dump complete")


        # expect dump result
        #r= s.alice.expect("Dump complete")
        a2 = PjtermAdapter(cnx, alias="bob", root=ROOT)
        bob = PjtermLevelOne("userB", "bob", adapter=a2)
        bob.send("d")
        r = bob.expect("Dump complete")

        time.sleep(5)


        r=alice.call("sip:1002@192.168.1.51")

        #time.sleep(4)

        r= bob.wait_incoming_call(timeout=20)
        r= bob.answer_call(code=200,timeout=60)

        r= alice.check_call()

        r= bob.get_last_received_request(sip_method="INVITE")


        print("wait 5s ...")
        time.sleep(5)

        #
        # r= bob.get_last_received_request(sip_method="INVITE")
        #
        #
        #
        # r= alice.send_dtmf("1234")
        # time.sleep(2)
        # r= bob.check_dtmf(("1234"))
        # r = bob.watch_log(duration=5)
        #
        # r= alice.check_call_quality(channel="TX")
        # r= bob.check_call_quality(channel="RX")


        #
        # r= alice.dump_call_quality_status()
        # r= alice.check_call_quality()
        #
        #
        # r= alice.hold()
        # r= alice.watch_log(duration=5)
        #
        # r= bob.watch_log(duration=1)
        #
        # print("wait 5s ...")
        # time.sleep(5)
        #
        # r= alice.unhold()
        # r= alice.watch_log(duration=5)
        #
        # r= bob.watch_log(duration=1)



        # r=alice.hangup()
        # print("wait 5s ...")
        # time.sleep(5)
        #
        #
        # alice.send("d")
        # r= alice.expect("Dump complete")
        #
        # bob.send("d")
        # r = bob.expect("Dump complete")
        #
        # print("wait 5s ...")
        # time.sleep(5)


        #r= s.alice._stop()
        #r= s.bob._stop()

        r= alice.hangup()
        r= bob.wait_hangup()
        time.sleep(2)

        r = bob.get_last_received_request(sip_method="BYE")


    except Exception as error:

        print("Interrupted on error: %s" % error)
        pass

    finally:
        # close session
        s.close_session()



        # shutdown session listener
        print("shutdown session server")
        DummySession.shutdown_session_listener(cnx)



    time.sleep(2)


    print("Done")



