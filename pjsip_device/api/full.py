import time
# # restop imports
#
# from restop.application import ApplicationError
#
# #from pjsip_device.session_server.ptf_manager2 import SypConfiguration
#
# # devices import
# from syprunner.models import SypPlatform,SypUser
# from pjsip_device.server import get_pjterm_path
#
# from restop_platform.resources import AgentResource
# from pjsip_device.proxy.agent import PjsipAgent,TestError,TimeoutError
from pjsip_device.proxy.syprfc.sip_parser import Message
#
# from pjsip_device.server.restop_agents import    Resource_pjterm_agents
#
# from models import DEVICE_NAME
# from agent import Agent
#
#from simple import SimplePjterm
from pjterm_api import PjtermApi

#class PjtermLevelOne(SimplePjterm):
class PjtermLevelOne(PjtermApi):
    """

            a pjterm enhanced with
                * check_incoming_call_from(self,display_name,**kwargs)


    """

    @classmethod
    def _check_caller(cls, display=None, number=None):
        """
            check call is emitted by the given user

            @user: str, the agent: Alice, Bob ,
            @display: str the display name of the caller (eg "Alice Alice")
            @number: str : the number of the caller

        """

        def validate(call):
            """

                @call: a sip INVITE object (a syprfc.Message instance)

                getting the From:Header
                    from_header= call['from']

                available attributes:
                    from_header.tag
                    from_header.value.uri.user
                    from_header.value.uri.host
                    from_header.value.displayable  # warning length limited to a 25 eg : 'Q11A-EasyValid Q11A-Ea...'

                    from_header.value.displayName


            """
            from_header = call['from']
            if display:
                # check display name
                received = from_header.value.displayName
                if not received == display:
                    # display name check has failed
                    raise ValueError('bad display name, expected: "%s" , received: "%s"' % (display, received))
            if number:
                raise NotImplementedError('check number not implemented yet')
            return True

        return validate


    def _check_incoming_call(self,validation,**kwargs):
        """

        :param validation: function , like the one returned by @check_caller
        :param kwargs:
        :return:
        """

        # get the last INVITE received
        #last_invite_request = self.execute(role,'get_last_received_request',operation='INVITE')
        last_invite_request = self.get_last_received_sip_request(sip_method="INVITE")
        # convert lines to a sip Message object
        message = Message(last_invite_request)
        # perform validation
        r = validation(message)
        return r


    #
    # local methods
    #

    def get_last_received_sip_request(self, sip_method="INVITE"):
        """
             return the last received sip request message for the specified operation

        :param role: Alice, Bob
        :param operation: INVITE ...
        :return: str  lines from sip message
        """
        # get the last INVITE received
        # last_request = self.execute(role,'get_last_received_request',operation=operation)

        #last_request = self._proxy_operation(item, 'get_last_received_request', sip_method=sip_method)
        #content = last_request['message']
        last_request = self.get_last_received_request( sip_method=sip_method)
        assert last_request['ready'] == True
        # convert lines to a sip Message object
        # message = Message("".join(last_request['lines']))
        # return message
        return "".join(last_request['lines'])



    def check_incoming_call_from(self,display_name,**kwargs):
        """
            checks display name on FROM header of the incoming INVITE message

            *@user*: str , the user receiving the call (eg Bob)

            *@display_name*: the display name to compare with ( or unkwown )

        :param user:
        :param display_name:
        :return:
        """
        # input= self.request.json
        # display_name= input['display_name']
        #
        # user_data= self.backend.item_get(self.collection,item)
        # user= user_data['alias']


        validation = self._check_caller(display=display_name)

        r= self._check_incoming_call(validation)

        return r



    # def check_incoming_call(self,validation,**kwargs):
    #     """
    #
    #
    #     """
    #
    #     # get the last INVITE received
    #     #last_invite_request = self.execute(role,'get_last_received_request',operation='INVITE')
    #     last_invite_request = self.get_last_received_sip_request(sip_method="INVITE")
    #     # convert lines to a sip Message object
    #     message = Message(last_invite_request)
    #     # perform validation
    #     r = validation(message)
    #     return r
    #
    #
    # #
    # # published operations
    # #
    #
    #
    # def op_call_user(self,item,**kwargs):
    #     """
    #
    #     /$agent_url/?/call_user  userX , format='universal
    #
    #     :param userA:
    #     :param userX:
    #     :param format:
    #     :return:
    #     """
    #     input= self.request.json
    #
    #     userX= input['userX']
    #     format= input.get('format','universal')
    #
    #
    #     sip_address= self.SessionClass.get_sip_address(user=userX,format=format)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #
    #
    # def op_call_feature_access_code(self,item,**kwargs):
    #     """
    #
    #        call Application server with a feature access code
    #
    #         eg  -CFA , -CFU
    #
    #     """
    #     input= self.request.json
    #
    #     call_feature_access_code= input['call_feature_access_code']
    #     userX= input.get(input['userX'],None)
    #     format= input.get('format',None)
    #
    #     sip_address= self.SessionClass.get_sip_address(user=userX,format=format,fac=call_feature_access_code)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #
    #
    #
    # def op_call_destination(self,item,**kwargs):
    #     """
    #
    #     :param user:
    #     :param destination:
    #     :return:
    #     """
    #     input= self.request.json
    #     destination= input['destination']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #
    #     resolved_destination= ptf.destination(destination)
    #     sip_address= resolved_destination
    #     #sip_address= self.SessionClass.get_sip_address(resolved_destination)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #sip_address= self.ptf.sip_address_for(resolved_destination)
    #     #print "pilot: user %s call destination %s  (%s)" % (user,destination,sip_address)
    #     #return self._call(user,sip_address=sip_address)
    #
    #
    # def op_call_sd(self,item,**kwargs):
    #     """
    #          user call with direct sd8 key sd ( 1..8 )
    #
    #          @user: eg Alice Bob
    #          @sd: the sd8 direct key on the terminal ( eg 1 , 2  ... 8)
    #
    #     :param user:
    #     :param sd:
    #     :return:
    #     """
    #     input= self.request.json
    #     sd= input['sd']
    #
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     sip_address= ptf.sip_address_for(sd)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #     #print "pilot: user %s call direct sd8 key %s  (%s)" % (user,sd,sip_address)
    #     #
    #     #return self._call(user,sip_address=sip_address)
    #
    #
    # def op_transfer_to_user(self,item,**kwargs):
    #     """
    #
    #        Unatented transfer call
    #
    #         *userA* transfer call to *userX* with *format* (universal,national ...)
    #
    #     :param userA:
    #     :param userX:
    #     :param format:
    #     :return:
    #     """
    #     # compute sip address for userX with format
    #     input= self.request.json
    #     userX= input['userX']
    #     format= input.get('format','universal')
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     sip_address = ptf.user(userX).sip_address_to_user(format)
    #
    #     #print  "pilot: user %s transfer call to user %s with format %s  (%s)" % ( userA, userX, format ,sip_address)
    #     #return self._transfer(userA,sip_address=sip_address)
    #
    #     return self._proxy_operation(item,'transfer',destination= sip_address)
    #
    # def op_transfer_to_destination(self,item,**kwargs):
    #     """
    #        transfer call (unatended) to a predefined destination (defined in platform.json)
    #
    #         *@user*: eg Alice , Bob
    #
    #         *@destination*:  the literal name of a destination defined in platform conf
    #
    #     :param user:
    #     :param destination:
    #     :return:
    #     """
    #     input= self.request.json
    #     destination= input['destination']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     # retrieve destination from configuration
    #     resolved_destination= ptf.destination(destination)
    #     sip_address= ptf.sip_address_for(resolved_destination)
    #     return self._proxy_operation(item,'transfer',destination= sip_address)
    #
    #
    #     #print "pilot: user %s transfer call to destination %s  (%s)" % (user,destination,sip_address)
    #     #return self._transfer(user,sip_address=sip_address)
    #
    # def op_activate_redirection_to_user(self,item,**kwargs):
    #     """
    #        call Application Server using a Feature Access Code to redirect userA to userB
    #
    #         *@userA*: the user to be redirected
    #
    #         *@userB*: target of the redirection
    #
    #         *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)
    #
    #         *@call_format*: site,national,international,universal
    #
    #         eg: activate redirection to user  Alice  Bob  CFA  national
    #
    #     :param userA:
    #     :param userB:
    #     :param redirect_kind:
    #     :param call_format:
    #     :return:
    #     """
    #     input= self.request.json
    #     userB= input['userB']
    #     redirect_kind= input['redirect_kind']
    #     call_format= input['call_format']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #
    #     fac_name = "+" + redirect_kind
    #     fac_code = ptf.fac(fac_name)
    #
    #     sip_address = ptf.sip_address_for( fac_code + ptf.user(userB).to_user(format_=call_format))
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #print "pilot: user %s ask a call forward via feature access code %s to user %s with format %s (%s) " % (
    #     #    userA,redirect_kind,userB,call_format,destination)
    #     # place the call
    #     #return self._call(userA,sip_address=destination)
    #
    # def op_cancel_redirection(self,item,**kwargs):
    #     """
    #        Convenience function to send a FAC sequence to AS to cancel a redirection
    #
    #         *@userA*: the user to be redirected
    #
    #         *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)
    #
    #     :param user:
    #     :param redirect_kind:
    #     :return:
    #     """
    #     input= self.request.json
    #     redirect_kind= input['redirect_kind']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     fac_name = "-" + redirect_kind
    #     fac_code = ptf.fac(fac_name)
    #     sip_address = ptf.sip_address_for(fac_code)
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #print "pilot: call AS to cancel a redirection of type %s for user %s (%s)" %(redirect_kind,user,destination)
    #     # place the call
    #     #return self._call(user,sip_address=destination)
    #
    # def op_check_incoming_call(self,item,**kwargs):
    #     """
    #         checks display name on FROM header of the incoming INVITE message
    #
    #         *@user*: str , the user receiving the call (eg Bob)
    #
    #         *@display_name*: the display name to compare with ( or unkwown )
    #
    #     :param user:
    #     :param display_name:
    #     :return:
    #     """
    #     input= self.request.json
    #     display_name= input['display_name']
    #
    #     user_data= self.backend.item_get(self.collection,item)
    #     user= user_data['alias']
    #
    #
    #     validation = self.check_caller(user,display_name)
    #
    #     r= self.check_incoming_call(item,validation)
    #     rc= { 'code':200, 'message': 'check OK' , 'logs':['Check_incoming call OK']}
    #     return rc

    #
    #
    # def op_get_last_received_sip_request(self,item,sip_method="INVITE"):
    #     """
    #
    #     :param item:
    #     :param sip_method:
    #     :return:
    #     """
    #     input= self.request.json
    #     sip_method=input['sip_method']
    #
    #     message= self.get_last_received_sip_request(item,sip_method=sip_method)
    #
    #     res= { 'code':200 , 'message': message, 'logs':[]}
    #     return res


