"""


    tools to parse

        sip messages
            Message()

        and sip headers
            Header()
            create_header_from_line(header_line)


    extracted from P2P-SIP project   rfc3261.py


"""


import re,traceback


from rfc2396 import  URI
from rfc2396 import  Address

from rfc4566 import SDP as rfc_SDP

from rfc3261 import Message as rfc_Message
from rfc3261 import Header as rfc_Header

#from  rfc3261 import URI




# #----------------------- Header and Message -------------------------------
# _debug = False
#
# _quote   = lambda s: '"' + s + '"' if s[0] != '"' != s[-1] else s
# _unquote = lambda s: s[1:-1] if s[0] == '"' == s[-1] else s
#
# # various header types: standard (default), address, comma and unstructured
# _address      = ['contact', 'from', 'record-route', 'refer-to', 'referred-by', 'route', 'to']
# _comma        = ['authorization', 'proxy-authenticate', 'proxy-authorization', 'www-authenticate']
# _unstructured = ['call-id', 'cseq', 'date', 'expires', 'max-forwards', 'organization', 'server', 'subject', 'timestamp', 'user-agent']
# # short form of header names
# _short        = ['allow-events', 'u', 'call-id', 'i', 'contact', 'm', 'content-encoding', 'e', 'content-length', 'l', 'content-type', 'c', 'event', 'o', 'from', 'f', 'subject', 's', 'supported', 'k', 'to', 't', 'via', 'v']
# # exception for canonicalization of header names
# _exception    = {'call-id':'Call-ID','cseq':'CSeq','www-authenticate':'WWW-Authenticate'}
# #_canon   = lambda s: '-'.join([x.capitalize() for x in s.split('-')]) if s.lower() not in ['cseq','call-id','www-authenticate'] else {'cseq':'CSeq','call-id':'Call-ID','www-authenticate':'WWW-Authenticate'}[s.lower()]
#
#
#
# # customizations L.T
# _address.extend(['remote-party-id','p-asserted-identity','p-called-party-id'])
# #_unstructured.extend(['p-asserted-identity',])
#
#
# def _canon(s):
#     '''Return the canonical form of the header.
#     >>> print _canon('call-Id'), _canon('fRoM'), _canon('refer-to')
#     Call-ID From Refer-To
#     '''
#     s = s.lower()
#     return ((len(s)==1) and s in _short and _canon(_short[_short.index(s)-1])) \
#         or (s in _exception and _exception[s]) or '-'.join([x.capitalize() for x in s.split('-')])
#
#
#
#
# class Header(object):
#     '''A SIP header object with dynamic properties.
#     Attributes such as name, and various parameters can be accessed on the object.
#
#     >>> print repr(Header('"Kundan Singh" <sip:kundan@example.net>', 'To'))
#     To: "Kundan Singh" <sip:kundan@example.net>
#     >>> print repr(Header('"Kundan"<sip:kundan99@example.net>', 'To'))
#     To: "Kundan" <sip:kundan99@example.net>
#     >>> print repr(Header('Sanjay <sip:sanjayc77@example.net>', 'fRoM'))
#     From: "Sanjay" <sip:sanjayc77@example.net>
#     >>> print repr(Header('application/sdp', 'conTenT-tyPe'))
#     Content-Type: application/sdp
#     >>> print repr(Header('presence; param=value;param2=another', 'Event'))
#     Event: presence;param=value;param2=another
#     >>> print repr(Header('78  INVITE', 'CSeq'))
#     CSeq: 78 INVITE
#     '''
#
#     def __init__(self, value=None, name=None):
#         '''Construct a Header from optional value and optional name.'''
#         self.name = name and _canon(name.strip()) or None
#         self.value = self._parse(value.strip(), self.name and self.name.lower() or None)
#
#     def _parse(self, value, name):
#         '''Parse a header string value for the given header name.'''
#         if name in _address: # address header
#             addr = Address(); addr.mustQuote = True
#             count = addr.parse(value)
#             value, rest = addr, value[count:]
#             if rest:
#                 for k, v in self.parseParams(rest): self.__dict__[k] = v
# #            for n,sep,v in map(lambda x: x.partition('='), rest.split(';') if rest else []):
# #                if n.strip():
# #                    self.__dict__[n.lower().strip()] = v.strip()
#         elif name not in _comma and name not in _unstructured: # standard
#             value, sep, rest = value.partition(';')
#             if rest:
#                 for k, v in self.parseParams(rest): self.__dict__[k] = v
# #            for n,sep,v in map(lambda x: x.partition('='), rest.split(';') if rest else []):
# #                # TO DO: add checks for token
# #                self.__dict__[n.lower().strip()] = v.strip()
#         if name in _comma:
#             self.authMethod, sep, rest = value.strip().partition(' ')
#             if rest:
#                 for k, v in self.parseParams(rest, delimiter=','): self.__dict__[k] = v
# #            for n,v in map(lambda x: x.strip().split('='), rest.split(',') if rest else []):
# #                self.__dict__[n.lower().strip()] = _unquote(v.strip())
#         elif name == 'cseq':
#             n, sep, self.method = map(lambda x: x.strip(), value.partition(' '))
#             self.number = int(n); value = n + ' ' + self.method
#         return value
#
#     @staticmethod
#     def parseParams(rest, delimiter=';'):
#         '''A generator to parse the parameters using the supplied delimitter.
#         >>> print list(Header.parseParams(";param1=value1;param2=value2"))
#         [('param1', 'value1'), ('param2', 'value2')]
#         >>> print list(Header.parseParams(';param1="value1" ;param2="value2"'))
#         [('param1', 'value1'), ('param2', 'value2')]
#         >>> print list(Header.parseParams('param1="value1", param2=value2', delimiter=','))
#         [('param1', 'value1'), ('param2', 'value2')]
#         >>> print list(Header.parseParams('param1="";param2'))
#         [('param1', ''), ('param2', '')]
#         >>> print list(Header.parseParams('param1="";param2=;'))  # error cases
#         [('param1', ''), ('param2', '')]
#         '''
#         try:
#             length, index = len(rest), 0
#             while index < length:
#                 sep1 = rest.find('=', index)
#                 sep2 = rest.find(delimiter, index)
#                 if sep2 < 0: sep2 = length # next parameter
#                 n = v = ''
#                 if sep1 >= 0 and sep1 < sep2: # parse "a=b;..." or "a=b"
#                     n = rest[index:sep1].lower().strip()
#                     if rest[sep1+1] == '"':
#                         sep1 += 1
#                         sep2 = rest.find('"', sep1+1)
#                     if sep2 >= 0:
#                         v = rest[sep1+1:sep2].strip()
#                         index = sep2+1
#                     else:
#                         v = rest[sep1+1:].strip()
#                         index = length
#                 elif sep1 < 0 or sep1 >= 0 and sep1 > sep2: # parse "a" or "a;b=c" or ";b"
#                     n, index = rest[index:sep2].lower().strip(), sep2+1
#                 else: break
#                 if n:
#                     yield (n, v)
#         except:
#             if _debug: print 'error parsing parameters'; traceback.print_exc()
#         raise StopIteration(None)
#
#
#     def __str__(self):
#         '''Return a string representation of the header value.'''
#         # TO DO: use reduce instead of join+map
#         name = self.name.lower()
#         rest = '' if ((name in _comma) or (name in _unstructured)) \
#                 else (';'.join(['%s'%(x,) if not y else ('%s=%s'%(x.lower(), y) if re.match(r'^[a-zA-Z0-9\-_\.=]*$', str(y)) else '%s="%s"'%(x.lower(), y))for x, y in self.__dict__.iteritems() if x.lower() not in ('name','value', '_viauri')]))
#         return str(self.value) + (rest and (';'+rest) or '');
#
#     def __repr__(self):
#         '''Return the string representation of header's "name: value"'''
#         return self.name + ": " + str(self)
#
#     def dup(self):
#         '''Duplicate this object.'''
#         return Header(self.__str__(), self.name)
#
#     # container access for parameters: use lower-case key in __dict__
#     def __getitem__(self, name): return self.__dict__.get(name.lower(), None)
#     def __setitem__(self, name, value): self.__dict__[name.lower()] = value
#     def __contains__(self, name): return name.lower() in self.__dict__
#
#     @property
#     def viaUri(self):
#         '''Read-only URI representing Via header's value.
#         >>> print Header('SIP/2.0/UDP example.net:5090;ttl=1', 'Via').viaUri
#         sip:example.net:5090;transport=udp
#         >>> print Header('SIP/2.0/UDP 192.1.2.3;rport=1078;received=76.17.12.18;branch=0', 'Via').viaUri
#         sip:76.17.12.18:1078;transport=udp
#         >>> print Header('SIP/2.0/UDP 192.1.2.3;maddr=224.0.1.75', 'Via').viaUri
#         sip:224.0.1.75:5060;transport=udp
#         '''
#         if not hasattr(self, '_viaUri'):
#             if self.name != 'Via': raise ValueError, 'viaUri available only on Via header'
#             proto, addr = self.value.split(' ')
#             type = proto.split('/')[2].lower()  # udp, tcp, tls
#             self._viaUri = URI('sip:' + addr + ';transport=' + type)
#             if self._viaUri.port == None: self._viaUri.port = 5060
#             if 'rport' in self:
#                 try: self._viaUri.port = int(self.rport)
#                 except: pass # probably not an int
#             if type not in ['tcp','sctp','tls']:
#                 if 'maddr' in self: self._viaUri.host = self.maddr
#                 elif 'received' in self: self._viaUri.host = self.received
#         return self._viaUri
#
#     @staticmethod
#     def createHeaders(value):
#         '''Parse a header line and return (name, [Header, Header, Header]) where name
#         represents the header name, and the list has list of Header objects, typically
#         one but for comma separated header line there can be multiple.
#         >>> print Header.createHeaders('Event: presence, reg')
#         ('Event', [Event: presence, Event: reg])
#         '''
#         name, value = map(str.strip, value.split(':', 1))
#         return (_canon(name),  map(lambda x: Header(x, name), value.split(',') if name.lower() not in _comma else [value]))
#
#
#
# class Message(object):
#     '''A SIP message object with dynamic properties.
#     The header names can be accessed as attributes or items and
#     are case-insensitive. Attributes such as method, uri (URI),
#     response (int), responsetext, protocol, and body are available.
#     Accessing an unavailable header gives None instead of exception.
#
#     >>> m = Message()
#     >>> m.method = 'INVITE'
#     '''
#
#     # we dont need to check body_length
#     bypass_body_length_control = True
#
#
#
#     # non-header attributes or items
#     _keywords = ['method','uri','response','responsetext','protocol','_body','body']
#     # headers that can appear only atmost once. subsequent occurance ignored.
#     _single = ['call-id', 'content-disposition', 'content-length', 'content-type', 'cseq', 'date', 'expires', 'event', 'max-forwards', 'organization', 'refer-to', 'referred-by', 'server', 'session-expires', 'subject', 'timestamp', 'to', 'user-agent']
#
#     def __init__(self, value=None):
#         self.method = self.uri = self.response = self.responsetext = self.protocol = self._body = None
#         if value: self._parse(value)
#
#     # attribute access: use lower-case name, and use container if not found
#     def __getattr__(self, name): return self.__getitem__(name)
#     def __getattribute__(self, name): return object.__getattribute__(self, name.lower())
#     def __setattr__(self, name, value): object.__setattr__(self, name.lower(), value)
#     def __delattr__(self, name): object.__delattr__(self, name.lower())
#     def __hasattr__(self, name): object.__hasattr__(self, name.lower())
#     # container access: use lower-case key in __dict__
#     def __getitem__(self, name): return self.__dict__.get(name.lower(), None)
#     def __setitem__(self, name, value): self.__dict__[name.lower()] = value
#     def __delitem__(self, name): del self.__dict__[name.lower()]
#     def __contains__(self, name): return name.lower() in self.__dict__
#
#     def _parse(self, value):
#         '''Parse a SIP message as this object. Throws exception on error'''
#         # TO DO: perform all error checking:
#         # 1. no \r\n\r\n in the message. (done)
#         # 2. no headers.
#         # 3. first line has less than three parts.
#         # 4. syntax for protocol, and must be SIP/2.0
#         # 5. syntax for method or response attributes
#         # 6. first header must not start with a space or tab.
#         # 7. detect and ignore header parsing and multiple instance errors.
#         # 8. Content-Length if present must match the length of body.
#         # 9. mandatory headers are To, From, Call-ID and CSeq.
#         # 10. syntax for top Via header and fields: ttl, maddr, received, branch.
#         indexCRLFCRLF, indexLFLF = value.find('\r\n\r\n'), value.find('\n\n')
#         firstheaders = body = ''
#         if indexCRLFCRLF >= 0 and indexLFLF >= 0:
#             if indexCRLFCRLF < indexLFLF: indexLFLF = -1
#             else: indexCRLFCRLF = -1
#         if indexCRLFCRLF >= 0:
#             firstheaders, body = value[:indexCRLFCRLF], value[indexCRLFCRLF+4:]
#         elif indexLFLF >= 0:
#             firstheaders, body = value[:indexLFLF], value[indexLFLF+2:]
#         else:
#             firstheaders, body = value, '' # assume no body
#         try: firstline, headers = firstheaders.split('\n', 1)
#         except: raise ValueError, 'No first line found'
#         if firstline[-1] == '\r': firstline = firstline[:-1]
#         a, b, c = firstline.split(' ', 2)
#         try:    # try as response
#             self.response, self.responsetext, self.protocol = int(b), c, a # throws error if b is not int.
#         except: # probably a request
#             self.method, self.uri, self.protocol = a, URI(b), c
#
#         hlist = []
#         for h in headers.split('\n'):
#             if h and h[-1] == '\r': h = h[:-1]
#             if h and (h[0] == ' ' or h[0] == '\t'):
#                 if hlist:
#                     hlist[-1] += h
#             else:
#                 hlist.append(h)
#         for h in hlist:
#             try:
#                 name, values = Header.createHeaders(h)
#                 if name not in self: # doesn't already exist
#                     self[name] = values if len(values) > 1 else values[0]
#                 elif name not in Message._single: # valid multiple-instance header
#                     if not isinstance(self[name],list): self[name] = [self[name]]
#                     self[name] += values
#             except:
#                 if _debug: print 'error parsing', h
#                 continue
#         bodyLen = int(self['Content-Length'].value) if 'Content-Length' in self else 0
#         if body: self.body = body
#         # LT , bypass length control
#         if not self.bypass_body_length_control:
#             if self.body != None and bodyLen != len(body): raise ValueError, 'Invalid content-length %d!=%d'%(bodyLen, len(body))
#         for h in ['To','From','CSeq','Call-ID']:
#             if h not in self: raise ValueError, 'Mandatory header %s missing'%(h)
#
#     def __repr__(self):
#         '''Return the formatted message string.'''
#         if self.method != None: m = self.method + ' ' + str(self.uri) + ' ' + self.protocol + '\r\n'
#         elif self.response != None: m = self.protocol + ' ' + str(self.response) + ' ' + self.responsetext + '\r\n'
#         else: return None # invalid message
#         for h in self:
#             m += repr(h) + '\r\n'
#         m+= '\r\n'
#         if self.body != None: m += self.body
#         return m
#
#     def dup(self):
#         '''Duplicate this object.'''
#         return Message(self.__repr__())
#
#     def __iter__(self):
#         '''Return iterator to iterate over all Header objects.'''
#         h = list()
#         for n in filter(lambda x: not x.startswith('_') and x not in Message._keywords, self.__dict__):
#             h += filter(lambda x: isinstance(x, Header), self[n] if isinstance(self[n],list) else [self[n]])
#         return iter(h)
#
#     def first(self, name):
#         '''Return the first Header object for this name, or None.'''
#         result = self[name]
#         return isinstance(result,list) and result[0] or result
#
#     def all(self, *args):
#         '''Return list of the Header object (or empty list) for all the header names in args.'''
#         args = map(lambda x: x.lower(), args)
#         h = list()
#         for n in filter(lambda x: x in args and not x.startswith('_') and x not in Message._keywords, self.__dict__):
#             h += filter(lambda x: isinstance(x, Header), self[n] if isinstance(self[n],list) else [self[n]])
#         return h
#
#     def insert(self, header, append=False):
#         if header and header.name:
#             if header.name not in self:
#                 self[header.name] = header
#             elif isinstance(self[header.name], Header):
#                 self[header.name] = (append and [self[header.name], header] or [header, self[header.name]])
#             else:
#                 if append: self[header.name].append(header)
#                 else: self[header.name].insert(0, header)
#         # TO DO: don't insert multi-instance if single header type.
#
#     def delete(self, name, position=None):
#         '''Delete a named header, either all (default) or at given position (0 for first, -1 for last).'''
#         if position is None: del self[name] # remove all headers with this name
#         else:
#             h = self.all(name) # get all headers
#             try: del h[position]    # and remove at given position
#             except: pass       # ignore any error in index
#             if len(h) == 0: del self[name]
#             else: self[name] = h[0] if len(h) == 1 else h
#
#     def body():
#         '''The body property, when set also sets the Content-Length header field.'''
#         def fset(self, value):
#             self._body = value
#             self['Content-Length'] = Header('%d'%(value and len(value) or 0), 'Content-Length')
#         def fget(self):
#             return self._body
#         return locals()
#     body = property(**body())
#
#     @staticmethod
#     def _populateMessage(m, headers=None, content=None):
#         '''Modify m to add headers (list of Header objects) and content (str body)'''
#         if headers:
#             for h in headers: m.insert(h, True) # append the header instead of overriding
#         if content: m.body = content
#         else: m['Content-Length'] = Header('0', 'Content-Length')
#
#     @staticmethod
#     def createRequest(method, uri, headers=None, content=None):
#         '''Create a new request Message with given attributes.'''
#         m = Message()
#         m.method, m.uri, m.protocol = method, URI(uri), 'SIP/2.0'
#         Message._populateMessage(m, headers, content)
#         if m.CSeq != None and m.CSeq.method != method: m.CSeq = Header(str(m.CSeq.number) + ' ' + method, 'CSeq')
#         #if _debug: print 'createRequest returned\n', m
#         return m
#
#     @staticmethod
#     def createResponse(response, responsetext, headers=None, content=None, r=None):
#         '''Create a new response Message with given attributes.
#         The original request may be specified as the r parameter.'''
#         m = Message()
#         m.response, m.responsetext, m.protocol = response, responsetext, 'SIP/2.0'
#         if r:
#             m.To, m.From, m.CSeq, m['Call-ID'], m.Via = r.To, r.From, r.CSeq, r['Call-ID'], r.Via
#             if response == 100: m.Timestamp = r.Timestamp
#         Message._populateMessage(m, headers, content)
#         return m
#
#     # define is1xx, is2xx, ... is6xx and isfinal
#     for x in range(1,7):
#         exec 'def is%dxx(self): return self.response and (self.response / 100 == %d)'%(x,x)
#         exec 'is%dxx = property(is%dxx)'%(x,x)
#     @property
#     def isfinal(self): return self.response and (self.response >= 200)
#
#
#     # add by LT
#     @property
#     def sdp(self):
#         """
#             return a SDP object brom message body
#         """
#         # check content-type is application-sdp
#         try:
#             content_type = self['content-type']
#             if not str(content_type) == 'application/sdp':
#                 raise ValueError('message content-type is not application/sdp')
#         except KeyError:
#             raise KeyError("message has no content type")
#         return SDP(self.body)

########## LT additions

class Header(rfc_Header):
    """

    """
    pass

Header._address.extend(['remote-party-id','p-asserted-identity','p-called-party-id'])

class SDP(rfc_SDP):
    """
        custom sdp to add facilities
            is_sendonly
            is_recvonly
            is_sendrecv

    """
    _sound_modes = ['sendrecv','recvonly','sendonly']


    def mode(self,connection=0):
        """
            return mode

        """
        # set default mode if none is there
        mode='sendrcv'
        for v in self.m[connection].a:
            if v in self._sound_modes:
                mode= v
                break
        return mode


    def is_mode(self,mode, connection=0):
        """

        """
        assert mode  in self._sound_modes

        for v in self.m[connection].a:
            if v == mode:
                return True
        # we dont find this mode explicilty
        if mode == 'sendrecv':
            # by default it is sendrecv
            return True
        else:
            # dont match the mode
            return False



class Message(rfc_Message):
    """


    """
    # add by LT
    @property
    def sdp(self):
        """
            return a SDP object brom message body
        """
        # check content-type is application-sdp
        try:
            content_type = self['content-type']
            if not str(content_type) == 'application/sdp':
                raise ValueError('message content-type is not application/sdp')
        except KeyError:
            raise KeyError("message has no content type")
        return SDP(self.body)









def create_header_from_line(header_line):
    """


    """
    name ,headers = Header.createHeaders(header_line)
    assert len(headers) == 1 , "create header doesnt support multiline headers"
    return headers[0]


# class Message(rfc_Message):
#     """
#
#     """
#
#     @property
#     def sdp(self):
#         """
#             return a SDP object brom message body
#         """
#         # check content-type is application-sdp
#         try:
#             content_type = self['content-type']
#             if not str(content_type) == 'application/sdp':
#                 raise ValueError('message content-type is not application/sdp')
#         except KeyError:
#             raise KeyError("message has no content type")
#         return SDP(self.body)









if __name__=="__main__":


    sample_rx_invite_message="""\
INVITE sip:+33146502511@172.16.229.1:54696;ob SIP/2.0
Via: SIP/2.0/UDP 172.16.229.144;rport;branch=z9hG4bKX488mX9mUpvSp
Max-Forwards: 69
From: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
To: <sip:+33146502511@172.16.229.1:54696;ob>
Call-ID: 61c08eb5-553c-1232-00a3-000c291dfe44
CSeq: 59655872 INVITE
Contact: <sip:mod_sofia@172.16.229.144:5060>
User-Agent: Configured by 2600hz!
Allow: INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, INFO, UPDATE, REGISTER, REFER, NOTIFY, PUBLISH, SUBSCRIBE
Supported: precondition, path, replaces
Allow-Events: talk, hold, conference, presence, as-feature-event, dialog, line-seize, call-info, sla, include-session-description, presence.winfo, message-summary, refer
Content-Type: application/sdp
Content-Disposition: session
Content-Length: 211
X-FS-Support: update_display,send_info
Remote-Party-ID: "2510" <sip:2510@172.16.229.144>;party=calling;screen=yes;privacy=off

v=0
o=FreeSWITCH 1398278759 1398278760 IN IP4 172.16.229.144
s=FreeSWITCH
c=IN IP4 172.16.229.144
t=0 0
m=audio 17922 RTP/AVP 8 9 0 3 101 13
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-16
a=ptime:20

"""

    sample_rx_invite_message_btic="""\
INVITE sip:+33146502515@213.56.88.208:64923;ob SIP/2.0
v:SIP/2.0/UDP 80.12.197.168:5060;branch=z9hG4bK_IMSCA0720430.000_530102838aaa2971fa3869d0657bed0b;lskpmc=P20
Record-Route:<sip:80.12.197.168;routing_id=pcscf_b_side;lskpmc=P20;lr>
f:"Q11A-EasyValid Q11A-EasyValid"<sip:2514@sip.osp.com;user=phone>;tag=1535289041-1395424405184-
t:"Q11B-EasyValid Q11B-EasyValid"<sip:+33146502515@sip.osp.com;user=phone>
i:BW185325184210314506329631@172.25.8.149
CSeq:853758817 INVITE
m:<sip:bticas02.sip.osp.com>
P-Asserted-Identity:<sip:+33146502514@sip.osp.com>,<tel:+33146502514>
Privacy:none
Allow:ACK,BYE,CANCEL,INFO,INVITE,OPTIONS,PRACK,REFER,NOTIFY,UPDATE
Accept:application/dtmf-relay,application/media_control+xml,application/sdp,multipart/mixed
k:
Max-Forwards:68
c:application/sdp
l:430
P-Called-Party-ID:<sip:+33146502515@sip.osp.com>
P-Early-Media:supported

v=0
o=BroadWorks 4371 1 IN IP4 sip.osp.com
s=-
b=AS:84
t=0 0
a=X-nat:0
m=audio 40000 RTP/AVP 98 97 99 104 3 0 8 9 96
c=IN IP4 172.16.229.141
b=TIAS:64000
a=sendrecv
a=rtpmap:98 speex/16000
a=rtpmap:97 speex/8000
a=rtpmap:99 speex/32000
a=rtpmap:104 iLBC/8000
a=fmtp:104 mode=30
a=rtpmap:3 GSM/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:9 G722/8000
a=rtpmap:96 telephone-event/8000
a=fmtp:96 0-15

"""

    sample_rx_invite_message_btelu="""\
INVITE sip:+33960833531@213.56.88.208:61467;ob SIP/2.0
v:SIP/2.0/UDP 80.12.197.168:5060;branch=z9hG4bK_IMSCA0720430.000_e32e757abce5315f175eed65e865dcb9;lskpmc=P04
Record-Route:<sip:80.12.197.168;routing_id=pcscf_b_side;lskpmc=P04;lr>
f:"U01 U01"<sip:+33960833530@sip.osp.com;user=phone>;tag=0082-00000c17-098b
t:<sip:+33960833531@sip.osp.com;user=phone>
i:0082-00000c17-098b53303c75-2049-3ccde8@sipua
CSeq:31917 INVITE
Max-Forwards:64
m:<sip:imspcf211gm.sip.osp.com>
Allow:PRACK,INVITE,ACK,BYE,CANCEL,UPDATE,INFO,SUBSCRIBE,NOTIFY,REFER,MESSAGE,OPTIONS
k:100rel
User-Agent:pjsip python
P-Asserted-Identity:<tel:+33960833530>,<sip:+33960833530@sip.osp.com;user=phone>
c:application/sdp
l:436
P-Called-Party-ID:<sip:+33960833531@sip.osp.com>
P-Early-Media:supported

v=0
o=- 3604658933 3604658933 IN IP4 sip.osp.com
s=-
b=AS:84
t=0 0
a=X-nat:0
m=audio 40000 RTP/AVP 98 97 99 104 3 0 8 9 96
c=IN IP4 172.16.229.141
b=TIAS:64000
a=sendrecv
a=rtpmap:98 speex/16000
a=rtpmap:97 speex/8000
a=rtpmap:99 speex/32000
a=rtpmap:104 iLBC/8000
a=fmtp:104 mode=30
a=rtpmap:3 GSM/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:9 G722/8000
a=rtpmap:96 telephone-event/8000
a=fmtp:96 0-15

"""


    # 18:43:56.609   pjsua_core.c  .RX 1315 bytes Request msg INVITE/cseq=20115 (rdata0x8dbc154) from UDP 80.12.197.168:5060:
    sample_btlu_received_invite="""\
INVITE sip:+33960833531@213.56.88.208:64949;ob SIP/2.0
v:SIP/2.0/UDP 80.12.197.168:5060;branch=z9hG4bK_IMSCA0720430.000_9da3de2c539283787795fc1e34850709;lskpmc=P16
Record-Route:<sip:80.12.197.168;routing_id=pcscf_b_side;lskpmc=P16;lr>
f:"U01 U01"<sip:+33960833530@sip.osp.com;user=phone>;tag=0082-000004e8-025c
t:<sip:+33960833531@sip.osp.com;user=phone>
i:0082-000004e8-025c532b28d9-5299-566af8@sipua
CSeq:20115 INVITE
Max-Forwards:64
m:<sip:imspcf211gm.sip.osp.com>
Allow:PRACK,INVITE,ACK,BYE,CANCEL,UPDATE,INFO,SUBSCRIBE,NOTIFY,REFER,MESSAGE,OPTIONS
k:100rel
User-Agent:pjsip python
P-Asserted-Identity:<tel:+33960833530>,<sip:+33960833530@sip.osp.com;user=phone>
Diversion:<sip:+33960833534@sip.osp.com;user=phone>;reason=unconditional;privacy=off;counter=1
c:application/sdp
l:436
P-Called-Party-ID:<sip:+33960833531@sip.osp.com>
P-Early-Media:supported

v=0
o=- 3604326233 3604326233 IN IP4 sip.osp.com
s=-
b=AS:84
t=0 0
a=X-nat:0
m=audio 40000 RTP/AVP 98 97 99 104 3 0 8 9 96
c=IN IP4 172.16.229.141
b=TIAS:64000
a=sendrecv
a=rtpmap:98 speex/16000
a=rtpmap:97 speex/8000
a=rtpmap:99 speex/32000
a=rtpmap:104 iLBC/8000
a=fmtp:104 mode=30
a=rtpmap:3 GSM/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:9 G722/8000
a=rtpmap:96 telephone-event/8000
a=fmtp:96 0-15

"""
#--end msg--



    sdp_samples=[
"""
v=0
o=- 3608972673 3608972674 IN IP4 192.168.216.1
s=pjmedia
b=AS:84
t=0 0
a=X-nat:0
m=audio 40002 RTP/AVP 8 101
c=IN IP4 192.168.216.1
b=TIAS:64000
a=rtcp:40003 IN IP4 192.168.216.1
a=sendrecv
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15

""",

"""\
v=0
o=FreeSWITCH 1400135866 1400135868 IN IP4 172.16.229.144
s=FreeSWITCH
c=IN IP4 172.16.229.144
t=0 0
m=audio 31878 RTP/AVP 8 96
a=rtpmap:8 PCMA/8000
a=rtpmap:96 telephone-event/8000
a=fmtp:96 0-16
a=recvonly
a=silenceSupp:off - - - -
a=ptime:20


""" ,
"""\
v=0
o=- 3609156524 3609156526 IN IP4 192.168.1.154
s=pjmedia
b=AS:84
t=0 0
a=X-nat:0
m=audio 40000 RTP/AVP 8 98 97 104 3 0 99 9 96
c=IN IP4 192.168.1.154
b=TIAS:64000
a=rtcp:40001 IN IP4 192.168.1.154
a=sendrecv
a=rtpmap:8 PCMA/8000
a=rtpmap:98 speex/16000
a=rtpmap:97 speex/8000
a=rtpmap:104 iLBC/8000
a=fmtp:104 mode=30
a=rtpmap:3 GSM/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:99 speex/32000
a=rtpmap:9 G722/8000
a=rtpmap:96 telephone-event/8000
a=fmtp:96 0-15


"""
]





    def display_adress(addr):

        if isinstance(addr.value,basestring):
            print addr.value
        else:
            print addr.value.displayable
            print addr.value.uri.user
            print addr.value.uri.host
            print addr.value.uri.scheme
        print


    def test_header():


        cmd = Header('INVITE sip:+33146502511@172.16.229.1:54696;ob SIP/2.0','INVITE')
        # not meaningfull for sip command


        response = Header('SIP/2.0/UDP example.net:5090;ttl=1', 'Via')
        print response.value
        print response.ttl
        assert response.viaUri == 'sip:example.net:5090;transport=udp'



        addr=Header('"2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF','From')
        print addr.tag
        display_adress(addr)


        addr=create_header_from_line('From: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF')
        print addr.tag
        display_adress(addr)


        addr= create_header_from_line('Contact: <sip:mod_sofia@172.16.229.144:5060>')
        display_adress(addr)



        e = create_header_from_line('CSeq: 59655872 INVITE')
        print e.number


        addr = Header('<sip:+33146502511@172.16.229.1:54696;ob>', 'To')
        display_adress(addr)


        print repr(Header('"Kundan Singh" <sip:kundan@example.net>', 'To'))


        addr = Header('"Kundan Singh" <sip:kundan@example.net>', 'From')
        display_adress(addr)

        addr = Header('<sip:kundan@example.net>', 'From')
        display_adress(addr)

        addr = Header('"Kundan Singh" <sip:kundan@example.net>', 'From')
        display_adress(addr)


        print 'Remote-Party-ID: "2510" <sip:2510@172.16.229.144>;party=calling;screen=yes;privacy=off'
        addr = Header('"2510" <sip:2510@172.16.229.144>;party=calling;screen=yes;privacy=off', 'Remote-Party-ID')
        print addr.party
        print addr.screen
        print addr.privacy
        display_adress(addr)


        # multi line ( comma separated )
        h= "P-Asserted-Identity:<tel:+33960833530>,<sip:+33960833530@sip.osp.com;user=phone>"
        print h
        name,headers = Header.createHeaders(h)

        for header in headers:
            #addr= Header( '<tel:+33960833530>,<sip:+33960833530@sip.osp.com;user=phone>','P-Asserted-Identity')
            display_adress(header)
            assert  header.value.displayable ==   '+33960833530'

        return


    def test_uri():


        r = URI('sip:kundan@example.net')
        assert r == "sip:kundan@example.net"

        r =  URI('sip:kundan:passwd@example.net:5060;transport=udp;lr?name=value&another=another')
        assert r == 'sip:kundan:passwd@example.net:5060;lr;transport=udp?name=value&another=another'

        r = URI('sip:192.1.2.3:5060')
        assert r == 'sip:192.1.2.3:5060'

        r = URI("sip:kundan@example.net") == URI("sip:Kundan@Example.NET")
        assert r == True

        r = URI()
        assert r == ''

        r = URI('tel:+1-212-9397063')
        assert r ==  'tel:+1-212-9397063'

        r = URI('sip:kundan@192.1.2.3:5060').hostPort
        assert r == ('192.1.2.3', 5060)

        return


    def test_sdp():

        """


        """
    for sdp_sample in sdp_samples:

        sdp = SDP(sdp_sample)

        print sdp.is_mode('sendrecv')
        print sdp.is_mode('sendonly')
        print sdp.is_mode('recvonly')

        continue







    def test_message():
        """

        """
        message = Message(sample_rx_invite_message)

        print message.method
        print message.uri
        print message.to

        print message['from']

        print message['remote-party-id']

        for header in message:
            print header

        From = message["from"]
        print From.tag
        print From.value.displayable
        print From.value.uri.user
        print From.value.uri.host
        print From.value.uri.scheme


        sdp = SDP(message.body)

        print message.sdp.is_mode('sendrecv')

        sdp = message.sdp



        return

    def test_message_short():
        """

        """
        message = Message(sample_rx_invite_message_btic)

        print message.method
        print message.uri
        print message.to

        print message['from']

        print message['remote-party-id']

        for header in message:
            print header

        From = message["from"]
        print From.tag
        print From.value.displayable
        print From.value.uri.user
        print From.value.uri.host
        print From.value.uri.scheme


        sdp = SDP(message.body)

        print message.sdp.is_mode('sendrecv')

        sdp = message.sdp


        message = Message(sample_rx_invite_message_btelu)

        print message.method
        print message.uri
        print message.to

        print message['from']

        print message['remote-party-id']

        for header in message:
            print header

        From = message["from"]
        print From.tag
        print From.value.displayable
        print From.value.displayName
        print From.value.uri.user
        print From.value.uri.host
        print From.value.uri.scheme


        sdp = SDP(message.body)

        print message.sdp.is_mode('sendrecv')

        sdp = message.sdp



        return


    def test_parse_received_invite():
        """

            parse a received INVITE (btlu)


        :return:
        """
        m= Message(sample_btlu_received_invite)

        # P-Asserted-Identity:<tel:+33960833530>,<sip:+33960833530@sip.osp.com;user=phone>
        p_asserted_identity= m['p-asserted-identity']

        p_called_party_id= m['P-Called-Party-ID']

        p_remote_party_id= m['Remote-Party-ID']


        #

        result={}
        if not isinstance(p_asserted_identity,list):
            p_asserted_identity= [p_asserted_identity,]
        for header in p_asserted_identity:
            result['scheme']= header.value.uri.scheme
            result['user'] = header.value.uri.user
            result['host']= header.value.uri.host

            if result['scheme'] == 'tel':
                print "telephone"
            elif result['scheme'] == 'sip':
                print "sip"
            assert  result['user'] ==   '+33960833530'
        print "p-asserted-identity= '%s'" % str(result)


        sdp= m.sdp

        print "sdp mode is:  %s" % sdp.mode()
        assert sdp.mode == 'sendrecv'

        sendrecv= m.sdp.is_mode('sendrecv')
        sendonly= m.sdp.is_mode('sendonly')
        recvonly= m.sdp.is_mode('recvonly')

        assert sendrecv == True
        return






    ### begins here


    test_uri()
    test_header()
    test_sdp()
    test_message()
    test_message_short()
    test_parse_received_invite()