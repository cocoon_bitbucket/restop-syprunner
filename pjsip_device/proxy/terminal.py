import os
from copy import deepcopy


SYPRUNNER_CONFIGURATION = dict (

    SYPRUNNER_BOUND_ADDR = None,
    SYPRUNNER_IP_ADDR = None,
    SYPRUNNER_OUTBOUND = None,
    SYPRUNNER_NAME_SERVER = None,

    # the base for the rtp port assignation
    #SYPRUNNER_RTP_PORT= 40000,
    SYPRUNNER_RTP_PORT= 20000,

    # the base for the local port assignation
    SYPRUNNER_LOCAL_PORT = 5060,

)

# where to find media files on target
SYPRUNNER_MEDIA_DIR = "/tests/media"


# demo mode
SYPRUNNER_BLUEBOX_DOMAIN ='bluebox'
# place the ip of the demo freeswitch in ENV var SYPRUNNER_BLUEBOX
SYPRUNNER_BLUEBOX_IP = os.environ.get('SYPRUNNER_BLUEBOX_IP',SYPRUNNER_BLUEBOX_DOMAIN)



def get_env_defaults():
    """
        return values of SYPRUNNER

    :return: dict ,
    """
    defaults= {}

    # set transport parameters from ENV and defaults
    for name in SYPRUNNER_CONFIGURATION.keys():
        env_var = os.environ.get(name,SYPRUNNER_CONFIGURATION[name])
        if name in ['SYPRUNNER_LOCAL_PORT',]:
            env_var = int(env_var)
        elif name in ['SYPRUNNER_RTP_PORT',]:
            # adapt start rtp_port  reserve 4 rtp_port for each terminal eg 40000 + 4
            if env_var :
                env_var = int(env_var)
            else :
                # ignore
                continue
        elif name in ['PJSUA_BOUND_ADDR',]:
            env_var = int(env_var)
        # translate ENV name into pjsua parameter name (truncate prefix + lower
        param_name = name[len('SYPRUNNER_'):].lower()
        defaults[param_name] = env_var

    return defaults





class TerminalOptions(object):
    """
        handle the options of a pjsip terminal

    """
    levels= ["system","platform","device","account","session"]
    default_options= {
        "system": {
                    "--log-level": 5, "--app-log-level": 4, "--stdout-refresh": 2 , '--null-audio': '' },
        "platform": {
                    "--registrar": None ,          # 193.253.72.124:5060;lr
                    "--realm": "*",                # sip.imsnsn.fr
                    "--password": None,            # nsnims2008
        },
        "device": {
                    "--max-calls": 4 },

        "account": {
                "--id": None,                      # sip:+33960813200@sip.imsnsn.fr
                "--username": None,                # +33960813200@sip.imsnsn.fr
        },
        "session": {
                "--local-port": 6000,
                "--rtp-port":   20000,
        }

    }


    def __init__(self,default=None):
        """

        :param default:
        :return:
        """
        self.options={}
        self._with_audio= False
        self._codecs= {}


        self.options.update(self.default_options)
        #self.options= deepcopy(self.default_options)

        if default:
            self.options.update(deepcopy(default))

    def set_system(self,options):
        self.options['system'].update(options)

    def set_device(self,options):
        self.options['device'].update(options)

    def set_platform(self,options):
        self.options['platform'].update(options)

    def set_account(self,options):
        self.options['account'].update(options)

    def set_session(self,options):
        self.options['session'].update(options)

    def set_with_audio(self):
        self._with_audio= True

    def set_null_audio(self):
        self._with_audio= False


    def add_options(self,options,level="session"):
        """

            add a list of options to a specific level
        :param options: list of options tuples eg ["--add
        :param level:
        :return:
        """
        raise NotImplementedError


    def get_options(self):
        """
            get a dictionary of computed options ( hierachic resolution)
        :return:
        """
        opt= {}
        opt.update(self.options['system'])
        opt.update(self.options['platform'])
        opt.update(self.options['device'])
        opt.update(self.options['account'])
        opt.update(self.options['session'])

        ## detect none value
        # nones= [ key for key,value in opt.iteritems() if value==None]
        # if nones:
        #     # some mandatory elements are None
        #     raise ValueError("None is not a correct value for options: %s" % ",".join(nones))

        # remove None values
        # nones = [key for key, value in opt.iteritems() if value == None]
        # for key in nones:
        #     del opt[key]

        # remove blanks value for password etc..
        blanks = [key for key, value in opt.iteritems() if not value ]
        for key in blanks:
            if key in ["--password"]:
                # remove space on None Value for password etc ...
                del opt[key]
            elif opt[key] is None:
                # remove None values
                del opt[key]


        # adjust with_audio
        if self._with_audio:
            # remove null audio
            try:
                del opt['--null-audio']
            except:
                pass
        else:
            # add null audio
            opt['--null-audio']= ''


        return opt

    def gen(self):
        """
        generate options , return a list of options compatible with pjsip
        :return:
        """
        opt= self.get_options()

        # handle codecs 1/2
        #  remove --add-codec and --dis-codec from options and generate codec line parameters
        codec_parameters= self.handle_codecs(opt)


       #  if self._codecs:
       #      # at least one codec has been set: remove default codec and add speciied codecs
       #      try:
       #          del opt['--add-codec']
       #      except:
       #          pass

        opts= [ "%s=%s" % (key,value) for key,value in opt.iteritems() if value]
        flags= [ "%s" % key for key,value in opt.iteritems() if not value]

        opts.extend(flags)
        # handle codec 2/2
        opts.extend(codec_parameters)
        #opts.extend(self.get_codecs())

        return opts

    def clone(self):
        """

        :return:
        """
        new= TerminalOptions(self.options)
        new._with_audio= self._with_audio
        new._codecs=self._codecs

        return new

    def clear_codecs(self):
        """

        :return:
        """
        self._codecs= {}

    def add_codec(self,*names):
        """

        :return:
        """
        if isinstance(names,basestring):
            names= [ names, ]
        for name in names:
            self._codecs[name]=None

    def clear_codec(self,*names):
        """

        :param name:
        :return:
        """
        if isinstance(names,basestring):
            names= [ names, ]
        for name in names:
            try:
                del self._codecs[name]
            except:
                pass

    def get_codecs(self):
        """

        :return:
        """
        lc= [ "--add-codec %s" % codec for codec,dummy in self._codecs.iteritems() ]
        return lc

    def handle_codecs(self,options):
        """

            take a dict of options and return --add-codec and dis-codec parameter list

        :param options:
        :return:
        """
        codecs= []
        try:
            add_codec= options.pop('--add-codec')
            add_codec= self.to_list(add_codec)
        except KeyError:
            add_codec=[]
        try:
            dis_codec= options.pop('--dis-codec')
            dis_codec= self.to_list(dis_codec)
        except KeyError:
            dis_codec=[]


        # remove any add-codec present in dis-codec
        for d in dis_codec:
            try:
                i= add_codec.index(d)
                del add_codec[i]
            except ValueError:
                #not in list : pass
                pass


        codecs= [ '--add-codec %s' % codec for codec in  add_codec]
        dis= [ '--dis-codec %s' % codec for codec in  dis_codec]
        codecs.extend(dis)
        return codecs

    def to_list(self,line):
        """
            transform a multi option line into a list

            eg  "PCMA,PCMU" ->      [ 'PCMA' , 'PCMU']
                'PCMA'      ->      ['PCMA']
                ['PCMA','PCMU'] ->  [ 'PCMA' , 'PCMU']

        :param line: list or comma separated string
        :return: list
        """
        if isinstance(line,list):
            # already a list return it
            pass
        elif isinstance(line,basestring):
            # it is a string, remove spaces
            line=line.replace(' ','')
            if ',' in line:
                #comma separated
                line= line.split(',')
            else:
                # single option
                line= [line]
        elif line is None:
            line=[]
        else:
            raise ValueError("wrong format for list: %s" % line)
        return line
