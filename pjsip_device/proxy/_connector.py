from restop_queues.connectors.process_connector import ProcessConnector






class PjtermConnector(ProcessConnector):
    """

    a connector for pjsip terminal


    usage:

        # create connector with name and command line
        c = ProcessConnector(name="syprunner-fake",command_line="./syprunner-fake.py" )

        # launch theerminal syprunner t
        c.open()

        # send "d" to terminal via stdin
        c.write('d')
        time.sleep(2)

        # read the stdout of the terminal
        line = c.readline()
        while line is not None:
            line = c.readline()

        # send the quit command to the terminal
        c.write('q')
        line = c.readline()
        while line is not None:
            line = c.readline()

        close the terminal
        c.close()



    """

    def send (self,line):
        # alias command for write
        return self.write(line)

    def write(self, line, **kwargs):
        """
            fix process connector to send only one newline

        :param line:
        :return:
        """
        if not line.endswith('\n'):
            line = line + b'\n'
        self._driver.stdin.writelines(line )
        self._driver.stdin.flush()