__author__ = 'cocoon'
"""

    phoneparser:


    simple tool to handle phone number




    in a nutshell
        from a sip username extract number


                BTIC                        BTelu

username    +331 46 50 25 14 @ domain       09 60 83 30 47 @ domain




"""

class PhoneNumber(object):
    """Class representing international telephone numbers.

    This class is hand-created based on phonenumber.proto. Please refer
    to that file for detailed descriptions of the meaning of each field.
    """

    def __init__(self,
                 country_code=None,
                 national_number=None,
                 extension=None,
                 italian_leading_zero=False,
                 number_of_leading_zeros=None,
                 raw_input=None,
                 country_code_source=None,
                 preferred_domestic_carrier_code=None):
        # The country calling code for this number, as defined by the
        # International Telecommunication Union (ITU). Fox example, this would
        # be 1 for NANPA countries, and 33 for France.
        #
        # None if not set, of type int otherwise.
        if country_code is None:
            self.country_code = None
        else:
            self.country_code = int(country_code)

        # Number does not contain National(trunk) prefix.
        # National (significant) Number is defined in International
        # Telecommunication Union (ITU) Recommendation E.164. It is a
        # language/country-neutral representation of a phone number at a
        # country level. For countries which have the concept of Area Code,
        # the National (significant) Number contains the area code. It
        # contains a maximum number of digits which equal to 15 - n, where n
        # is the number of digits of the country code. Take note that National
        # (significant) Number does not contain National(trunk)
        # prefix.
        #
        # None if not set, of type long otherwise (and so it will never
        # contain any formatting (hypens, spaces, parentheses), nor any
        # alphanumeric spellings).

        if national_number is None:
            self.national_number = None
        else:
            self.national_number = long(national_number)

        # Extension is not standardized in ITU recommendations, except for
        # being defined as a series of numbers with a maximum length of 40
        # digits.
        #
        # When present, it is a Unicode string to accommodate for the
        # possible use of a leading zero in the extension (organizations
        # have complete freedom to do so, as there is no standard defined).
        # However, only ASCII digits should be stored here.
        self.extension = extension  # None or Unicode '[0-9]+'

        # In some countries, the national (significant) number starts with one
        # or more "0"s without this being a national prefix or trunk code of
        # some kind. For example, the leading zero in the national
        # (significant) number of an Italian phone number indicates the number
        # is a fixed-line number.  There have been plans to migrate fixed-line
        # numbers to start with the digit two since December 2000, but it has
        # not happened yet. See http://en.wikipedia.org/wiki/%2B39 for more
        # details.
        #
        # These fields can be safely ignored (there is no need to set them)
        # for most countries. Some limited number of countries behave like
        # Italy - for these cases, if the leading zero(s) of a number would be
        # retained even when dialling internationally, set this flag to true,
        # and also set the number of leading zeros.
        #
        # Clients who use the parsing functionality of the i18n phone number
        # libraries will have these fields set if necessary automatically.
        self.italian_leading_zero = bool(italian_leading_zero)
        self.number_of_leading_zeros = number_of_leading_zeros  # None or int

        # The next few fields are non-essential fields for a phone number.
        # They retain extra information about the form the phone number was
        # in when it was provided to us to parse. They can be safely
        # ignored by most clients.

        # This field is used to store the raw input string containing phone
        # numbers before it was canonicalized by the library. For example, it
        # could be used to store alphanumerical numbers such as
        # "1-800-GOOG-411".
        self.raw_input = raw_input  # None or Unicode string

        # The source from which the country_code is derived. This is not set
        # in the general parsing method, but in the method that parses and
        # keeps raw_input. New fields could be added upon request.
        self.country_code_source = country_code_source
        # None or CountryCodeSource.VALUE

        # The carrier selection code that is preferred when calling this
        # phone number domestically. This also includes codes that need to
        # be dialed in some countries when calling from landlines to mobiles
        # or vice versa. For example, in Columbia, a "3" needs to be dialed
        # before the phone number itself when calling from a mobile phone to
        # a domestic landline phone and vice versa.
        #
        # Note this is the "preferred" code, which means other codes may work
        # as well.
        self.preferred_domestic_carrier_code = preferred_domestic_carrier_code
        # None or Unicode string

    def clear(self):
        """Erase the contents of the object"""
        self.country_code = None
        self.national_number = None
        self.extension = None
        self.italian_leading_zero = False
        self.number_of_leading_zeros = None
        self.raw_input = None
        self.country_code_source = None
        self.preferred_domestic_carrier_code = None

    def merge_from(self, other):
        """Merge information from another PhoneNumber object into this one."""
        if other.country_code is not None:
            self.country_code = other.country_code
        if other.national_number is not None:
            self.national_number = other.national_number
        if other.extension is not None:
            self.extension = other.extension
        if other.italian_leading_zero is not None:
            self.italian_leading_zero = other.italian_leading_zero
        if other.number_of_leading_zeros is not None:
            self.number_of_leading_zeros = other.number_of_leading_zeros
        if other.raw_input is not None:
            self.raw_input = other.raw_input
        if other.country_code_source is not None:
            self.country_code_source = other.country_code_source
        if other.preferred_domestic_carrier_code is not None:
            self.preferred_domestic_carrier_code = other.preferred_domestic_carrier_code

    def __eq__(self, other):
        if not isinstance(other, PhoneNumber):
            return False
        return (self.country_code == other.country_code and
                self.national_number == other.national_number and
                self.extension == other.extension and
                self.italian_leading_zero == other.italian_leading_zero and
                self.number_of_leading_zeros == other.number_of_leading_zeros and
                self.raw_input == other.raw_input and
                self.country_code_source == other.country_code_source and
                self.preferred_domestic_carrier_code == other.preferred_domestic_carrier_code)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return ("PhoneNumber(country_code=%s, national_number=%s, extension=%s, " +
                       "italian_leading_zero=%s, number_of_leading_zeros=%s, " +
                       "country_code_source=%s, preferred_domestic_carrier_code=%s)") % (
                    self.country_code,
                    self.national_number,
                    str(self.extension),
                    self.italian_leading_zero,
                    self.number_of_leading_zeros,
                    self.country_code_source,
                    str(self.preferred_domestic_carrier_code))

    def __unicode__(self):
        result = ("Country Code: %s National Number: %s") % (self.country_code, self.national_number)
        if self.italian_leading_zero is not None:
            result += " Leading Zero(s): %s" % self.italian_leading_zero
        if self.number_of_leading_zeros is not None:
            result += " Number of leading zeros: %d" % self.number_of_leading_zeros
        if self.extension is not None:
            result += " Extension: %s" % self.extension
        if self.country_code_source is not None:
            result += " Country Code Source: %s" % self.country_code_source
        if self.preferred_domestic_carrier_code is not None:
            result += " Preferred Domestic Carrier Code: %s" % self.preferred_domestic_carrier_code
        return result





class FrenchPhoneNumber(PhoneNumber):
    """

            simple tool to handle french phone number


            eg number = FrenchNumber.parse("+33146502514")  idem with 0033146502514 0146502514

    """
    @classmethod
    def parse(cls,raw):
        """
            parse a french number return a PhoneNumber instance

        """
        clean = str(raw).replace(' ','')
        # is there a country code
        if clean.startswith('+'):
            # this a universal number eg +33 1 46 50 2514
            if clean.startswith('+33'):
                # this a universal french number , create a number 146502514
                number = clean[3:]
            else:
                raise ValueError("Not a french number: %s" % str(clean))
        elif clean.startswith("0033"):
            # international french number
            number=clean[4:]
        elif clean.startswith('0'):
            # a complete french national number: 0146502514 , 06506070 , 0960833047
            number=clean[1:]
        else:
            # assume it is a national phone number whithout the 0 prefix 146502514 , 6506070 , 960833047
            number=clean[:]

        return cls(country_code="33",national_number=number)

    @classmethod
    def parse_from_sip(cls,raw):
        """
            parse a french number return a PhoneNumber instance

        """
        if raw.startswith('sip:'):
            body= raw[4:]
        else:
            body = raw
        if "@" in body:
            raw_number , domain = body.split("@")
        else:
            raw_number = body

        return cls.parse(raw_number)


    def to_universal(self):
        """
        """
        return "+" + "%s%s" % (self.country_code,self.national_number)


    def to_national(self,escape=""):
        """

        """
        return escape + "0" + str(self.national_number)



    def to_international(self,escape=""):
        """
        """
        return escape + "00" + "%s%s" % (self.country_code,self.national_number)









if __name__=="__main__":


    import re


    def test_PhoneNumber():
        """

        """

        btic = PhoneNumber(country_code = "33", national_number = "146502514")
        btlu = PhoneNumber(country_code = "33",national_number = "960833047" )


        btic2 = FrenchPhoneNumber.parse("+33 1 46 50 25 14")
        assert btic2 == btic

        btlu2 = FrenchPhoneNumber.parse("0960833047")
        assert  btlu2 == btlu


        for number in ["146502514","+33146502514", "0033146502514", "0146502514"]:
            assert btic == FrenchPhoneNumber.parse(number)

        for number in ["960833047", "+33960833047","0033960833047", "0960833047"]:
            assert btlu == FrenchPhoneNumber.parse(number)


        assert btlu2.to_universal() == "+33960833047"
        assert btlu2.to_international() == "0033960833047"
        assert btlu2.to_national() == "0960833047"

        return


    def test_parse_from_sip():
        """

        """
        btlu = FrenchPhoneNumber.parse("0960833047")

        for entry in ["sip:0960833047@sip.osp.com","0960833047@sip.osp.com","0960833047"]:
            assert btlu == FrenchPhoneNumber.parse_from_sip(entry)



    def test_with_escape():


        btic = PhoneNumber(country_code = "33", national_number = "146502514")
        btlu = PhoneNumber(country_code = "33",national_number = "960833047" )





    def get_regexp(exten,base_number,site_id=''):
        """
            @exten: str , 4 digit account ident  2510 , 25 11
            @base_number: str , number without leading 0 , and without extension ( 14650 )
            @site_id: str
        """
        return r'^(((0|00|0033|00033|\+33)(%s))|%s){0,1}(%s)$' %(base_number,site_id,exten)
        # if site_id:
        #     r = r'^(((0|00|0033|00033|\+33)(%s))|%s){0,1}(%s)$' %(base_number,site_id,exten)
        # else:
        #     r = r'^(((0|00|0033|00033|\+33)(%s))){0,1}(%s)$' %(base_number,exten)
        # return r

    def test_tools_for_freeswitch():
        """


           0 00 0033 00033 +33     14650 2510
                11                       2510
                                         2510


        """
        #sel = re.compile(r'^(((0|00|0033|00033|\+33)(14650))|){0,1}(2510)$')

        sel = re.compile(get_regexp('2510','14650','11'))

        tries = [
            "2510", "112510" ,  "0146502510" , '00146502510' , '0033146502510' , "00033146502510" , "+33146502510"
            "2511", "122510" ,  "0246502510" , '0014650112510' ,
        ]
        for tel in tries:
            print "try :%s" % tel
            m = sel.search(tel)
            if m:
                print m.group(1)
                print m.group(2)
                print m.group(3)
                print m.group(4)
                print m.group(5)
                exten = m.group(5)
                print
            else:
                print "no match"
                print
            continue


        return


    ##### begins here ######


    #test_PhoneNumber()
    #test_parse_from_sip()

    test_tools_for_freeswitch()

    print