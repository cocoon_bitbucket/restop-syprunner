"""

    a pjterm session object


    use
        PjtermSessionApi from pjsip_device.api


"""
import time
from pjsip_device.adapter import PjtermSessionListener,PjtermSessionApi
from pjsip_device.adapter import PjtermAdapter

from pjsip_device.api import PjtermLevelOne


class PjtermSession(object):
    """

        handle a pjterm session


         create and shutdwon an api listener


    """
    root= "pjterm"


    @classmethod
    def create_session_listener(self,cnx):
        """

        :param members:
        :return:
        """


        # create pjterm session listener
        session = PjtermSessionListener(cnx, root=self.root)
        session.start()
        return session


    @classmethod
    def shutdown_session_listener(self,cnx):
        """

        :return:
        """
        #session = PjtermSessionListener(cnx, root="pjterm")
        session = PjtermSessionApi(cnx, root= self.root)
        session.send_kill()

    def __init__(self,cnx,root="pjterm"):
        """

        :param cnx:
        :param members:
        """
        self.cnx= cnx
        if root != "pjterm":
            self.root= root


    def open_session(self,members= "alice bob"):
        """

        :param memebers:
        :return:
        """
        # get a session handler
        session = PjtermSessionApi(self.cnx, root=self.root)

        # send clear
        session.send_clear()

        # create remote session
        i= session.open_session(members)
        time.sleep(1)


    def close_session(self):
        """

        :param members:
        :return:
        """
        # get a session handler
        #session = PjtermSessionListener(cnx, root="pjterm")

        print("close session %s/-/close" % self.root)
        #i = self.cnx.publish("pjterm/-/close", "alice bob")

        session = PjtermSessionApi(self.cnx, root= self.root)
        i = session.close_session()

        print("wait 5s ...")
        time.sleep(5)



    def get_terminal(self,name,alias=None,terminal_class=PjtermLevelOne):
        """
        :param name: string eg "userA"
        :param alias: string eg "alice"
        :param options:  string  , pjterm configuration eg "--id ..."
        :return:
        """
        alias= alias or name
        # create pjtermApi
        adapter = PjtermAdapter(self.cnx, alias=alias ,root=self.root)
        #alice = PjtermLevelOne("userA", "alice", adapter=adapter)
        alice = terminal_class( name, role=alias, adapter=adapter)
        #i = alice._start(options)
        return alice


    def start_terminal(self,name,alias=None,options="",terminal_class=PjtermLevelOne):
        """

        :param name:
        :param alias:
        :param options:
        :return:
        """
        alias= alias or name
        terminal= self.get_terminal(name,alias=alias,terminal_class=terminal_class)
        return terminal._start(options=options)