"""

    pjterm_session


    a task to listen to remote pjterm terminals and store stdout on redis queues


    subscribe to redis channel pjterm/*

    store data from channels pjterm/*/stdout to redis queue pjterm:*:stdout
    store data from channels pjterm/*/stdlog to redis queue pjterm:*:stdlog
    store data from channels pjterm/*/stderr to redis queue pjterm:*:stderr

    if a message arrive on channel pjterm/--session--/kill then exit
    if a message arrive on channel pjterm/--session--/clear then reset redis queues


"""
import threading
import time



import logging
logger= logging.getLogger('pjterm')


class PjtermSessionListener(threading.Thread):
    """
        a redis pubsub listener  of pjterm/*


        respond to command on channel $root:--system--:$command


    """
    #root_channel= "pjterm"


    def __init__(self, redis_db, root="pjterm"):
        """

        :param writer: instance of publisher.Writer
        :param connector: instance of ProcessConnector (pjterm)
        """
        threading.Thread.__init__(self)
        self.root= root
        self.redis_db = redis_db
        self.pubsub = self.redis_db.pubsub()
        self.exit_channel= "%s/-session-/kill" % self.root
        self.clear_chanel= "%s/-session-/clear" % self.root
        self.done= False


    #
    #  convenience client functions
    #
    def send_clear(self):
        """
            send a message to clear the redis queues pjterm:*
        :return:
        """
        self.redis_db.publish(self.clear_chanel,"")

    def send_kill(self):
        """
            send a message to kill pjterm sessions
        :return:
        """
        self.redis_db.publish(self.exit_channel ,"")


    def _clear(self):
        """"""
        for key in self.redis_db.scan_iter("%s:*" % self.root):
            self.redis_db.delete(key)
        self.log("RESET pjterm SESSION")
        #self.redis_db.rpush("%s:--system--:stdlog" % self.root,"RESET pjterm SESSION")

    # def channel(self,channel_name):
    #     """
    #
    #     :param channel_name:
    #     :return:
    #     """
    #     return "%s/%s/%s" % (self.root_channel,self.terminal_name,channel_name)

    def log(self,message):
        """

        :param message:
        :return:
        """
        print(message)
        self.redis_db.rpush("%s:-session-:stdlog" % self.root,message)

    def work(self, item):
        """

        :param item: a redis pubsub message of type: message
        :return:
        """
        #self.log("session received message: %s" % item["data"])
        channel= item["channel"]
        # if channel == self.exit_channel:
        #     # exit signal
        #     self.log("receive exit signal")
        #     self.done = True
        #     return
        channel_parts= channel.split("/")
        if len(channel_parts) == 3:
            #self.log(channel_parts)
            root,user,name= channel_parts
            if user == "-session-":
                # execute command on the session
                self.session_control(name,item["data"])
                return
            elif user == "-":
                # this is a pterm_manager message
                self.manager_message(name,item['data'])
                #return
            queue_name= "%s:%s:%s" % (root,user,name)
            self.redis_db.rpush(queue_name,item["data"])
        else:
            # unknown channel
            self.log("unknown channel: %s " % channel)


    def run(self):
        """



        :return:
        """
        # start relay_stdout thread
        self.log("%s session starting" % self.root)

        # first send a message to end previous pjterm_session
        self.redis_db.publish(self.exit_channel,"")
        time.sleep(1)

        # clear all pjterm:* queues
        self._clear()

        # subscribe to pjterm/* channels
        self.log("subscribe to %s channels" % self.root)
        self.pubsub.psubscribe(self.root + "/*")


        self.done= False
        self.log("start listening")
        while not self.done:
            item= self.pubsub.get_message(timeout=5)
            if item:
                if item["type"].endswith("subscribe"):
                    # assume a subscribe message
                    self.log("Subscribtion confirmation %s to channel %s" % (item["type"], item["channel"]))
                else:
                    # assume a message or pmessage
                    self.work(item)
            # else:
            #     print("no item")

        self.pubsub.unsubscribe()
        self.log("%s session unsubscribed and finished" % self.root)


    def session_control(self,control,message):
        """


        :param control:
        :param message:
        :return:
        """
        self.log("receive -session- control :%s" % control)
        if control == "clear":
            # clear all redis queues
            self._clear()
        elif control == "kill":
            # will exit a next iteration
            self.done = True

    def manager_message(self,control,message):
        """

        :param control:
        :param message:
        :return:
        """
        if control == "open":
            # this a new session
            pass
        elif control == "close":
            # this is a close session
            pass


if __name__ == "__main__":

    import redis

    #REDIS_HOST= "192.168.1.21"
    REDIS_HOST= "192.168.99.100"


    REDIS_DB= 0
    REDIS_PORT= 6379

    #REDIS_URL= "redis://192.168.1.21/0"


    logging.basicConfig(level=logging.DEBUG)

    pool = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    r = redis.Redis(connection_pool= pool)

    ping = r.ping()
    logging.log(logging.DEBUG,"redis ping is :%s" % ping)


    sl = PjtermSessionListener(r,root="pjterm")
    sl.start()


    sl.join()
