import redis
from pjterm_adapter import get_pjterm_observer

import time
from pjterm_api import PjtermApi
from pjterm_session import PjtermSessionListener

#REDIS_HOST = "redis"
REDIS_HOST = "192.168.99.100"
ROOT = "pjterm"



cnx = redis.Redis(host=REDIS_HOST)
cnx.ping()


bob= get_pjterm_observer(cnx,"bob")
bob.start()

bob.join()

print("Done")