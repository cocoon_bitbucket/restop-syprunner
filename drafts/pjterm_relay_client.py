import time
import redis
import threading
from pjterm_session import PjtermSessionListener


class Listener(threading.Thread):
    def __init__(self, r, channels):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)

    def work(self, item):
        print(item['channel'], ":", item['data'])

    def run(self):
        for item in self.pubsub.listen():
            if item['data'] == "KILL":
                self.pubsub.unsubscribe()
                print(self, "unsubscribed and finished")
                break
            else:
                self.work(item)





if __name__ == "__main__":


    REDIS_HOST= "redis"
    REDIS_HOST= "192.168.99.100"

    r = redis.Redis(host=REDIS_HOST)
    r.ping()


    # create pjterm session
    session= PjtermSessionListener(r,root="pjterm")
    session_exit_channel = session.exit_channel
    session.start()




    alice_channel = "pjterm/alice/stdin"


    l= Listener(r,["pjterm/alice/stdlog","pjterm/alice/stdout","pjterm"])
    l.start()

    time.sleep(1)



    # start pjterm listeners
    i = r.publish("pjterm/-/open", "alice bob")
    time.sleep(1)

    # start alice terminal
    i = r.publish("pjterm/alice/ctrl", "OPEN --log-level=5 --local-port=5061")

    time.sleep(1)

    # start bob terminal
    i = r.publish("pjterm/bob/ctrl", "OPEN --log-level=5 --local-port=5062")


    # send dump config command
    #r.publish("pjterm/alice/ctrl", "CLOSE")
    r.publish("pjterm/alice/stdin","d")


    time.sleep(3)


    # close all terminals
    i = r.publish("pjterm/-/close", "")
    time.sleep(5)

    # send quit command directly to alice
    #r.publish("pjterm/alice/stdin", "q")


    #time.sleep(5)



    # send to kill remote pjterm
    #i = r.publish("pjterm/alice/ctrl","KILL")

    # send to kill local relay
    i = r.publish("pjterm/alice/stdlog", "KILL")

    time.sleep(1)

    # send exit signal to pjterm_session
    i = r.publish(session_exit_channel , "")


    time.sleep(3)



    print("Done")
