format: v1

platform:
  name: demo
  default_profile: default
  default_enterprise: enterprise_1
  default_site: site_1_1

common:
  # common profile attributes
  #domain: bluebox
  platform_name: demo
  platform_version: demo_qualif
  #proxy: 192.168.1.50:5060
  escape_prefix: ""
  escape_prefix_explicit: "0"
  media_dir: '/usr/share/sounds/syprunner'

profiles:

  default:
    collection: syprunner_agents
    password: '1234'
    platform_name: demo
    platform_version: demo_qualif

    proxy: "192.168.1.50:5060"
    domain: bluebox
    registrar: bluebox

    escape_prefix: ""
    escape_prefix_explicit: "0"

    options:
      "--norefersub": ""
      "--no-vad": ""
      "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
      "--stdout-refresh": 4

  local:
    # for pjsip terminals with no registration
    collection: syprunner_agents
    password: ''
    username: ''
    id: ''
    realm: ''
    registrar: ''
    domain: ''
    proxy: ''
    options:
      "--norefersub": ""
      "--no-vad": ""
      "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
      "--stdout-refresh": 4
      "--ip-addr": "192.168.99.100"
      "--no-tcp": ""
      "--auto-update-nat": "0"

  bluebox:
    # for pjsip terminals registering on demo platform bluebox
    collection: syprunner_agents
    password: '1234'
    platform_name: demo
    platform_version: demo_qualif

    proxy: "192.168.1.50:5060"
    domain: bluebox
    registrar: bluebox

    escape_prefix: ""
    escape_prefix_explicit: "0"

    options:
      "--norefersub": ""
      "--no-vad": ""
      "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
      "--stdout-refresh": 4

  demo:
    # for pjsip terminals registering on demo platform bluebox
    collection: syprunner_agents
    password: '1234'
    platform_name: demo
    platform_version: demo_qualif

    #domain: *
    #registrar: 192.168.99.100:5060
    registrar: 192.168.99.100

    escape_prefix: ""
    escape_prefix_explicit: "0"

    options:
      "--norefersub": ""
      "--no-vad": ""
      "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
      "--stdout-refresh": 4
      "--ip-addr": "192.168.99.100"
      "--no-tcp": ""
      "--auto-update-nat": "0"


  fox:
      # for pjsip terminals registering on demo platform bluebox
      collection: syprunner_agents
      password: '1234'
      platform_name: demo
      platform_version: demo_qualif

      #domain: *
      registrar: 192.168.1.51

      escape_prefix: ""
      escape_prefix_explicit: "0"

      options:
        "--norefersub": ""
        "--no-vad": ""
        "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
        "--stdout-refresh": 4
        "--ip-addr": "192.168.1.51"
        "--no-tcp": ""
        #"--auto-update-nat": "0"


  freeswitch:
      # for pjsip terminals registering on demo platform bluebox
      collection: syprunner_agents
      password: '1234'
      platform_name: demo
      platform_version: demo_qualif

      #domain: *
      registrar: 192.168.1.51

      escape_prefix: ""
      escape_prefix_explicit: "0"

      options:
        "--norefersub": ""
        "--no-vad": ""
        "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
        "--stdout-refresh": 4
        "--ip-addr": "192.168.99.100"
        "--no-tcp": ""
        #"--auto-update-nat": "0"
        "--bound-addr": "192.168.99.100"


  ims:
    # for pjsip terminals registering on an ims platform ( btic/btelu )
    collection: syprunner_agents

    platform_name: "PreprodPPR24"
    platform_version": "miso62"

    proxy: "80.12.10.69:5060"
    domain: "btelu.test.fr"
    registrar: "sip:btelu.test.fr"

    escape_code: ""
    escape_code_explicit: "0"

    media_dir: "/home/vagrant/Documents/var/robot/VNR-Tests"

    options:
      "--no-tcp": ""
      "--auto-update-nat": 0
      "--disable-via-rewrite": ""
      "--disable-rport": ""
      "--disable-stun": ""
      "--add-codec": "PCMA"
      "--norefersub": ""
      "--use-100rel": ""
      "--dis-codec": "PCMU/8000,speex/16000,speex/8000,GSM/8000,speex/32000,iLBC/8000,G722/16000"

  android:
    # for android devices
    collection: droydrunner_agents
    domain: "android"
    password: "0000"


  b2gas:
      # for pjsip terminals registering on b2gas platform
      collection: syprunner_agents
      password: '1234'
      platform_name: demo
      platform_version: demo_qualif

      #domain: *
      registrar: 192.168.99.100

      escape_prefix: ""
      escape_prefix_explicit: "0"

      options:
        "--norefersub": ""
        "--no-vad": ""
        "--dis-codec": "PCMU,speex,GSM,G722,iLBC"
        "--stdout-refresh": 4
        "--ip-addr": "192.168.99.100"
        "--no-tcp": ""
        #"--auto-update-nat": "0"




devices:
    # devices by alias

    alice:
      profile: fox
      username: "1001"
    bob:
      profile: fox
      username: "1002"


    Forest:
      profile: local
      #username: "+33146511105@local"
      username: "Forest"
      #id: "sip:Forest@local"


    Fanny:
      profile: local
      #username: "+33146511106@local"
      #id: "sip:Fanny@local"
      username: "Fanny"

    Alice:
      username: "+33146511101"
      id: "+33146511101"
      site: site_1_1

    Alice_noSDA:
      profile: default
      number: "6310"
      username: "P2361EVQ1_6310"
      site: site_1_1

    Mobby:
      profile: android
      username: "0915f9ece8942b04"
      id: "+33784109762"

    Marylin:
      profile: android
      username: "0915f9e0b3900c05"
      id: "+33784109764"

    U0001:
      profile: ims
      username: "53EB73KLTRIJ"
      password: "9617BCB2B7921CC5"
      display: "LASTU0001 Firstu0001"
      id: "+33482259961"

    U0002:
      profile: ims
      username: "C8063ALMLSUO"
      password: "273C0CB3F5748757"
      display: "LASTU0002 Firstu0002"
      id: "+33482259962"


    b2gas1:
      profile: b2gas
      username: "1001"

    b2gas2:
      profile: b2gas
      username: "1002"





enterprises:
  enterprise_1:
    CID: 0
    name: LPRISMEVQ1
  enterprise_2:
    CID: 0
    name: LPRISMEVQ2

sites:
  site_1_1:
    name: "2361EVQ1"
    enterprise: enterprise_1
    CallingLineIDGroupName: CLID_2361EVQ1
    CallingLineIDGroupNumber: "0146502509"
    LocationDialingCode: "11"

  site_1_2:
    name: "2361EVQ2"
    enterprise: enterprise_1
    CallingLineIDGroupName: CLID_2361EVQ2"
    CallingLineIDGroupNumber: "0146502519"
    LocationDialingCode: "12"

  site_1_virtual:
    name: "virtual"
    enterprise: enterprise_1

  site_2_1:
    name: 2361EVQ3
    enterprise: enterprise_2
    CallingLineIDGroupName: CLID_2361EVQ3
    CallingLineIDGroupNumber: "0146502530"
    LocationDialingCode: "21"


destinations:
  Alice_SD_2: 2
  Alice_SD_3: 3
  Alice_SD_4: 4
  Alice_noSDA_SD_2: 2
  Alice_noSDA_SD_3: 3
  Alice_noSDA_SD_4: 4
  wrong_Extension: "00000"
  wrong_FeatureAccessCode_1: "*100"
  wrong_FeatureAccessCode_2: "*72"
  wrong_PrivateNumber: "112527"
  wrong_Sid_E1S1: "112527"
  wrong_Sid_E1S2: "122518"
  oms_service: "sip:+33146511102@192.168.1.50;play=http_audio.example.net_song.wav"


FeatureAccessCode:
  "+CFA": ["*2011", "CFA Activation Call Forward Always"]
  "+CFB": ["*2021","CFB Activation , CallForward Busy Activation"]
  "+CFNA": ["*2031","CFNA Activation , Call Forward No Answer Activation"]
  "+CFNR": ["*2041","CFNR Activation,    Call Forward Not Reachable Activation"]
  "+CLIR": ["*3651","Call Line ID Delivery Blocking , per call, or Call Line Identity Restriction"]
  "-CFA": ["*2010", "Call Forward Always Deactivation"]
  "-CFB": ["*2020","CFB DeActivation , CallForward Busy DeActivation"]
  "-CFNA": ["*2030","CFNA DeActivation , Call Forward No Answer DeActivation"]
  "-CFNR": ["*2040","CFNR DeActivation,    Call Forward Not Reachable DeActivation"]
  "?CFA": ["*2012", "Call Forward Always Interrogation"]
  "?CFB": ["*2022","CFB Interrogation , CallForward Busy Interrogation"]
  "?CFNA": ["*2032","CFNA Interrogation , Call Forward No Answer Interrogation"]
  "?CFNR": ["*2042","CFNR Interrogation,    Call Forward Not Reachable Interrogation"]

