"""

    blueprint to handle http routing to hubs

"""
import flask
from flask import Blueprint,request,make_response

from restop_platform.models import Hub
from restop.client import RestopClient

#ALL_METHODS= ['GET', 'POST','PUT','PATCH','DELETE','HEAD','OPTIONS']
ALL_METHODS= ['GET', 'POST','PUT']
router= Blueprint('router','router')


@router.route('/', defaults={'path': ''}, methods=ALL_METHODS)
@router.route('/<collection>/', defaults={'path': ''}, methods=ALL_METHODS)
@router.route('/<collection>/<path:path>' ,methods=ALL_METHODS)
#@router.route('/<collection>/<device>/<path:path>' ,methods=ALL_METHODS)
def hub_router(collection='',path=''):
    """
        collect all url like /restop/api/v1/hub/*
    """
    app= flask.current_app
    journal= app.config['journal']
    journal.write("router url collection:%s, path:%s" % (collection,path))
    if collection:
        hub= Hub.get(Hub.name==collection)
        hub_url= hub.hub_url
        url= "%s/%s/%s" % (hub_url,collection,path)
        journal.write('redirect to [%s]' % url)
        client= RestopClient(base_url=url)
        if request.method == "POST":
            journal.write('method is POST')
            data= request.json
            data= data or {}
            journal.write('data is [%s]' % str(data))
            response= client.post(url,data=data)
            journal.write('response received from hub: status_code=%s, content_type: %s' % (
                response.status_code, response.headers['content-type'])
                          )
        elif request.method== "GET":
            response= client.get(url)
        else:
            return dict(message="router: unsupported method: %s" % request.method,status=500)

        #return "router: route to %s/%s/%s" % (hub_url,collection,path)
        return (response.text, response.status_code, response.headers.items())
        # status_code = response.status_code
        # content= response.content
        # response= make_response()

    return "hub routing to collection %s , path is %s" % (collection,path)

























































