import time
import subprocess
import os
import fcntl
import pexpect

#from publisher import Writer
import logging
logger= logging.getLogger('pjterm')

PJTERM_EXEC= "./pjterm"


class PexpectConnector(object):
    """

        a connector to write on a process

    """

    def __init__(self, name ,log=None,command_line='bash\n',**kwargs):
        """

        :param name: string  the queue_key eg queue:id:destination
        :param log:
        :param redis_db:
        """
        self._name = name
        self.log = log or logger
        self._driver = None

        for key, value in kwargs.iteritems():
            setattr(self, key, value)


        self.shell= True
        self.command_line= command_line
        self._buffer= []
        self._remain=""

    def open(self, timeout= 1,**kwargs):
        """ start shell process """
        if "command_line" in kwargs:
            command_line= kwargs.pop("command_line")
        else:
            command_line= self.command_line

        self.log.debug(("Pexpect " + command_line))

        self._driver= pexpect.spawn ( command_line ,timeout=int(timeout))

        # self._driver = subprocess.Popen(
        #     command_line,
        #     shell=self.shell,
        #     bufsize=0,
        #     stdin=subprocess.PIPE,
        #     stdout=subprocess.PIPE,
        #     universal_newlines=False
        # )
        time.sleep(2)
        #started = self._driver.poll()
        started =True
        self.log.debug("connector started=%s" % started)
        return True

    def close(self):
        """ close shell process """
        # force close pty
        self._driver.close(force=True)
        return self._driver.terminated


    def write(self, line,**kwargs):
        """
            write line to serial port

        :param line:
        :return:
        """
        if not line.endswith('\n'):
            line= line + b'\n'

        # IOError: [Errno 32] Broken pipe
        try:
            #self._driver.stdin.writelines(line)
            #self._driver.stdin.flush()
            self._driver.sendline(line)
        except IOError as e:
            self.log.error("IOError: %s" % e)


    def readline(self, wait= False,**kwargs):
        """

        :param wait:
        :param kwargs:
        :return:
        """
        line= None
        index = self._driver.expect(["\n",pexpect.EOF,pexpect.TIMEOUT])
        if index == 0:
            line= self._driver.before.rstrip("\r")
        elif index == 1:
            # EOF
            line=None
        elif index == 2:
            # TIMEOUT
            line= None
        return line


        # if wait:
        #     # blocking read
        #     line = self._driver.stdout.readline()
        #     return line
        # else:
        #     if self._buffer:
        #         # still things in buffer pop first
        #         return self._buffer.pop(0)
        #     else:
        #         # buffer is empty get some more
        #         #complete= False
        #         all= self._read()
        #
        #         if all == '':
        #             # empty
        #             return None
        #
        #         self._buffer= all.split('\n')
        #         if self._remain:
        #             self._buffer[0]= self._remain + self._buffer[0]
        #         self.remain=""
        #         if self._buffer[-1] == '':
        #             # complete: remote last blank line
        #             self._buffer.pop(-1)
        #         else:
        #             # last line was not complete: remember it
        #             self._remain= self._buffer.pop(-1)
        #         return self._buffer.pop(0)

    # def _read(self):
    #     """
    #         non blocking read
    #     :param wait:
    #     :return:
    #     """
    #     output= self._driver.stdout
    #     fd= output.fileno()
    #     fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    #     fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    #     try:
    #         return output.read()
    #     except:
    #         return ""


if __name__ == "__main__":

    REDIS_HOST= "192.168.1.21"
    REDIS_DB= 0
    REDIS_PORT= 6379

    REDIS_URL= "redis://192.168.1.21/0"

    PUBSUB= True


    logging.basicConfig(level=logging.DEBUG)



    #logger= get_logger(r ,"pjterm/alice")

    c = PexpectConnector("pjterm",log=logger ,command_line="bash")
    c.open()

    r= c.write("ls -l")

    line= c.readline()
    print(line)
    while line is not None:
        line = c.readline()
        print(line)



    c.close()

    time.sleep(5)

    print("Done")
