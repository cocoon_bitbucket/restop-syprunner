#!/bin/sh
exec pjterm \
 --app-log-level=4 \
 --id=sip:Fanny \
 --username=Fanny \
 --local-port=5061 \
 --ip-addr=192.168.99.100 \
 --max-calls=4 \
 --log-level=5 \
 --realm=* \
 --rec-file=rec-userA.wav \
 --rtp-port=20000 \
 --stdout-refresh=4 \
 --norefersub  \
 --null-audio  \
 --no-vad --no-tcp  \
 --dis-codec PCMU --dis-codec speex --dis-codec GSM --dis-codec G722 --dis-codec iLBC \
 --auto-answer=200
