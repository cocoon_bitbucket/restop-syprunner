#!/bin/sh

# pjsua --null-audio --local-port 5061 --rtp-port 4000  --id=sip:1000@192.168.1.51 --ip-addr=192.168.99.100 --username=1000 --password=1234 --registrar=sip:192.168.1.51 --realm=* --auto-answer=200
# sip:1002@192.168.1.51
exec pjterm \
  --local-port 5061 \
  --rtp-port 4008 \
  --id=sip:1001@192.168.99.100 \
  --ip-addr=192.168.99.100 \
  --username=1001 \
  --password=1234 \
  --realm=* \
  --log-level 4 --app-log-level 4 \
  --null-audio \
  --no-tcp \
  --auto-answer=200