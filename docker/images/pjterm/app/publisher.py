"""

    a redis publisher/pusher and logger


    usage:


    POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    writer= Writer( pool, name= "alice" , root= "pjterm" )

    #
    # pubsub usage
    #
    publisher= p .get_writer(pubsub=True)
    # publish to pjterm/alice/stdin
    publisher.write("stdin","hello")

    log= publisher.get_logger()
    # log to pjterm/alice/stdlog
    log.debug("hello")

    #
    # push usage
    #
    publisher= p .get_writer(pubsub=False)
    # add to queue  pjterm:container:stdin:id:alice
    publisher.write("stdin","hello")

    log= publisher.get_logger()
    # add to queue  pjterm:container:stdlog:id:alice
    log.debug("hello")



"""
import redis


#
# class logger
#
class Rlogger(object):
    """
        a redis logger ( pubsub or push )

    """
    stdlog = "stdlog"

    def __init__(self, parent):
        """

        :param parent: instance of writer ( or has a write(channel,message) method
        """
        self.parent = parent

    def _log(self, message):
        """

        :param message:
        :return:
        """
        return self.parent.write(channel_name=self.stdlog, message=message)

    info = _log
    debug = _log
    error = _log
    warn = _log


class Writer(object):

    """

        publish to $root/$name/channel

        push to $root:container:channel:$root:id:$name


    """
    def __init__(self,pool,name,root="pjterm"):
        """

        :param pool: instance of redis ConnectionPool
        :param name:
        :param root_name:
        """
        self.pool=pool
        self.name= name
        self.root= root

        self.cnx= redis.Redis(connection_pool=pool)

    def write(self,channel_name,message):
        """

        :param channel_name:
        :param message:
        :return:
        """
        raise NotImplementedError

    def get_writer(self,pubsub=True):
        """

        :param pubsub:
        :return:
        """
        if pubsub:
            return Publisher(self.pool,self.name,self.root)
        else:
            return Pusher(self.pool,self.name,self.root)

    def get_logger(self):
        """

        :return:
        """
        return Rlogger(self)


    #
    #  pubsub
    #

    def channel(self,channel_name):
        """

        :param channel_name:
        :return:
        """
        return "%s/%s/%s" % (self.root,self.name,channel_name)


    def publish(self,channel_name,message):
        """

        :param channel_name:
        :param message:
        :return:
        """
        return self.cnx.publish(self.channel(channel_name),message)


    #
    #  queue
    #

    def queue_name(self,channel_name):
        """

        :param channel_name: str eg pjterm:container:stdlog:pjterm:id:alice
                    to be compatible with walrus
        :return:
        """
        return "%s:container:%s:%s:id:%s" % (self.root,channel_name,self.root,self.name)


    def push(self, channel_name, message):
        """

        :param channel_name:
        :param message:
        :return:
        """
        key = self.queue_name(channel_name)
        return self.cnx.lpush(key, message)


    def clear(self, *channel_name):
        """

        :param channel_name:
        :return:
        """
        for ch in channel_name:
            key = self.queue_name(ch)
            self.cnx.delete(key)


    def size(self, channel_name):
        """

        :param channel_name:
        :return:
        """
        key = self.queue_name(channel_name)
        return self.cnx.llen(key)





class Publisher(Writer):
    """

    """
    def write(self,channel_name,message):
        """

        :param channel_name:
        :param message:
        :return:
        """
        return self.publish(channel_name,message)

class Pusher(Writer):
    """

    """
    def write(self,channel_name,message):
        return self.push(channel_name,message)




if __name__=="__main__":
    #REDIS_HOST = "192.168.1.21"
    REDIS_HOST = "127.0.0.1"
    REDIS_DB = 0
    REDIS_PORT = 6379


    POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    p =Writer(POOL, name="alice", root="pjterm")

    channel= p.channel("stdlog")
    assert channel == 'pjterm/alice/stdlog'
    queue_name= p.queue_name(("stdlog"))
    assert queue_name == 'pjterm:container:stdlog:pjterm:id:alice'

    r = p.clear("stdlog")
    assert p.size("stlog") == 0

    r= p.push("stdlog","hello")
    r = p.push("stdlog", "world")
    r= p.size("stdlog") == 2
    r= p.clear("stdlog")
    assert p.size("stlog") == 0

    r= p.publish("stdlog","hello there")


    ps= p .get_writer(pubsub=True)
    log= ps.get_logger()

    log.debug("hello")


    ps= p .get_writer(pubsub=True)
    log= ps.get_logger()

    log.debug("hello")

    ps = p.get_writer(pubsub=False)
    log = ps.get_logger()

    log.debug("hello")



    print("Done")