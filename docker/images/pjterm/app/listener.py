

import threading
import time
from publisher import Writer
from pjterm_connector import PJTERM_EXEC


import logging
logger= logging.getLogger('pjterm')

#PJTERM_EXEC= "./pjterm"


class Listener(threading.Thread):
    """
        a redis pubsub listener

    """
    #root_channel= "pjterm"


    def __init__(self, writer, connector):
        """

        :param writer: instance of publisher.Writer
        :param connector: instance of ProcessConnector (pjterm)
        """
        threading.Thread.__init__(self)
        self.writer= writer
        self.terminal_name= writer.name
        self.pubsub = self.writer.cnx.pubsub()
        for ch in ["ctrl","stdin"]:
            self.pubsub.subscribe(self.writer.channel(ch))
        self.connector = connector
        self.done= False

        # setup the stdout thread
        self.stdout_thread = threading.Thread(target=self.relay_stdout, args=())
        self.stdout_thread.daemon = True

    # def channel(self,channel_name):
    #     """
    #
    #     :param channel_name:
    #     :return:
    #     """
    #     return "%s/%s/%s" % (self.root_channel,self.terminal_name,channel_name)

    def log(self,message):
        """

        :param message:
        :return:
        """
        self.writer.write("stdlog",message)
        # message= "%s: %s" % (self.terminal_name,message)
        # self.redis.publish(self.channel("stdlog"),message)
        # #print(message)

    def work(self, item):
        """

        :param item:
        :return:
        """
        #pprint(item)
        if item["type"] == "message":
            # messages
            self.log("Message is: %s" % item["data"])
            if item["channel"].endswith("/ctrl"):
                # this is a control message
                self.control(item["data"])
            elif item["channel"].endswith("/stdin"):
                # this is message for pjterm stdin
                self.relay_stdin(item["data"])
            else:
                # unkwonw channel
                self.writer.write("stderr","unknown channel: %s" % item["channel"])


        elif item["type"].endswith("subscribe"):
            # assume a subscribe message
            self.log("Subscribtion confirmation %s to channel %s" % (item["type"],item["channel"]))


        #print(item['channel'], ":", item['data'])

    def run(self):
        """



        :return:
        """
        # start relay_stdout thread
        self.log("START LISTENING")

        self.done= False
        while not self.done:
            item= self.pubsub.get_message(timeout=5)
            if item:
                self.work(item)
            # else:
            #     print("no item")

        self.pubsub.unsubscribe()
        self.log("unsubscribed and finished")


    def control(self,message):
        """

        :param message: a json structure  <string> <dict>
        :return:
        """
        self.log("RECEIVE CONTROL message: %s" % message)
        if message.upper().startswith("OPEN "):
            return self.open(message)

        elif message.upper().startswith("CLOSE"):
            return self.close(message)

        elif message == "KILL":
            self.log("receive KILL signal")
            self.done= True


    def relay_stdin(self,message):
        """

            redirect message from channel pjterm/user/stdin to connector stdin
        :param message:
        :return:
        """
        #self.log("STDIN")
        self.connector.write(message)



    def relay_stdout(self):
        """

            read pjterm lines and publish it to redis channel /pjterm/alice/stdout

        :return:
        """
        while not self.done:
            line= self.connector.readline(wait=True)
            # publish it to stdout channel
            if line:
                self.writer.write("stdout",line)
            else:
                # when broken pipe connector.readline return None
                # self.log("received line from connector: [%s]" % line)
                # slow down the process
                self.log("connector BROKEN PIPE")
                self.done= True
                time.sleep(1)


    def open(self,line):
        """

        :param line: str eg  OPEN --id alice
        :return:
        """
        options = line[len("OPEN "):]
        command_line= PJTERM_EXEC + " " + options
        # start the pjterm connector
        r= self.connector.open(command_line=command_line)

        # start the loop to copy stdout
        self.stdout_thread.start()
        self.log("terminal started: %s" % r)


    def close(self,line):
        """

        :param line:
        :return:
        """
        # send quit message [q] to terminal
        self.log("send quit message to connector")
        self.connector.write("q")




if __name__ == "__main__":

    import redis

    REDIS_HOST= "192.168.1.21"
    REDIS_DB= 0
    REDIS_PORT= 6379

    REDIS_URL= "redis://192.168.1.21/0"


    logging.basicConfig(level=logging.DEBUG)

    pool = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    r = redis.Redis(connection_pool= pool)

    ping = r.ping()
    logging.log(logging.DEBUG,"redis ping is :%s" % ping)


    writer= Writer(pool,name="alice", root="pjterm")
    publisher= writer.get_writer(pubsub=True)



