#!/usr/bin/env python

"""


        pjterm_manager  listen to channels

            pjterm/-/open   alice bob   to start terminal listeners
            pjterm/-/close              to close all active terminals

            pjterm/alice/open  : to start effective terminal
            pjterm/alice/close  : to close terminal

        publish to
            pjterm/-/stdlog




"""

import os
import threading
import time
from pprint import pprint
from publisher import Writer
from listener import Listener
from pjterm_connector import ProcessConnector
from pjterm_connector import PJTERM_EXEC

#PJTERM_EXEC= "pjterm"


import logging
logger= logging.getLogger('pjterm')




PUBSUB = True


def get_observer(r , root="pjterm"):
    """
        objserver for pjterm/* channels

    :param r:
    :return:
    """
    pubsub = r.pubsub()
    pubsub.psubscribe("%s/*" % root)

    def _observer():
        """

        :return:
        """
        for message in pubsub.listen():
            pprint(message)

    observer = threading.Thread(target=_observer, args=())
    observer.daemon = True
    return observer





class Manager(threading.Thread):
    """
        a redis pubsub listener

    """
    #root_channel= "pjterm"


    def __init__(self, writer):
        """

        :param writer: instance of publisher.Writer
        :param connector: instance of ProcessConnector (pjterm)
        """
        threading.Thread.__init__(self)
        self.writer= writer
        self.terminal_name= writer.name
        self.pubsub = self.writer.cnx.pubsub()
        for ch in ["open","close"]:
            self.pubsub.subscribe(self.writer.channel(ch))
        self.done= False

        self.terminals= []

        # setup the stdout thread
        #self.stdout_thread = threading.Thread(target=self.relay_stdout, args=())
        #self.stdout_thread.daemon = True


    def log(self,message):
        """

        :param message:
        :return:
        """
        self.writer.write("stdlog",message)

    def work(self, item):
        """

        :param item:
        :return:
        """
        #pprint(item)
        if item["type"] == "message":
            # messages
            self.log("Message is: %s" % item["data"])
            if item["channel"].endswith("/open"):
                # this is a control message
                self.open(item["data"])
            elif item["channel"].endswith("/close"):
                # this is message for pjterm stdin
                self.close(item["data"])
            else:
                # unkwonw channel
                self.writer.write("stderr","unknown channel: %s" % item["channel"])


        elif item["type"].endswith("subscribe"):
            # assume a subscribe message
            self.log("Subscribtion confirmation %s to channel %s" % (item["type"],item["channel"]))


        #print(item['channel'], ":", item['data'])

    def run(self):
        """



        :return:
        """
        # start relay_stdout thread
        self.log("MANAGER START LISTENING")
        #self.stdout_thread.start()

        self.done= False
        while not self.done:
            item= self.pubsub.get_message(timeout=5)
            if item:
                self.work(item)
            # else:
            #     print("no item")

        self.pubsub.unsubscribe()
        self.log("MANAGER unsubscribed and finished")


    def open(self,message):
        """
            open a session : create terminals

        :param message:
        :return:
        """
        # close previous session if any
        self.clear()

        if message:
            # split terminal names
            terminals= message.split(" ")
            # start a terminal for each user
            for terminal in terminals:
                terminal= terminal.strip()
                self.log("starting terminal %s" % terminal)
                writer= Writer(self.writer.pool,name=terminal,root=self.writer.root)
                publisher = writer.get_writer(pubsub=PUBSUB)
                logger = self.writer.get_logger()
                connector = ProcessConnector(self.writer.root, log=logger)
                pjterm_listener = Listener(publisher, connector)
                pjterm_listener.start()
                self.log("terminal %s started" % terminal)
                #time.sleep(0.1)
            self.terminals=terminals
        self.log("all terminal started")



    def close(self,message):
        """
            close a session : delete terminals

        :param message:
        :return:
        """
        if self.terminals:
            self.log("closing terminals")
            for terminal in self.terminals:
                # send a close message to terminal listener
                writer = Writer(self.writer.pool, name=terminal, root=self.writer.root)
                publisher = writer.get_writer(pubsub=PUBSUB)
                publisher.write("ctrl","CLOSE")

            self.terminals=[]

    def clear(self):
        """
                clear the previous session ( and kill any pjterm process )
        :return:
        """
        self.close("clear all")
        os.system("pkill %s" % PJTERM_EXEC)




if __name__ == "__main__":

    import os
    import redis

    #REDIS_HOST= "192.168.1.21"

    if "REDIS_HOST" in os.environ:
        REDIS_HOST= os.environ["REDIS_HOST"]
    else:
        REDIS_HOST= "redis"

    #REDIS_HOST= "192.168.99.100"
    #REDIS_HOST = "redis"


    REDIS_DB= 0
    REDIS_PORT= 6379


    logging.basicConfig(level=logging.DEBUG)
    logging.log(logging.INFO,"open redis connection at :%s" % REDIS_HOST)

    pool = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    r = redis.Redis(connection_pool= pool)

    ping = r.ping()
    logging.log(logging.INFO,"redis ping is :%s" % ping)


    observer = get_observer(r, "pjterm")
    observer.start()


    writer= Writer(pool,name="-", root="pjterm")
    publisher= writer.get_writer(pubsub=True)


    manager = Manager(publisher)
    manager.start()

    # r = redis.Redis(connection_pool=pool)
    # r.publish("pjterm/-/open", "alice")
    #
    # time.sleep(1)
    # r.publish("pjterm/alice/ctrl", "OPEN --log-level=5")


    manager.join()

