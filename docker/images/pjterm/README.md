pjterm-manager
==============
a process to spawn pjterm-device



build the image
===============

docker build --no-cache -t cocoon/pjterm .


start the pjterm manager
========================
docker run --rm --name pjterm --net=host  -e REDIS_HOST=192.168.1.21   cocoon/pjterm



usage
=====
we comunicate with the manager and the terminal via redis pubsub channels
like  
* pjterm/-/ctrl

* pjterm/alice/ctrl
* pjterm/alice/stdin



create terminals
----------------

=> pjterm/-/open    Alice Bob

log on pjterm/-/stdlog


start terminal Alice
--------------------

=> pjterm/alice/ctrl OPEN --id alice --username alice --local-port 5061

log on pjterm/alice/stdlog
out on pjterm/alice/stdout


start terminal Bob
------------------

=> pjterm/Bob/ctrl OPEN --id bob --username  bob --local-port 5062

log on pjterm/bob/stdlog
out on pjterm/bob/stdout

close terminals
---------------

=> pjterm/-/close

manager send quit message to all terminals  
     pjterm/alice/stdin q
     pjterm/bob/stdin q
    



