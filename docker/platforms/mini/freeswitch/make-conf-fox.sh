#!/usr/bin/env bash

# fox address is 192.168.1.26


# delete existing conf
rm -rf conf/*

# initialize conf from ci-minimal
cp -rf conf-origin/* conf/

# replace 192.168.99 with new ip address
#ls conf/*.xml | xargs sed -i '' 's/192.168.99.100/192.168.1.26/g'

sed -i 's/192.168.99.100/192.168.1.26/g' conf/vars.xml