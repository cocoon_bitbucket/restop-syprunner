*** Settings ***
Documentation     standard tests
...

# libraries
Library  restop_client


#Suite Setup  init suite
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

${platform_url}=  http://192.168.99.100:5000/restop/api/v1
#${platform_url}=  http://192.168.1.26:5000/restop/api/v1


#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***

Unit call absolute
	[Arguments] 	${user1}  ${user2}  ${destination}
	[Documentation] 	simple call


  Open session 	${user1}   ${user2}


  call  ${user1}  destination=${destination}

  wait_incoming_call  ${user2}  timeout=20


  answer_call  ${user2}  code=200  timeout=20

  check_call  ${user1}

  hangup  ${user1}

  wait_hangup  ${user2}

  Close Session



Unit Call User
	[Arguments] 	${user1}  ${user2}
	[Documentation] 	simple call


  Open session 	${user1}   ${user2}


  #call  ${user1}  destination=sip:1002@192.168.99.100
  call user  ${user1}  userX=${user2}  format=ext

  wait_incoming_call  ${user2}  timeout=20


  answer_call  ${user2}  code=200  timeout=20

  check_call  ${user1}

  hangup  ${user1}

  wait_hangup  ${user2}

  Close Session

*** Test Cases ***

syprunner demo call absolute
  [Template]  Unit Call Absolute
  alice  bob  sip:1002@192.168.99.100



syprunner demo call user
  [Template]  Unit Call User
  alice  bob
  bob  alice


#syprunner b2gas simple call
#  [Template]  Unit Call User
#  b2gas1  b2gas2
#  b2gas2  b2gas1






