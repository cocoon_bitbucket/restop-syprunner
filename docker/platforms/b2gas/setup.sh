#!/usr/bin/env bash


echo "build images"

echo "build pjterm image: cocoon/pjterm"
docker build --no-cache --force-rm -t cocoon/pjterm ../../images/pjterm


echo "build restop image: cocoon/restop"
docker build --no-cache --force-rm -t cocoon/restop ../../images/restop-alpine


echo "build syprunner image: cocoon/restop-syprunner"
docker build --no-cache --force-rm -t cocoon/restop-syprunner ../../images/syprunner


#echo "fetch freeswitch base  image: praekeltfoundation/freeswitch"
#docker pull praekeltfoundation/freeswitch