
import logging
import os

from flask.config import Config
from restop.client import RestopClient
from restop_platform import Dashboard
from restop_platform.models import Apidoc
from restop_platform.models import Database, Model

from application import start_server
from syprunner.models import DEVICE_NAME
from syprunner.models import SypPlatform as Platform
from pjsip_device.session import PjtermSession
# from _resources import DeviceResource
from syprunner.resources import Resource_syprunner_agents as DeviceResource

log= logging.getLogger(DEVICE_NAME)
log.setLevel(logging.DEBUG)

#logging.basicConfig()

root_path= dir_path = os.path.dirname(os.path.realpath(__file__))


# get config from local config.cfg
config= Config(root_path)
config.from_pyfile('config.cfg')
master_url= config['RESTOP_MASTER_URL']
hub_name= config['RESTOP_HUB_NAME']
platform_name= config.get('RESTOP_PLATFORM_NAME','default')

# create client to master
master= RestopClient(base_url=master_url)

# fetch platform data from master
url= master.url_operation(collection='platforms',item_id=1,operation='get_config')
log.debug('fetch platform data from master at %s' % url)
response= master.post(url)
if response.status_code == 200:
    platform_data= response.json()
else:
    raise RuntimeError('cannot fetch platform at %s' % url)

# # fetch hub name for self
# url= master.url_operation(collection='platforms',item_id=1,operation='get_hub')
# log.debug('fetch hub data from master at %s' % url)
# response= master.post(url,data= { 'name': hub_name})
# if response.status_code == 200:
#     hub_data= response.json()
# else:
#     raise RuntimeError('cannot fetch hub data from master at %s' % url)

# register hub to master
url= master.url_operation(collection='platforms',item_id=1,operation='register')
log.debug('register to master at %s' % url)
response= master.post(url,data= { 'name': hub_name})
if response.status_code == 200:
    hub_data= response.json()
else:
    raise RuntimeError('cannot register to master at %s' % url)

redis_host= hub_data['redis_host']
redis_port= hub_data['redis_port']
redis_db= hub_data['redis_db']

hub_host= hub_data['host']
hub_port= hub_data['port']


# ...
log.debug('set redis database and bind model')
db = Database(host=redis_host, port=redis_port, db=redis_db)
Model.bind(database=db, namespace=None)
dashboard= Dashboard(db)
dashboard.main['hub_ip']= hub_host
dashboard.main['hub_port']= hub_port
dashboard.main['hub_name']= hub_name


# get existing platform instance
try:
    p = Platform.get(Platform.name == platform_name)
except Exception as e:
    # does not exists create it
    log.debug('create platform')
    db.flushdb()
    p = Platform.create(name=platform_name, data=platform_data)
    p.setup(master=False)

# get local api doc and send it to master for update
log.debug('setup autodoc')
doc= DeviceResource._autodoc()
data = Apidoc.from_autodoc(doc,collections=[DEVICE_NAME],platform_name=platform_name)

# data={'operations':[],'collection_operations':[],'collections':[DEVICE_NAME]}
# for operation in doc['item_methods'].keys():
#     op = operation.replace('op_','')
#     data['operations'].append(op)
# for operation in doc['collection_methods'].keys():
#     op = operation.replace('op_col_','')
#     data['collection_operations'].append(op)

# store local apidoc
log.debug('create or update local apidoc')
api = Apidoc.create_or_update(platform_name=platform_name,
                    session_collection='sessions',
                    collections=data['collections'],
                    collection_operations=list(data['collection_operations']),
                    operations=list(data['operations'])
                    )

# update master
url= master.url_item(collection='apidoc',item_id='samples')
log.debug('update master apidoc at %s' % url)
response= master.post(url,data= data)
if response.status_code == 200:
    pass
else:
    raise RuntimeError('cannot update doc to master')

# start pjterm Listener
log.info('start pjterm Listener')
pjterm_session = PjtermSession(cnx=db)
pjterm_session.create_session_listener(cnx=db)
# close an eventual ongoing session
pjterm_session.close_session()
#session = PjtermSessionListener( db, root="pjterm")
#session.start()



log.debug('start hub server')
if hub_host== 'localhost':
    # publish on all interface instead of local host
    hub_host= '0.0.0.0'
start_server(host=hub_host, port=hub_port, debug=False, threaded=True)

#print 'Done'