import re

from restop_platform.resources import AgentResource


class Resource_pjterm_agents(AgentResource):
    """
        proxy to syprunner

        start: launch a syprunner process and communicate with it via redis queues

        stop: stop the thread and syprunner process

        send:  send input to syprunner stdin queue
        read : read syprunner output via stdout queue

    """
    collection= 'pjterm_agents'


    first_line= r'os_core_unix\.c \!pjlib 2[^ ]* for POSIX initialized'
    last_line=  r'pjsua_core\.c  \.PJSUA destroyed\.\.\.'

    registred= ".* registration success, status=(\d+) (.*)"

    #
    # generic agent interface
    #

    def op_start(self,item,**kwarg):
        """

        :param item:
        :param kwarg:
        :return:
        """
        # select syprunner
        data= self.request.json
        data= data or {}

        self.started= False

        # retrieve agent data
        agent = self.model.load(item)
        try:
            command_line= agent.parameters['command_line']
        except KeyError as e:
            raise RuntimeError("no command line for syp agent ")
        #agent_data=self.backend.item_get(self.collection,item)
        #command_line= agent_data['parameters']['command_line']

        # search for registrar in command line
        registrar= None
        try:
            cl= command_line.split(' ')
            i= cl.index('--registrar')
            registrar= cl[i+1]
        except Exception as e:
            # no registrar
            pass


        self.started= self.check_process(item,registrar=registrar)

        #  create a
        #adapter= self.backend.adapters_get_client(agent_key)
        #agent_indice= "user" +  chr( 65 + agent_data['agent_indice'])
        # create syprunner proxy
        #proxy= PjsipAgent(agent_indice,agent_data['alias'],adapter)
        #self._save_agent_proxy(item,proxy)
        return self.started

    #
    #
    # def check_process(self,item,registrar= None):
    #     """
    #         check we can communicate with the syprunner process
    #     :return:
    #     """
    #     # send dump status  d\n
    #     # receive status
    #     #agent_key= self.backend.item_key(self.collection,item)
    #     #client = self.backend.adapters_get_client(agent_key)
    #
    #     if registrar:
    #         # wait for registration
    #         pattern= self.registred
    #         timeout= 10
    #     else:
    #         # no registration: wait for first line
    #         pattern= self.first_line
    #         timeout= 3
    #
    #
    #     r= self.model.expect(item,data= dict(pattern=pattern,timeout=timeout))
    #     if "=== found " in r[-1]:
    #         return True
    #     else:
    #         return False
    #
    #
    # def op_stop(self,item,**kwargs):
    #     """
    #
    #         shutdown a terminal:  send q,
    #
    #     :param item:
    #     :param kwargs:
    #     :return:
    #     """
    #
    #     data= self.request.json
    #     data= data or {}
    #
    #     agent = self.model.load(item)
    #     #agent_data=self.backend.item_get(self.collection,item)
    #
    #
    #     #agent_key= self.backend.item_key(self.collection,item)
    #     #client = self.backend.adapters_get_client(agent_key)
    #
    #
    #     res= dict( result='204',message= 'agent stopped', logs=[])
    #
    #     #send quit to syprunner process
    #     #r= client.send('q')
    #     #r= client.send('q')
    #     agent.stdin.append("q\n")
    #     agent.stdin.append("q\n")
    #
    #
    #     # wait for pjsua destroyed
    #     parameters= dict( pattern= self.last_line,timeout=10,cancel_on=[])
    #     #lines= self.expect(item, parameters)
    #     lines= self.expect(item,parameters)
    #     if lines:
    #         if '=== found' in lines[-1]:
    #             res['result']= 200
    #             res['message']= 'agent has been stopped'
    #     res['logs']= lines
    #
    #
    #     # send exit to stop the adapter
    #     #client.exit()
    #     agent.exit()
    #
    #     # update agent status
    #     agent.stopped= True
    #     agent.save()
    #     #agent_data['stopped']= True
    #     #self.backend.item_put(self.collection,item,agent_data)
    #
    #     # send dump status command
    #     #client.send('d')
    #     #r= self.expect(item,data= dict(pattern=self.first_line))
    #     #if "=== found " in r[-1]:
    #     #    return True
    #     #else:
    #     #    return False
    #     return res




