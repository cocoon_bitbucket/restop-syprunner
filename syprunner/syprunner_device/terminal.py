
from pjsip_device.api.full import PjtermLevelOne

from pjsip_device.adapter import PjtermAdapter

from restop_platform import Dashboard

from syprunner.models import SypPlatform as Platform
from syprunner.models import SypUser

"""


    an enhanced pjsip_device pjterm  ( pjsip_device.full.PjtermLevelOne )


    exploit platform data to provides services like call_user ...



"""

def to_sip(address):
    """
        add sip: prefix if not existant
    :param address:
    :return:
    """
    if not address.startswith("sip:"):
        return "sip:" + address
    return address



class PjtermLevelDb(PjtermLevelOne):
    """



    """
    collection = 'syprunner_agents'


    #  def __init__(self, name, role=None, adapter=None, echo=True, trace=True,
    #              have_reg=True, **parameters):


    def __init__(self, alias, **kwargs):
        """

        :param alias: string ( alice , bob ...)
        :param kwargs:
        """


        # set agent,   agent is the db model
        self.alias = alias
        try:
            self.model = SypUser.get(SypUser.name == alias)
        except ValueError as e:
            # not a member of session
            raise RuntimeError("no such sip agent: [%s] in this session " % alias)

        self.agent_id = self.model.get_hash_id()
        self.kwargs = kwargs

        self.redis_db = self.model.database
        self.dashboard = Dashboard(self.redis_db)

        self.state = {}

        #
        # setup platform info
        self._platform = Platform.get(Platform.name == 'default')
        self._platform_info = self._platform.get_info()

        # init
        self._adapter = PjtermAdapter(redis_db=self.redis_db, alias=self.alias)
        super(PjtermLevelDb, self).__init__(self.alias, adapter=self._adapter)

        return


    def call_user(self,userX, format="universal",**kwargs):
        """

        :param userX: string alice,bob ...
        :param format: string universal ext ...
        :param kwargs:
        :return:
        """

        user = SypUser.get(SypUser.name==userX)
        sip_address= user.sip_address_to_user(format= format)

        return self.call(destination= sip_address)


    #
    # to test
    #


    def call_feature_access_code(self,fac,userX=None,format=None):
        """

           call Application server with a feature access code

            eg  -CFA , -CFU

        """
        # input= self.request.json
        # call_feature_access_code= input['call_feature_access_code']
        # userX= input.get(input['userX'],None)
        # format= input.get('format',None)
        #fac_code= self._platform.featureAccessCode.get(fac)
        sip_address = self._platform.resolve_address(user=userX, fac=fac, format=format, to_sip=True)

        #sip_address= self.SessionClass.get_sip_address(user=userX,format=format,fac=call_feature_access_code)

        return self.call(destination= sip_address)



    def call_destination(self,destination,**kwargs):
        """

        :param user:
        :param destination:
        :return:
        """
        resolved_destination= self._platform.destination(destination)
        sip_address= resolved_destination

        return self.call(destination= sip_address)

    ## obsolete use call_destination instead ( ALICE_SD_1)
    # def call_sd(self,sd,**kwargs):
    #     """
    #          user call with direct sd8 key sd ( 1..8 )
    #
    #          @user: eg Alice Bob
    #          @sd: the sd8 direct key on the terminal ( eg 1 , 2  ... 8)
    #
    #     :param user:
    #     :param sd:
    #     :return:
    #     """
    #     sip_address= self._platform.sip_address_for(sd)
    #     return self.call(destination= sip_address)



    def transfer_to_user(self,userX,format="universal",**kwargs):
        """

           Unatented transfer call

            *userA* transfer call to *userX* with *format* (universal,national ...)

        :param userA:
        :param userX:
        :param format:
        :return:
        """
        # compute sip address for userX with format
        # input= self.request.json
        # userX= input['userX']
        # format= input.get('format','universal')
        #
        # conf = SypConfiguration.from_file(self.syprunner_configuration)
        # ptf= conf.get_ptf(self.platform_version)

        sip_address = self._platform.user(userX).sip_address_to_user(format)

        self.log("pilot: user %s transfer call to user %s with format %s  (%s)" % ( self.alias, userX, format ,sip_address))

        return self.transfer(destination= sip_address)


    def transfer_to_destination(self,destination,**kwargs):
        """
           transfer call (unatended) to a predefined destination (defined in platform.json)

            *@user*: eg Alice , Bob

            *@destination*:  the literal name of a destination defined in platform conf

        :param user:
        :param destination:
        :return:
        """
        resolved_destination = self._platform.destination(destination)
        sip_address= to_sip(resolved_destination)
        self.log("pilot: user %s transfer call to destination %s  (%s)" % (self.alias, destination, sip_address))
        return self.transfer(destination= sip_address)


    def activate_redirection_to_user(self,userB,redirect_kind="CFA",call_format="universal",**kwargs):
        """
           call Application Server using a Feature Access Code to redirect userA to userB

            *@userA*: the user to be redirected

            *@userB*: target of the redirection

            *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)

            *@call_format*: site,national,international,universal

            eg: activate redirection to user  Alice  Bob  CFA  national

        :param userA:
        :param userB:
        :param redirect_kind:
        :param call_format:
        :return:
        """
        fac_name= redirect_kind
        if not fac_name.startswith("+"):
            fac_name = "+" + redirect_kind

        sip_address= self._platform.resolve_address(user=self.alias,fac=fac_name,format=call_format,to_sip=True)
        self.log("pilot: user %s ask a call forward via feature access code %s to user %s with format %s (%s) " % (
            self.alias,redirect_kind,userB,call_format,sip_address))

        return self.call(destination= sip_address)


    def cancel_redirection(self,redirect_kind,**kwargs):
        """
           Convenience function to send a FAC sequence to AS to cancel a redirection

            *@userA*: the user to be redirected

            *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)

        :param user:
        :param redirect_kind:
        :return:
        """
        # input= self.request.json
        # redirect_kind= input['redirect_kind']
        #
        # conf = SypConfiguration.from_file(self.syprunner_configuration)
        # ptf= conf.get_ptf(self.platform_version)

        fac_name= redirect_kind
        if not fac_name.startswith("-"):
            fac_name = "-" + redirect_kind
        #sip_address = ptf.sip_address_for(fac_code)
        sip_address = self._platform.resolve_address(user=self.alias, fac=fac_name,format="universal", to_sip=True)
        self.log("pilot: call AS to cancel a redirection of type %s for user %s (%s)" %(redirect_kind,self.alias,sip_address))

        return self.call(destination=sip_address)



    def check_incoming_call(self,display_name,**kwargs):
        """
            checks display name on FROM header of the incoming INVITE message

            *@user*: str , the user receiving the call (eg Bob)

            *@display_name*: the display name to compare with ( or unkwown )

        :param user:
        :param display_name:
        :return:
        """
        # input= self.request.json
        # display_name= input['display_name']
        #
        # user_data= self.backend.item_get(self.collection,item)
        # user= user_data['alias']


        validation = self._check_caller(display=display_name)

        r= self._check_incoming_call(validation)

        return r
