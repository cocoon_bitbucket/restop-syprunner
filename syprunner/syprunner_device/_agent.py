
import json
from restop.application import ApplicationResponse,ApplicationError,CriticalError
from wbackend.logger import SimpleLogger as Logger
from restop_queues import QueueClient,QueueServerBase
from restop_platform import Dashboard
from restop_adapters.client import ClientAdapter
from syprunner.models import SypPlatform as Platform

from restop_adapters.adapter import exit_command
#from restop_adapters.adapter import ThreadedAdapter as AgentAdapter




class Agent(object):
    """

        an agent to drive a voip terminal ( syprunner )

        send commands and recevice response fomr adapter
        -----------


    """
    collection = 'syprunner_agents'

    def __init__(self, agent_model, **kwargs):
        """

        :param agent_model:
        :param kwargs:
        """
        self.model = agent_model
        self.agent_id = self.model.get_hash_id()
        self.kwargs = kwargs

        self.redis_db = self.model.database
        self.dashboard = Dashboard(self.redis_db)
        #self.log = Logger(self.model.logs.key, self.redis_db)
        self.log = Logger(self.model.stdlog.key, self.redis_db)

        self.state = {}

        self.log.info('%s: initialize' % self.agent_id)

        self.session_id = self.model.session
        self.alias = self.model.name
        self.device_data = self.model.parameters

        self._client = None
        #self._scheduler = None
        self._setup()

    def _setup(self):
        """

        """
        self._setup_platform()
        self._setup_client()

    def _setup_platform(self):
        """
        """
        # set platform_info
        self._platform = Platform.get(Platform.name == 'default')
        self._platform_info = self._platform.get_info()
        #self._station_peer_address = self._platform_info['peer_address']


    def _setup_client(self):
        """

        :return:
        """
        # start a client to access redis queues
        #self._client = ClientAdapter(self.agent_id, log=self.log, redis_db=self.redis_db)
        self._client= QueueClient(self.agent_id,self.log,redis_db=self.redis_db)



    def _send(self,operation,**kwargs):
        """
            send an operation to adapter via redis queue  (call , hangup)

        """
        r= self._client.send(operation,kwargs)



    #
    # control interface
    #
    def start(self):
        """
            start the agent adapter
        """
        adapter= AgentAdapter(self.model,log=self.log,run_mode='passive')
        adapter.start()
        return self.model.make_response(True)

    def stop(self):
        """
            send a message to adapter to stop
        """
        return self.model.make_response(True)

    #
    # client interface
    #
    def send(self, operation , **kwargs):
        """

            send a command to the device
        :param cmd: string
        """
        r = self._client.send(operation ,kwargs)
        return r

    def watch(self,timeout=5):
        """
            watch device output for a duration of <timeout>
        :param timeout: integer ,number of seconds to wait for
        :return:
        """
        lines= self._client.expect('Never Catch This',timeout=timeout)
        # add lines to log
        self.model.logs.extend(lines)
        return "OK"

    def expect(self,pattern,timeout=10,cancel_on=None, regex='no', **kwargs ):
        """
            read device output to find the pattern <pattern>

        :param pattern: string ,the pattern to search for
        :param timeout: integer, seconds to wait ( default is 5s)
        :return: string; OK ,FAILED or CANCELED
        """
        if str(regex).lower() in ['no','false','-1']:
            regex= False
        else:
            regex= True

        r = self._client.expect(pattern, timeout=timeout, regex=regex,cancel_on=cancel_on,**kwargs)
        last_line= r[-1]
        # add lines to log
        self.model.logs.extend(r)

        if '=== found' in last_line:
            return "OK"

        elif '=== timeout' in last_line:

            message = 'FAILED'

        elif '=== canceled' in last_line:
            message = 'CANCELED'
        else:
            message = 'FAILED ???'

        response= self.model.make_response(message,500)
        raise ApplicationError(**response)




class AgentAdapter(QueueServerBase):
    """

        read Agent stdin to execute commands


    """
    collection= "syprunner_agents"


    def run_once(self):
        """
            wait commands ( name, kwargs ) from stdin
        :return:
        """
        self.keep_alive()

        # read stdin and transfer to stdout
        if len(self.model.stdin):
            line = self.model.stdin.popleft()
            # interpret command   cmd <json arguments>
            if ' ' in line:
                command, arguments = line.split(' ', 1)
            else:
                command = line
                arguments = ""
            status, response = self.execute_command(command, arguments=arguments)
            self.model.stdout.append(json.dumps([status, response]))

        self.handle_signal()
        return



    def execute_command(self, command, arguments=None):
        """

        :param command: string,   q:tv ,
        :param content:
        :return:
        """
        command = command.strip('\n').strip()
        self.log.debug('received command: %s %s' % (command, arguments))
        response= ""
        if arguments:
            try:
                arguments = json.loads(arguments)
            except Exception  as e:
                response= 'cannot convert argument %s to json %s' % (arguments,e)
                self.log.debug(response)
                return False, response
        else:
            arguments = {}

        try:
            func = getattr(self.connector, command)
        except AttributeError:
            # not implemented
            response = "command [%s] not implemented" % command
            self.log.debug(response)
            return False,response

        status= True
        try:
            response = func(** arguments)
        except Exception as e:
            response = "execution raised: %s" % e
            self.log.error(response)
            status= False
        #response = json.dumps(response)
        self.log.debug('response is %s' % response)

        return status,response