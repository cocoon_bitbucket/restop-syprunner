__author__ = 'cocoon'

from restop.application import ApplicationError
from models import DEVICE_NAME
from models import LocalSession
from agent import Agent
from restop.plugins.resources import RootResource
from restop_platform.resources import AgentResource

class Resource_Root(AgentResource):
    """

    """
    collection= 'root'


class DeviceResource(object):
    """
        resource for syprunner_agents

        syprunner_agents/-/open
        syprunner_agents/-/close



    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass= Agent



    # @classmethod
    # def _autodoc(cls,**kwargs):
    #     """
    #
    #         generate autodocumentation
    #     :param kwargs:
    #     :return:
    #     """
    #     info = ScanMethods()
    #     info.add_docstring(cls)
    #     info.add_op_methods(cls)
    #     if cls._AgentClass:
    #         info.add_methods(cls._AgentClass)
    #     return info.result


    @property
    def model(self):
        return self.backend.select_plugin(self.collection)


    def configure_session(self,session_id,members,configuration):
        """

        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration

    def create_agent(self,alias,configuration=None,session_id=None):
        #
        agent=self.model.create(name=alias,parameters=configuration,session=session_id)
        return agent


    def collection_post(self, **kwargs):
        """
            create a sample object
        :return:
        """
        data = self.request.json or {}

        obj = self.model.create(**data)
        return True

    def item_get(self, item, **kwargs):
        """
        :param kwargs:
        :return:
        """
        obj = self.model.load(item)
        data = obj._data
        return repr(data)


    # def op_col_open_session(self, item, **kwargs):
    #     """
    #         open a local session
    #
    #         POST /hub/samples/-/open_session
    #
    #     :return:
    #     """
    #     #assert item == '-'
    #     agents= []
    #     data= self.request.json
    #     session_id= data['session_id']
    #     members= data['members']
    #     configuration=data['configuration']
    #     session_configuration= self.configure_session(session_id,members,configuration)
    #     # create agents
    #     for member_alias in members:
    #         agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
    #         agents.append(agent.get_id())
    #     # prepare  response
    #     data= { "agents": agents, "session_configuration":session_configuration}
    #
    #     return { 'result':200, 'message':data,'logs':['session open']}
    #
    #
    # def op_col_close_session(self, item, **kwargs):
    #     """
    #         close a local session
    #
    #         POST /hub/samples/-/close_session
    #
    #             parameters:  members
    #
    #     :return:
    #     """
    #     #assert item == '-'
    #     data= self.request.json
    #
    #     session_id= data['session_id']
    #     members= data['members']
    #
    #     message= "close session: %s , with members: %s" % (session_id,str(members))
    #
    #     return { 'result':200, 'message':data,'logs':[message]}

    def op_col_open_session(self, item, **kwargs):
        """
            open a local session

            POST /hub/samples/-/open_session

        :return:
        """
        #assert item == '-'
        agents= []
        data= self.request.json
        session_id= data['session_id']
        members= data['members']
        configuration=data['configuration']
        session_configuration= self.configure_session(session_id,members,configuration)

        # create local session
        local_session= LocalSession.create(session_id=session_id,data={'members':members,'configuration':configuration})

        # create agents
        for member_alias in members:
            agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
            agents.append(agent.get_id())
        # prepare  response
        data= { "agents": agents, "session_configuration":session_configuration}

        return { 'result':200, 'message':data,'logs':[]}

    def op_col_close_session(self, item, **kwargs):
        """
            close a local session

            POST /hub/samples/-/close_session

                parameters:  members

        :return:
        """
        #assert item == '-'
        data= self.request.json

        session_id= data['session_id']
        members= data['members']

        # load local session
        local_session=LocalSession.load(session_id)

        # for each agent of local session
        # call agent.stop


        rc= local_session.close()

        # local_session.status='closed'
        # local_session.save()
        #
        # message= "close session: %s , with members: %s" % (session_id,str(members))
        #
        # local_session.logs.append(message)

        response= local_session.make_response(rc)
        return response




    def op_dummy(self,item,**kwargs):
        """
            for test purpose
        :return: dictionary
        """
        return dict( result=200 , message="OK" , logs=['operation dummy called on item %s' % item])


    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            override standard dispatcher: search for a op_$operation method

        """
        # check if operation is a direct implementation = op_start , op_stop ...
        op= None
        if item == '-':
            # collection operation
            op_name = "op_col_" + operation
        else:
            # item operation
            op_name = "op_" + operation

        try:
            op= getattr(self,op_name)
        except AttributeError:
            pass
        if op :
            # call local operation op_*
            return op(item,**kwargs)

        # # check if operation is defined in Mobile
        # try:
        #     agent_func= getattr(self._AgentClass,operation)
        # except AttributeError:
        #     # operation does not exists on interface
        #     raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        # operation exists
        model = self.model.load(item)
        model.logs.append("agent [%s/%s] received [%s] command" % (self.model.collection,item,operation))
        agent = self._AgentClass(model)

        try:
            func= getattr(agent,operation)
        except AttributeError:
            # operation does not exists on interface
            raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )

        input_data= self.request.json
        if input_data:
            rc= func(**input_data)
        else:
            rc= func()
        #return rc
        return model.make_response(rc)







# class DeviceResource(AgentResource):
#     """
#         Samples : a demo Application
#
#     """
#     collection= DEVICE_NAME
#     protected= ''
#
#     _AgentClass= Agent
#
#
#
#
#
#
#
#     def configure_session(self,session_id,members,configuration):
#         """
#         :param session_id:
#         :param members:
#         :param configuration:
#         :return:
#         """
#         return configuration
#
#
#     # def op_col_open_session(self, item, **kwargs):
#     #     """
#     #         open a local session
#     #
#     #         POST /hub/samples/-/open_session
#     #
#     #     :return:
#     #     """
#     #     #assert item == '-'
#     #     agents= []
#     #     data= self.request.json
#     #     session_id= data['session_id']
#     #     members= data['members']
#     #     configuration=data['configuration']
#     #     session_configuration= self.configure_session(session_id,members,configuration)
#     #
#     #     # create local session
#     #     local_session= LocalSession.create(session_id=session_id,data={'members':members,'configuration':configuration})
#     #
#     #     # create agents
#     #     for member_alias in members:
#     #         agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
#     #         agents.append(agent.get_id())
#     #     # prepare  response
#     #     data= { "agents": agents, "session_configuration":session_configuration}
#     #
#     #     return { 'result':200, 'message':data,'logs':[]}
#     #
#     # def op_col_close_session(self, item, **kwargs):
#     #     """
#     #         close a local session
#     #
#     #         POST /hub/samples/-/close_session
#     #
#     #             parameters:  members
#     #
#     #     :return:
#     #     """
#     #     #assert item == '-'
#     #     data= self.request.json
#     #
#     #     session_id= data['session_id']
#     #     members= data['members']
#     #
#     #     # load local session
#     #     local_session=LocalSession.load(session_id)
#     #
#     #     # for each agent of local session
#     #     # call agent.stop
#     #
#     #
#     #     rc= local_session.close()
#     #
#     #     # local_session.status='closed'
#     #     # local_session.save()
#     #     #
#     #     # message= "close session: %s , with members: %s" % (session_id,str(members))
#     #     #
#     #     # local_session.logs.append(message)
#     #
#     #     response= local_session.make_response(rc)
#     #     return response
#
#     def op_dummy(self,item,**kwargs):
#         """
#          POST /restop/api/v1/hub/samples/1/dummy
#
#         :param item:
#         :param kwargs:
#         :return:
#         """
#         return dict( result=200 , message="OK" , logs=['operation dummy called on item %s' % item])
#
#
#
#
#     def op_raise_error(self,item,**kwargs):
#         """
#             raise an error
#
#         :param item:
#         :param kwargs:
#         :return:
#         """
#         x= 1/0
#         return
#
#     def op_not_implemented(self,item,**kwargs):
#         """
#
#         :param item:
#         :param kwargs:
#         :return:
#         """
#         model = self.model.load(item)
#         model.logs.append("agent [%s/%s] received not_implemented command" % (model.collection,item))
#         raise NotImplementedError('operation not implemented')
#
#     def op_application_error(self,item,**kwargs):
#         """
#
#         :param item:
#         :param kwargs:
#         :return:
#         """
#         logs=[]
#         model = self.model.load(item)
#         model.logs.append("agent [%s/%s] received application_error command" % (model.collection,item))
#         #logs= list(model.logs)
#         logs.append("agent [%s/%s] received application_error command" % (model.collection, item))
#         raise ApplicationError('an application error raised',500,logs=logs)
#
