#!/usr/bin/env python

__author__ = 'cocoon'
"""

    pseudo terminal

    read command from stdin

    write output to stderr



sample command line:

--app-log-level 4 --id sip:+33146511101@bluebox --null-audio  --local-port 5061 --max-calls 4 --log-level 5 --realm bluebox --rec-file rec-userA.wav --username 1000 --rtp-port 20000 --password 1234 --registrar sip:192.168.1.50:5060 --stdout-refresh 2




"""
import sys
import os

prompt = ''

users = [ 'sip:+33146511101@bluebox','sip:+33146511102@bluebox' ]
traces = [ 'userA.txt', 'userB.txt']

first_line = '16:10:00.523 os_core_unix.c !pjlib 2.3 for POSIX initialized'
last_line =  '16:10:33.945   pjsua_core.c  .PJSUA destroyed...'


here= os.path.realpath(os.path.dirname(__file__))


index_user_trace= None
# try:
#     # decode command line
#     index_id= sys.argv.index('--id') +1
#
#     # get --id parameter ( eg sip:1000@bluebox )
#     user_id= sys.argv[index_id]
#
#     index_user_trace= users.index(user_id)
# except:
#     print "Error: bad --id option"
#     sys.exit(-1)


if index_user_trace is not None:
    try:
        trace = os.path.join(here,'syprunner-samples',traces[index_user_trace])
        os.stat(trace)
    except :
        print "Error: no such file: %s" % trace
        sys.exit(-1)


print first_line
while 1:

    # read stdin
    command = raw_input(prompt)

    if command == 'exit':
        print "exit"
        print last_line
        break

    elif command== 'd':
        # dump status
        print "pong"

    elif command== 'dc':
        # dump config
        print "pong"

    elif command == 'ping':
        print "pong"

    elif command == "show":
        print "show command received"
        print "blabla"

    elif command.startswith('echo '):
        text = command[5:]
        print text

    elif command== 'trace':
        with open(trace,'rb') as fh:
            print fh.read()
            fh.close()

    elif command == 'call':
        pass

    elif command.startswith('sip:'):
        pass

    elif command == 'h':
        # hangup
        pass

    elif command == 'a':
        # answer
        pass
    elif command in ['200','480','486','487']:
        pass

    elif command == 'q':
        # shutdown
        pass

    else:
        print "Error: bad command: %s" % command








