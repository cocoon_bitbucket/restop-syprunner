
from yaml import load
from wbackend.model import Database,Model
from syprunner.models import SypPlatform as Platform
from syprunner.models import SypUser
#from syprunner.models import Syprunner_Agents

from pjsip_device.session import PjtermSession


import logging
log= logging.getLogger(__name__)


REDIS_HOST= "127.0.0.1"
REDIS_DB=0

PLATFORM_YAML= "./test_terminal_configurations.yml"


def setup():

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()



class MemberOptions(object):
    """

    """
    def __init__(self,option_string):
        """

        :param option_string: string eg "--username alice
        """
        self.options= []
        parts = option_string.split("--")
        for element in parts[1:]:
            self.options.append(element.strip())

    def check(self,option_value):
        """

        :param option_value: string
        :return:
        """
        if option_value in self.options:
            return True
        return False


class Options(object):
    """

    """
    def __init__(self,members):
        """

        :param members: list of strings : members  eg [alice,bob]
        """
        self.members={}
        ptf = Platform.get(Platform.name == 'default')
        options = ptf.compute_pjterm_options(members)
        for i,data in enumerate(options):
            self.members[members[i]]= MemberOptions(options[i])


    def check(self,member,option):
        """

        :param member: string eg alice,bob
        :param option: string eg username="alice"
        :return:
        """
        return self.members[member].check(option)




def test_compute_pjterm_options():
    """
        test with basic freeswitch register
        alice 1001
        bob 1002

    :return:
    """
    setup()

    members = ["alice"]
    options=Options(members)

    assert options.check( "alice",  u'id=sip:1001@192.168.1.51')


    return

def test_profile_fox():
    """
        registrar: 192.168.1.51
        "--ip-addr": "192.168.1.51"

        alice:
          username: "1001"

        bob:
         username: "1002"

    :return:
    """
    setup()

    members = ["alice","bob"]
    options=Options(members)

    # check alice
    assert options.check( "alice",  u'id=sip:1001@192.168.1.51')
    assert options.check("alice",   u'username=1001')
    assert options.check("alice",   u'registrar=sip:192.168.1.51')
    assert options.check("alice",   u'ip-addr=192.168.1.51')
    assert options.check("alice",   u'local-port=5061')

    # check bob
    assert options.check("bob", u'id=sip:1002@192.168.1.51')
    assert options.check("bob", u'username=1002')
    assert options.check("bob", u'registrar=sip:192.168.1.51')
    assert options.check("bob", u'ip-addr=192.168.1.51')
    assert options.check("bob", u'local-port=5062')

    return


def test_profile_ims():
    """
        proxy: "80.12.10.69:5060"
        domain: "btelu.test.fr"
        registrar: "sip:btelu.test.fr"

        U0001:
            username: "53EB73KLTRIJ"
            password: "9617BCB2B7921CC5"
            display: "LASTU0001 Firstu0001"
            id: "+33482259961"

        U0002:
            username: "C8063ALMLSUO"
            password: "273C0CB3F5748757"
            display: "LASTU0002 Firstu0002"
            id: "+33482259962"


    :return:
    """
    setup()

    members = ["U0001","U0002"]
    options=Options(members)

    # check U0001
    assert options.check( "U0001",  "id=sip:+33482259961@btelu.test.fr")
    assert options.check("U0001",u'username=53EB73KLTRIJ@btelu.test.fr')
    assert options.check("U0001",u'proxy=sip:80.12.10.69:5060')
    assert options.check("U0001", u'registrar=sip:btelu.test.fr')
    assert options.check("U0001", u'realm=btelu.test.fr')
    assert options.check("U0001", u'local-port=5061')

    # check U0002
    assert options.check("U0002", u'id=sip:+33482259962@btelu.test.fr')
    assert options.check("U0002", u'username=C8063ALMLSUO@btelu.test.fr')
    assert options.check("U0002", u'proxy=sip:80.12.10.69:5060')
    assert options.check("U0002", u'registrar=sip:btelu.test.fr')
    assert options.check("U0002", u'realm=btelu.test.fr')
    assert options.check("U0002", u'local-port=5062')
    return


def test_profile_b2gas():
    """
        registrar: 192.168.1.51
        "--ip-addr": "192.168.1.51"

        b2gas1:
          username: "1001"

        b2gas2:
         username: "1002"

    :return:
    """
    setup()

    members = ["b2gas1","b2gas2"]
    options=Options(members)

    # check b2gas1
    assert options.check( "b2gas1", u'id=sip:835076974@195.25.83.6')
    assert options.check("b2gas1",  u'username=0223066974')
    assert options.check("b2gas1",  u'registrar=sip:195.25.83.6')
    assert options.check("b2gas1",  u'local-port=5061')
    assert options.check("b2gas1", u'realm=ccmsipline')
    assert options.check("b2gas1", u'ip-addr=192.168.99.100')

    # check b2gas2
    assert options.check( "b2gas2", u'id=sip:835076909@195.25.83.6')
    assert options.check("b2gas2",  u'username=0223066909')
    assert options.check("b2gas2",  u'registrar=sip:195.25.83.6')
    assert options.check("b2gas2",  u'local-port=5062')
    assert options.check("b2gas1", u'realm=ccmsipline')
    assert options.check("b2gas1", u'ip-addr=192.168.99.100')




if __name__ == "__main__":


    logging.basicConfig(level=logging.DEBUG)

    test_compute_pjterm_options()
    test_profile_fox()

    test_profile_ims()

    test_profile_b2gas()

    print("Done.")