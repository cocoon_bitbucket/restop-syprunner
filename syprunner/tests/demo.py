# -*- coding: utf-8 -*-

__author__ = 'cocoon'

import time

import os
from restop_client import Pilot

"""

    end to end test via pilot ( robotframework client )

    needs 
    * a restop master ( tests.mastr.application )
    * a restop-syprunner hub ( syprunner.starter )
    * a cocoon/pjterm and redis at 192.168.99.100

"""




user= "Fanny"


master_url= "http://localhost:5000/restop/api/v1"
#master_url= "http://192.168.99.100:5000/restop/api/v1"

def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=master_url)


    return data


def test_samples_basic():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session("Fanny","Forest")

        time.sleep(3)

        # res = p.send("Fanny",cmd = "d")
        # res = p.expect("Fanny", pattern= "Dump complete")

        # res = p.send("Forest", cmd="d")
        # res = p.expect("Forest", pattern= "Dump complete")

        res = p.call( "Fanny", destination="sip:192.168.99.100:5062")
        res= p.watch_log("Forest",duration=5)

        res = p.send("Forest", cmd="d")
        res = p.expect("Forest", pattern= "Dump complete")

        r = p.wait_incoming_call("Forest", timeout=20)
        r = p.answer_call( "Forest", code=200, timeout=60)

        r = p.check_call("Fanny")

        print("wait 5s ...")
        time.sleep(5)




        # try:
        #     res= p.raise_error(user)
        # except:
        #     pass
        #
        # try:
        #     res= p.application_error(user)
        # except:
        #     pass
        #
        # try:
        #     res = p.not_implemented(user)
        # except:
        #     pass

        #res= p.dummy(user)

        # read alarm on samples
        time.sleep(5)


    except RuntimeError as e:
        print("interrupted: %s" % e.message)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return


def test_basic_freeswitch():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session("alice","bob")

        time.sleep(3)

        # res = p.send("Fanny",cmd = "d")
        # res = p.expect("Fanny", pattern= "Dump complete")

        # res = p.send("Forest", cmd="d")
        # res = p.expect("Forest", pattern= "Dump complete")

        #res = p.call( "alice", destination="sip:1002@192.168.99.100")
        res = p.call("alice", destination="sip:1002@192.168.1.51")
        #res= p.watch_log("alice",duration=20)

        # res = p.send("bob", cmd="d")
        # res = p.expect("bob", pattern= "Dump complete")

        r = p.wait_incoming_call("bob", timeout=20)
        #time.sleep(3)

        r = p.answer_call( "bob", code=200, timeout= 20)

        r = p.check_call("alice")

        print("wait 5s ...")
        time.sleep(5)

        # resp = alice.run("se")
        r = p.send_dtmf("alice", digits="1234")
        time.sleep(2)
        r = p.check_dtmf("bob", digits="1234")
        # r = bob.watch_log(duration=5)

        r = p.check_call_quality("alice", channel="TX")
        r = p.check_call_quality("bob", channel="RX")

        # r = alice.run("hangup")
        # r = bob.run("wait_hangup")
        time.sleep(2)

        #r = p.get_last_received_sip_request("bob", sip_method="BYE")


        r = p.hangup("alice")
        r = p.wait_hangup("bob")


        # read alarm on samples
        time.sleep(5)


    except RuntimeError as e:
        print("interrupted: %s" % e.message)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



def test_response_freeswitch():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session("bob")

        time.sleep(3)


        r = p.wait_incoming_call("bob", timeout=20)
        #time.sleep(3)

        r = p.answer_call( "bob", code=200, timeout= 20)

        res= p.watch_log("bob",duration=20)

        time.sleep(5)


    except RuntimeError as e:
        print("interrupted: %s" % e.message)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



def test_calling_freeswitch():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session("bob")

        time.sleep(3)

        res = p.call("bob", destination="sip:1001@192.168.1.26")

        #r = p.check_call("bob")

        res= p.watch_log("bob",duration=20)

        r= p.hangup("bob")

        time.sleep(5)


    except RuntimeError as e:
        print("interrupted: %s" % e.message)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return


def test_basic_b2gas():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session("b2gas1","b2gas2")

        time.sleep(3)

        # res = p.send("Fanny",cmd = "d")
        # res = p.expect("Fanny", pattern= "Dump complete")

        # res = p.send("Forest", cmd="d")
        # res = p.expect("Forest", pattern= "Dump complete")

        #res = p.call( "alice", destination="sip:1002@192.168.99.100")
        res = p.call("b2gas1", destination="sip:1002@192.168.99.100")
        #res= p.watch_log("alice",duration=20)

        # res = p.send("bob", cmd="d")
        # res = p.expect("bob", pattern= "Dump complete")

        r = p.wait_incoming_call("b2gas2", timeout=20)
        #time.sleep(3)

        r = p.answer_call( "b2gas2", code=200, timeout= 20)

        r = p.check_call("b2gas1")

        print("wait 5s ...")
        time.sleep(5)

        # resp = alice.run("se")
        r = p.send_dtmf("b2gas1", digits="1234")
        time.sleep(2)
        r = p.check_dtmf("b2gas2", digits="1234")
        # r = bob.watch_log(duration=5)

        r = p.check_call_quality("b2gas1", channel="TX")
        r = p.check_call_quality("b2gas2", channel="RX")

        # r = alice.run("hangup")
        # r = bob.run("wait_hangup")
        time.sleep(2)

        #r = p.get_last_received_sip_request("bob", sip_method="BYE")


        r = p.hangup("b2gas1")
        r = p.wait_hangup("b2gas2")


        # read alarm on samples
        time.sleep(5)


    except RuntimeError as e:
        print("interrupted: %s" % e.message)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return








if __name__=="__main__":

    #test_fetch_restop_interface()
    #test_samples_basic()
    test_basic_freeswitch()


    #test_response_freeswitch()
    #test_calling_freeswitch()

    #test_basic_b2gas()





    print("Done.")
