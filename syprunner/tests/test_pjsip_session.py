
from yaml import load

from wbackend.model import Database,Model
from syprunner.models import SypPlatform


from pjsip_device.proxy.session import PjsipSessionConfiguration
from pjsip_device.proxy.terminal import TerminalOptions

platform_file= './platform.yml'


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= SypPlatform.get(SypPlatform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        stream = open(platform_file,"rb")
        data = load(stream)
        p = SypPlatform.create(name='default', data=data)
        #p.setup()

    return db

def get_new_ptf():
    """

    :return:
    """
    db = get_new_db()

    # set up the default platform
    ptf = SypPlatform.get(SypPlatform.name == 'default')
    ptf.setup()
    return ptf




def test_terminal_options():
    """



    :return:
    """
    ptf = get_new_ptf()

    fanny= ptf.user("Fanny")

    options= fanny.data["options"]

    t= TerminalOptions()





    return


def test_sypConfiguration_model():
    """


    :return:
    """
    ptf= get_new_ptf()

    #users= ["Alice","U0001"]
    #users= ["Fanny"]

    users = ["Alice", "U0001","Fanny"]

    options= ptf.compute_pjterm_options(users)




    #fanny= ptf.user_profile("Fanny")
    #print(fanny)

    # alice= ptf.user_profile("Alice")
    # print(alice)
    #
    # alice_noSDA= ptf.user_profile("Alice_noSDA")
    # print(alice_noSDA)
    # u0001 = ptf.user_profile("U0001")
    # print(u0001)


    return



if __name__=="__main__":

    #test_terminal_options()
    test_sypConfiguration_model()


    print("Done.")
