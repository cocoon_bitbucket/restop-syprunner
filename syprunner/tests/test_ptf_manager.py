


# import os
# from copy import deepcopy
# from pjsip_device.proxy.phoneparser import FrenchPhoneNumber
#
# import json

# conditional import of yaml module
yaml_module= True
try:
    from yaml import load,dump
except ImportError:
    yaml_module= False

if yaml_module:
    try:
        from yaml import CLoader as Loader, CDumper as Dumper
    except ImportError:
        from yaml import Loader, Dumper

from syprunner.ptf_manager2 import SypConfiguration,TerminalOptions




if __name__ == "__main__":


    #fname= "../player/platform.json"
    #fname= '../server/bin/syprunner-samples/platform.json'
    fname_yaml= "./_platform.yml"
    platform_version= "demo_qualif"


    # def test_overview():
    #
    #     print("overview")
    #
    #     # # assume it is a filename , load it
    #     # with open( fname,"rb") as fh:
    #     #     platforms = json.loads(fh.read())
    #     #     conf = platforms[platform_version]
    #     #     ptf = SypPlatform(conf)
    #
    #
    #     cnf= SypConfiguration.from_file(fname)
    #     ptf= cnf.get_ptf(platform_version)
    #
    #
    #     alice= ptf.user('Alice')
    #     assert alice.agent_collection == 'syp_agents'
    #     #assert alice.to_user()== ""
    #
    #     mobby= ptf.user('Mobby')
    #     assert mobby.agent_collection == 'droyd_agents'
    #
    #
    #     return


    def test_yaml():
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)

        alice= ptf.user('Alice')
        print(alice.to_user())
        print(alice.sip_address_to_user(format='universal'))

        alfred= ptf.user('Alfred')
        print(alfred.to_user())
        print(alfred.sip_address_to_user(format='universal'))
        print(alfred.sip_address_to_user(format='national'))
        print(alfred.sip_address_to_user(format='ext'))
        print(alfred.sip_address_to_user(format='sid_ext'))
        print(alfred.sip_address_to_user(format='contact'))


        return

    def test_TerminalOptions():
        """


        :return:
        """
        o= TerminalOptions()

        o.set_device({
            "--no-tcp":"",
            "--auto-update-nat": 0,
            "--disable-stun": "",
            "--disable-via-rewrite":"",
            "--disable-rport":"",
            "--norefersub":"",
            "--reg-timeout": 300,
            "--add-codec": "PCMA",
            "--max-calls": 4,

        })
        o.set_platform({

            "--password": "nsnims2008",
            "--registrar": "sip:sip.imsnsn.fr",
            "--realm": "sip.imsnsn.fr",
            "--proxy":"sip:193.253.72.124:5060;lr",
            "--nameserver": "213.56.88.194",
        })

        o.set_account({
            "--username": "+33960813200@sip.imsnsn.fr",
            "--id":"sip:+33960813200@sip.imsnsn.fr",
            #"--password": "specific_password",
        })

        o.set_session({
            "--local-port": 6002,
            "--rtp-port": 20004,
            "--max-calls": "2"
        })

        o.set_with_audio()

        opts= o.gen()

        print(" ".join(opts))


        t2= o.clone()
        t2.set_account({
            "--username": "+33960813299@sip.imsnsn.fr",
            "--id":"sip:+33960813299@sip.imsnsn.fr",
            "--password": "specific_password",
        })

        t2.set_null_audio()

        print(" ".join(t2.gen()))

        print()

        print(" ".join(o.gen()))


        return


    def test_platform_interface():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)


        # create terminals ( with accounts , without session)
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'])
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'],(2,4,2))
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'],[2,4,2])

        #terminals= ptf.make_terminals(('Alice','Bob','Charlie'))


        # device configuration
        btelu_device= {
            '--no-tcp':'',
            '--auto-update-nat': 0,
            '--disable-via-rewrite':'',
            '--disable-rport':'',
            '--disable-stun':'',
            '--add-codec': 'PCMA',
        }

        users= ('Alice','Bob','Charlie')
        user_config= (
            {'--max-calls':2},
            {'--add-codec':'PCMU'},
            {},
        )


        terminals= []

        # create terminals with system,platform,device and account info
        for indice,allias in enumerate(users):

            # clone a basic terminal with system and platform info
            #terminal= ptf.get_terminal(allias,device=btelu_device, account= user_config[indice])
            terminal= ptf.get_terminal(allias, account= user_config[indice])
            terminals.append(terminal)

        # update all terminals with session info
        ptf.update_terminal_with_session(terminals,local_port=6000,rtp_port=20000)

        # compute terminal options
        terminal_configurations = [ " ".join(term.gen()) for term in terminals ]

        # display terminal option
        for t in terminal_configurations:
            print(t)


        return


    def test_DeviceSection():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)


        default= ptf.device_section.get_device('default')
        ims= ptf.device_section.get_device('ims')
        try:
            bad= ptf.device_section.get_device('bad')
        except KeyError:
            pass


    def test_codecs():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)

        allias= 'Alice'
        terminal= ptf.get_terminal(allias)

        terminal.set_session({'--add-codec': 'PCMA'})

        print(" ".join(terminal.gen()))


        print("add codecs")
        terminal.add_codec('PCMU')
        terminal.add_codec('speex')

        terminal.add_codec('other','another')

        terminal.clear_codec('another','dummy')

        print(" ".join(terminal.get_codecs()))


        print(" ".join(terminal.gen()))

        return


    # test begins here
    #test_overview()
    test_yaml()

    #test_TerminalOptions()
    #test_platform_interface()
    #test_DeviceSection()
    #test_codecs()



    print()