
import time
from yaml import load
from copy import deepcopy
import pprint
import requests

from wbackend.model import Database,Model
from restop_queues.client import QueueClient
from restop.application import ApplicationError


from restop_platform.dashboard import Dashboard
from syprunner.models import SypPlatform as Platform
from syprunner.models import Syprunner_Agents as AgentModel


from syprunner.syprunner_device.agent import Agent, AgentAdapter


from pjsip_device.proxy.connector import PjtermConnector
from pjsip_device.proxy.agent import PjsipAgent


import logging


platform_file= './platform.yml'
agent_name = 'Fanny'

hub_ip= '192.168.1.55'
hub_port= 8080



def get_new_db():
    """

    :return:
    """
    db = Database(host='redis', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/_platform.yml')
        #stream = file(platform_file)
        stream=open(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def get_model(agent_name='tv'):

    db= get_new_db()

    platform = Platform.get(Platform.name == 'default')
    agent_parameters= platform.data['devices'][agent_name]
    agent_profile= agent_parameters['profile']
    parameters= deepcopy(platform.data['profiles'][agent_profile])
    parameters.update(agent_parameters)

    #parameters['remote_url']= "192.168.1.20:8080"

    agent = AgentModel.create(name=agent_name, session=1, parameters= parameters)
    agent_id= agent.get_id()
    agent = AgentModel.load(agent_id)

    return agent



#
#
#

def test_basic():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """

    agent_model= get_model(agent_name)

    database= agent_model.database

    client= QueueClient(agent_model.get_hash_id(),redis_db=database)


    agent = Agent(agent_model)

    # simulate a start adapter
    #r= agent.start()
    pjterm_connector = PjtermConnector("syprunner-fake",log=agent.log, command_line="./pjterm-fake.py" )
    pjterm_agent= PjsipAgent("userA","Fanny",adapter=pjterm_connector)
    # start pjterm binary
    pjterm_connector.open()



    #adapter = AgentAdapter(agent_model, run_mode='passive')
    adapter = AgentAdapter(agent_model, run_mode='threaded')
    adapter.connector= pjterm_agent

    # start adapter
    adapter.start()


    #r= agent._send("ping")

    #r= agent.send("echo" , text='hello there' )

    r = agent.send("expect", pattern='POSIX')

    #adapter.run_once()

    #r= adapter.model.readline(channel_name="stdin")

    #pjterm_agent.close()
    pjterm_connector.close()
    r= agent.stop()


    return



if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)

    test_basic()



    print("Done.")
