from yaml import load

from wbackend.model import Database,Model
#from restop_platform.models import Platform
from syprunner.models import Syprunner_Agents as AgentModel
from syprunner.models import SypPlatform, SypUser
from syprunner.resources import Resource_syprunner_agents as AgentResource


platform_file= './platform.yml'

def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= SypPlatform.get(SypPlatform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/_platform.yml')
        stream = open(platform_file,"rb")
        data = load(stream)
        p = SypPlatform.create(name='default', data=data)
        #p.setup()

    return db

def test_model():
    """

    :return:
    """
    db= get_new_db()

    sample= AgentModel.create(name='sample_1',session=1)

    return

def test_apidoc():
    """


    :return:
    """
    autodoc = AgentResource._autodoc()
    return


def test_sypplatform_model():
    """


    :return:
    """
    db = get_new_db()

    ptf= SypPlatform.get(SypPlatform.name == 'default')
    ptf.setup()

    fanny= ptf.user_profile("Fanny")
    #fanny = ptf.user_profile("Fanny")
    print(fanny)

    alice= ptf.user_profile("Alice")
    print(alice)

    alice_noSDA= ptf.user_profile("Alice_noSDA")
    print(alice_noSDA)
    u0001 = ptf.user_profile("U0001")
    print(u0001)



    return


def test_sypuser_model():
    """


    :return:
    """
    db = get_new_db()

    ptf= SypPlatform.get(SypPlatform.name == 'default')
    ptf.setup()

    fanny= SypUser.get_or_create()

    fanny= ptf.user_profile("Fanny")


def test_feature_access_code():
    """


    :return:
    """
    db = get_new_db()

    ptf= SypPlatform.get(SypPlatform.name == 'default')
    ptf.setup()



    code= ptf.fac("+CFA")
    assert code == "*2011"

    return



def compute_sip_address(user):
    """

    :param user:
    :return:
    """
    print("user: %s" % user.name)
    formats= SypUser.number_formats
    for format in formats:
        try:
            d = user.sip_address_to_user(format=format)
        except Exception as error:
            print("format: %s ==> FAILED: %s" % (format,str(error)))
            continue
        print("format: %s ==> %s " % (format, d))




def test_resolve_address():
    """

    :return:
    """
    db = get_new_db()

    ptf = SypPlatform.get(SypPlatform.name == 'default')
    ptf.setup()

    u1= ptf.user('U0001')
    compute_sip_address(u1)

    u = ptf.user('Alice')
    compute_sip_address(u)

    u = ptf.user('alice')
    compute_sip_address(u)

    u = ptf.user('Fanny')
    compute_sip_address(u)



    r= ptf.resolve_address(user="alice", fac=None, format="ext", destination=None, to_sip=True)
    assert r== u'sip:1001@192.168.1.51'

    r = ptf.resolve_address(user=None, fac="+CFA", format=None, destination=None, to_sip=True)
    assert r == u'*2011'


    r = ptf.resolve_address(user="Alice", fac="+CFA", format="ext", destination=None, to_sip=True)
    assert r == u'*20111101'

    r = ptf.resolve_address(user=None, fac=None, format=None, destination="oms_service", to_sip=True)
    assert r == u'sip:+33146511102@192.168.1.50;play=http_audio.example.net_song.wav'

    # print(u1.to_user('international'))
    # print(u1.to_user('national'))
    #
    # d= u1.sip_address_to_user(format="international")
    # print d




    #a1= ptf.resolve_address(user="U0001", fac=None, format="international", destination=None, to_sip=True)


    return




if __name__=="__main__":

    test_sypplatform_model()
    test_apidoc()
    test_model()

    test_feature_access_code()

    test_resolve_address()

    print("Done.")