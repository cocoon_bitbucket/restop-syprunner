
import time
from yaml import load
from wbackend.model import Database,Model
from syprunner.models import SypPlatform as Platform
from syprunner.models import SypUser
from syprunner.controllers import SessionController,TerminalController
#from syprunner.models import Syprunner_Agents

from pjsip_device.session import PjtermSession


import logging
log = logging.getLogger(__name__)

"""
    tests with models (platform.yml) and cocoon/pjterm manager
    but without http api 



"""




#REDIS_HOST = "127.0.0.1"
REDIS_HOST = "192.168.99.100"
#REDIS_HOST = "192.168.1.26"
REDIS_DB = 2
PLATFORM_YAML= "./platform.yml"


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    # try:
    #     p= SypPlatform.get(SypPlatform.name=='default')
    # except Exception as e:
    #     # does not exists create it
    #     db.flushdb()
    #     #stream = file('../../tests/_platform.yml')
    #     stream = open(platform_file,"rb")
    #     data = load(stream)
    #     p = SypPlatform.create(name='default', data=data)
    #     #p.setup()

    return db


def test_main():

    """

    :return:
    """

    # get redis db
    #db =get_new_db()

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    # start pjterm Listener
    log.info('start pjterm Listener')
    PjtermSession.create_session_listener(cnx=db)


    try:
        print(".")

        #
        # open a session with fanny and forest
        #
        # POST /restop/api/v1/sessions  ==>
        members= ["Fanny", "Forest"]

        ptf = Platform.get(Platform.name == 'default')
        # ptf.setup()
        options = ptf.compute_pjterm_options(members)
        for index, alias in enumerate(members):
            #configuration[alias]["command_line"] = options[index]
            user = ptf.user(alias)
            user.data["command_line"]= options[index]
            user.save()
            # user.delete()

        # start a new pjterm session
        s = PjtermSession(cnx=ptf.database)
        s.open_session(members=" ".join(members))

        #
        # start pjterm for fanny
        #
        user= "Fanny"
        # retrieve agent data
        #agent = self.model.load(item)
        #agent= Syprunner_Agents.load(user)

        agent= SypUser.get(SypUser.name==user)
        try:
            command_line = agent.data['command_line']
        except KeyError as e:
            raise RuntimeError("no command line for syp agent %s" % user)

        session = PjtermSession( db)
        result = session.start_terminal( user, options=command_line)
        #self.started = result
        #time.sleep(0.1)

        # get logs
        terminal = session.get_terminal(user)
        logs = terminal.adapter.read_log(0)
        logs.append("**** agent started: %s ***" % user)
        agent.log_offset = terminal.adapter.log_len()
        agent.save()
        response = dict(result=200, logs=logs, message=True)


        #
        # start pjterm for forest
        #
        user = "Forest"
        # retrieve agent data
        # agent = self.model.load(item)
        # agent= Syprunner_Agents.load(user)

        agent = SypUser.get(SypUser.name == user)
        try:
            command_line = agent.data['command_line']
        except KeyError as e:
            raise RuntimeError("no command line for syp agent %s" % user)

        session = PjtermSession(db)
        result = session.start_terminal(user, options=command_line)
        # self.started = result
        # time.sleep(0.1)

        # get logs
        terminal = session.get_terminal(user)
        logs = terminal.adapter.read_log(0)
        logs.append("**** agent started: %s ***" % user)
        agent.log_offset = terminal.adapter.log_len()
        agent.save()
        response = dict(result=200, logs=logs, message=True)



        #
        #  fanny call forest
        #
        user= "Fanny"
        destination = "sip:forest@192.168.99.100:5062"
        agent = SypUser.get(SypUser.name == user)

        s = PjtermSession(db)
        term = s.get_terminal(user)
        term.call(destination=destination)

        #
        #  forest wait fo call
        #
        user = "Forest"
        agent = SypUser.get(SypUser.name == user)

        s = PjtermSession(db)
        term = s.get_terminal(user)
        term.wait_incoming_call(timeout=5)

        #
        #  forest answer_call
        #
        user = "Forest"
        agent = SypUser.get(SypUser.name == user)

        s = PjtermSession(db)
        term = s.get_terminal(user)
        term.answer_call()

        #
        #  fanny check call confirmed
        #
        user = "Fanny"
        agent = SypUser.get(SypUser.name == user)

        s = PjtermSession(db)
        term = s.get_terminal(user)
        term.check_call(state="CONFIRMED")


        #
        # call established
        #
        log.info("call established")
        time.sleep(5)



    except Exception as e:
        print(e)

    finally:

        # close session
        # close session
        s = PjtermSession(db)
        s.close_session()


        # shutdown pjterm Listener
        log.info('kill pjterm Listener')
        s = PjtermSession.shutdown_session_listener(db)


def test_controllers():
    """


    :return:
    """

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    # start pjterm Listener
    log.info('start pjterm Listener')
    PjtermSession.create_session_listener(cnx=db)


    try:
        print(".")

        #
        # open a session with fanny and forest
        #
        # POST /restop/api/v1/sessions  ==>
        members= ["Fanny", "Forest"]

        sc = SessionController()
        options = sc.open(members)

        #
        # start pjterm for fanny
        #
        user= "Fanny"
        sc = SessionController()

        r1 = sc.start_terminal(user)

        #
        # start pjterm for forest
        #
        user = "Forest"
        sc = SessionController()
        r2 = sc.start_terminal(user)

        #
        # start pjterm for bad
        #
        # user = "bad"
        # sc = SessionController()
        # r2 = sc.start_terminal(user)



        #
        #  fanny call forest
        #
        user = "Fanny"
        sc = SessionController()
        t = TerminalController(sc,user)
        response= t.run("call",destination="sip:forest@192.168.99.100:5062")

        #
        #  forest wait fo call
        #

        user = "Forest"
        sc = SessionController()
        t= sc.terminal_controller(user)
        response = t.run("wait_incoming_call",timeout=5)

        #
        #  forest answer_call
        #
        t = SessionController().terminal_controller("Forest")
        response= t.run("answer_call")

        #
        #  fanny check call confirmed
        #
        t = SessionController().terminal_controller("Fanny")
        response = t.run("check_call",state="CONFIRMED")


        #
        # call established
        #
        log.info("call established")
        time.sleep(5)



    except Exception as e:
        print(e)

    finally:

        # close session
        # close session
        s = PjtermSession(db)
        s.close_session()


        # shutdown pjterm Listener
        log.info('kill pjterm Listener')
        s = PjtermSession.shutdown_session_listener(db)


def test_demo_controllers():
    """
        test with basic freeswitch register
        alice 1001
        bob 1002

    :return:
    """

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    # start pjterm Listener
    log.info('start pjterm Listener')
    PjtermSession.create_session_listener(cnx=db)


    try:
        print(".")

        #
        # open a session with fanny and forest
        #
        # POST /restop/api/v1/sessions  ==>
        members= ["alice", "bob"]

        sc = SessionController()
        options = sc.open(members)

        #
        # start pjterm for fanny
        #
        user= "alice"
        sc = SessionController()
        r1 = sc.start_terminal(user)

        #
        # start pjterm for forest
        #
        user = "bob"
        sc = SessionController()
        r2 = sc.start_terminal(user)

        #
        # start pjterm for bad
        #
        # user = "bad"
        # sc = SessionController()
        # r2 = sc.start_terminal(user)



        #
        #  fanny call forest
        #
        user = "alice"
        sc = SessionController()
        t = TerminalController(sc,user)


        #response= t.run("call",destination="sip:1002@192.168.99.100")
        #response = t.run("call", destination="sip:1002@192.168.1.51")

        response= t.run("call_user",userX="bob",format="ext")


        #
        #  forest wait fo call
        #

        user = "bob"
        sc = SessionController()
        t= sc.terminal_controller(user)
        response = t.run("wait_incoming_call",timeout=20)

        #
        #  forest answer_call
        #
        t = SessionController().terminal_controller("bob")
        response= t.run("answer_call")

        #
        #  fanny check call confirmed
        #
        t = SessionController().terminal_controller("alice")
        response = t.run("check_call",state="CONFIRMED")


        #
        # call established
        #
        log.info("call established")
        time.sleep(2)



        alice = SessionController().terminal_controller("alice")
        bob= SessionController().terminal_controller("bob")


        #resp = alice.run("se")
        r = alice.run("send_dtmf" ,digits="1234")
        time.sleep(2)
        r = bob.run("check_dtmf", digits="1234")
        #r = bob.watch_log(duration=5)

        r = alice.run("check_call_quality",channel="TX")
        r = bob.run("check_call_quality",channel="RX")


        # r = alice.run("hangup")
        # r = bob.run("wait_hangup")
        # time.sleep(2)

        r = bob.run("get_last_received_sip_request",sip_method="BYE")





    except Exception as e:
        print(e)

    finally:

        # close session
        # close session
        s = PjtermSession(db)
        s.close_session()


        # shutdown pjterm Listener
        log.info('kill pjterm Listener')
        s = PjtermSession.shutdown_session_listener(db)


def test_calling_freeswitch():
    """
        test with basic freeswitch register
        alice 1001
        bob 1002

    :return:
    """

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    # start pjterm Listener
    log.info('start pjterm Listener')
    PjtermSession.create_session_listener(cnx=db)


    try:
        print(".")

        #
        # open a session with fanny and forest
        #
        # POST /restop/api/v1/sessions  ==>
        members= ["bob"]

        sc = SessionController()
        options = sc.open(members)
        #
        # start pjterm for forest
        #
        user = "bob"
        sc = SessionController()
        r2 = sc.start_terminal(user)

        #
        # start pjterm for bad
        #
        # user = "bad"
        # sc = SessionController()
        # r2 = sc.start_terminal(user)



        #
        #  fanny call forest
        #
        # user = "bob"
        # sc = SessionController()
        # t = TerminalController(sc,user)

        t= SessionController().terminal_controller("bob")
        response= t.run("call",destination="sip:1001@192.168.1.26")

        #response= t.run("call_user",userX="bob",format="ext")


        #
        #  fanny check call confirmed
        #
        t = SessionController().terminal_controller("bob")
        response = t.run("check_call",state="CONFIRMED")

        #
        # call established
        #
        log.info("call established")
        time.sleep(10)


    except Exception as e:
        print(e)

    finally:

        # close session
        # close session
        s = PjtermSession(db)
        s.close_session()


        # shutdown pjterm Listener
        log.info('kill pjterm Listener')
        s = PjtermSession.shutdown_session_listener(db)



def test_pjtermlevelDb():
    """
        test with basic freeswitch register
        alice 1001
        bob 1002

    :return:
    """

    log.debug('set redis database and bind model')
    db = Database(host=REDIS_HOST, port=6379, db= REDIS_DB)
    Model.bind(database=db, namespace=None)


    db_create = True
    if db_create:
        log.info("create platform from yaml")
        db.flushdb()
        stream = open( PLATFORM_YAML)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    # start pjterm Listener
    log.info('start pjterm Listener')
    PjtermSession.create_session_listener(cnx=db)


    try:
        print(".")

        #
        # open a session with fanny and forest
        #
        # POST /restop/api/v1/sessions  ==>
        members= ["alice", "bob"]

        sc = SessionController()
        options = sc.open(members)

        #
        # start pjterm for fanny
        #
        sc = SessionController()
        r1 = sc.start_terminal("alice")

        #
        # start pjterm for forest
        #
        sc = SessionController()
        r2 = sc.start_terminal("bob")


        # build terminal controllers
        alice = SessionController().terminal_controller("alice")
        bob = SessionController().terminal_controller("bob")

        #r= alice.run("call_feature_access_code",fac= "+CFA", userX="bob", format="ext")
        #r=alice.run("call_destination",destination= "wrong_Sid_E1S2")

        #r= alice.run("call_sd",sd=1)

        #r=alice.run("transfer_to_user", userX="bob" ,format="ext")

        #r=alice.run("transfer_to_destination",destination="wrong_PrivateNumber?")

        #r=alice.run("activate_redirection_to_user",userB="bob",redirect_kind="CFA",call_format="ext")


        #r= alice.run("cancel_redirection",redirect_kind="CFA")





        #  alice call bob
        #response = alice.run("call", destination="sip:1002@192.168.1.51")
        response= alice.run("call_user",userX="bob",format="ext")


        #  bob wait for call
        response = bob.run("wait_incoming_call",timeout=20)

        r= bob.run("check_incoming_call",display_name="Extension 1001")

        #
        #  forest answer_call
        #
        t = SessionController().terminal_controller("bob")
        response= t.run("answer_call")

        #
        #  alice check call confirmed
        #
        response = alice.run("check_call",state="CONFIRMED")


        #
        # call established
        #
        log.info("call established")
        time.sleep(2)



    except Exception as e:
        print(e)

    finally:

        # close session
        # close session
        s = PjtermSession(db)
        s.close_session()


        # shutdown pjterm Listener
        log.info('kill pjterm Listener')
        s = PjtermSession.shutdown_session_listener(db)






if __name__ == "__main__":


    logging.basicConfig(level=logging.DEBUG)

    #test_main()

    #test_controllers()


    #test_demo_controllers()


    #test_calling_freeswitch()


    test_pjtermlevelDb()



    print("Done.")