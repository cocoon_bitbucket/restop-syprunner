__author__ = 'cocoon'


import time

# restop imports

#from pjsip_device.server import get_pjterm_path
#
#from syprunner_device.restop_agents import Resource_pjterm_agents
from restop.application import ApplicationError
from restop_platform.resources import AgentResource

from agent import NewAgent as Agent
from syprunner.models import DEVICE_NAME
from pjsip_device.session import PjtermSession
# devices import
from syprunner.models import SypPlatform
from syprunner.controllers import SessionController

# from pjsip_device.session_server.ptf_manager2 import SypConfiguration
# from pjsip_device.proxy.agent import PjsipAgent,TestError,TimeoutError
# from pjsip_device.proxy.syprfc.sip_parser import Message
# from agent import Agent


syprunner_configuration= './platform.json'
platform_version= 'demo_qualif'



class Resource_syprunner_agents(AgentResource):
    """

        a full syprunner agent

        add the platform resolution to simple call functions


        def call_feature_access_code(self,user,call_feature_access_code,userX=None,format=None):
        def call_destination(self,user,destination):
        def call_user(self,userA,userX,format="universal"):
        def call_sd(self,user,sd):
        def transfer_to_user(self,userA,userX,format="universal"):
        def transfer_to_destination(self,user,destination):
        def activate_redirection_to_user(self,userA,userB,redirect_kind,call_format):
        def cancel_redirection(self,user,redirect_kind):

        def check_incoming_call(self,user,display_name):



    """
    collection = DEVICE_NAME
    _AgentClass = Agent
    #protected = ''


    #collection= 'syprunner_agents'

    #SessionClass= Resource_phone_sessions

    syprunner_configuration= syprunner_configuration
    platform_version= platform_version


    def configure_session(self,session_id,members,configuration):
        """

                called to configure session ( url syprunner_agents/-/open

        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        ptf = SypPlatform.get(SypPlatform.name == 'default')
        #ptf.setup()
        options = ptf.compute_pjterm_options(members)
        for index,alias in enumerate(members):
            configuration[alias]["command_line"] = options[index]
            # user= ptf.user(alias)
            # #user.data["command_line"]= options[index]
            # user.save()
            # #user.delete()

        # # start a new pjterm session
        # s= PjtermSession(cnx=ptf.database)
        # s.open_session(members=" ".join(members))

        return configuration


    def op_start(self, item, **kwarg):
        """
            triggererd on  syprunner_agent/<alias>/start

        :param item:
        :param kwarg:
        :return:
        """
        # select syprunner
        data = self.request.json
        data = data or {}

        self.started = False

        sc = SessionController()
        response = sc.start_terminal(item)

        # raise appincation error if result is not 200
        if response.get("result",200) != 200:
            raise ApplicationError(
                response.get("message","no message"),
                logs= response.get("logs",[]),
                status_code = response.get("status",500)
            )


        return response

    def op_stop(self, item, **kwarg):
        """
            triggererd on  syprunner_agent/<alias>/stop

        :param item:
        :param kwarg:
        :return:
        """
        # select syprunner
        data = self.request.json
        data = data or {}

        self.started = False


        # input_data = self.request.json
        # if input_data is None:
        #     input_data = {}

        t = SessionController().terminal_controller(item)
        response = t.run("_stop")



        # # retrieve agent data
        # agent = self.model.load(item)
        # # # get logs
        # # terminal = session.get_terminal(item)
        # # logs = terminal.adapter.read_log(0)
        # # logs.append("**** agent started: %s ***" % item)
        # # agent.log_offset = terminal.adapter.log_len()
        # # agent.save()
        logs=["close terminal %s" % item]
        response = dict(result=200, logs=logs, message=True)

        return response


    def op_col_open_session(self, item, **kwargs):
        """
            open a local session

            POST /hub/samples/-/open_session

        :return:
        """
        #assert item == '-'
        agents= []
        data= self.request.json
        session_id= data['session_id']
        members= data['members']
        configuration=data['configuration']


        session_configuration= self.configure_session(session_id,members,configuration)
        for member_alias in members:
            agents.append(member_alias)

        sc = SessionController()
        options = sc.open(members)


        # # create agents
        # for member_alias in members:
        #     agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
        #     agents.append(agent.get_id())
        # prepare  response


        data= { "agents": agents, "session_configuration":session_configuration}

        return { 'result':200, 'message':data,'logs':[]}



    def op_col_close_session(self, item, **kwargs):
        """
            close a local session

            POST /hub/samples/-/close_session

                parameters:  members

        :return:
        """
        #assert item == '-'
        data= self.request.json

        session_id= data['session_id']
        members= data['members']

        sc = SessionController()
        r = sc.close()

        # # close session
        # session= PjtermSession(cnx=self.model.database)
        # r= session.close_session()

        response= dict(result=200, message=True, logs=["syprunner session closed"])


        return response



    def _operation(self,collection,item,operation,**kwargs):
        """
            execute an operation on an item

            override standard dispatcher: search for a op_$operation method

        """
        # check if operation is a direct implementation = op_start , op_stop ...
        op= None
        if item == '-':
            # collection operation
            op_name = "op_col_" + operation
        else:
            # item operation
            op_name = "op_" + operation

        try:
            op= getattr(self,op_name)
        except AttributeError:
            pass
        if op :
            # call local operation op_*
            return op(item,**kwargs)



        input_data = self.request.json
        if input_data is None:
            input_data= {}

        t = SessionController().terminal_controller(item)
        response = t.run( operation , **input_data)

        # # operation exists
        # model = self.model.load(item)
        # model.logs.append("agent [%s/%s] received [%s] command" % (self.model.collection,item,operation))
        # agent = self._AgentClass(model)
        #
        # try:
        #     func= getattr(agent,operation)
        # except AttributeError:
        #     # operation does not exists on interface
        #     raise ApplicationError("invalid operation [%s] on item [%s]" % (operation,item) , 404 )
        #
        # try:
        #     input_data= self.request.json
        #     if input_data:
        #         rc= func(**input_data)
        #     else:
        #         rc= func()
        #     #return rc
        #     response= agent._make_response(result=200,message=rc)
        #
        # except Exception as error:
        #     # error executing method
        #     response= agent._make_response(result=500,message=None)
        #     response["logs"].append("error: %s" % error)

        return response



    #
    #  class methods
    #

    @classmethod
    def check_caller(cls,user, display=None , number=None):
        """
            check call is emitted by the given user

            @user: str, the agent: Alice, Bob ,
            @display: str the display name of the caller (eg "Alice Alice")
            @number: str : the number of the caller

        """
        def validate(call):
            """

                @call: a sip INVITE object (a syprfc.Message instance)

                getting the From:Header
                    from_header= call['from']

                available attributes:
                    from_header.tag
                    from_header.value.uri.user
                    from_header.value.uri.host
                    from_header.value.displayable  # warning length limited to a 25 eg : 'Q11A-EasyValid Q11A-Ea...'

                    from_header.value.displayName


            """
            from_header= call['from']
            if display:
                # check display name
                received = from_header.value.displayName
                if not received == display:
                    # display name check has failed
                    raise ValueError('bad display name, expected: "%s" , received: "%s"' %(display,received))
            if number:
                raise NotImplementedError('check number not implemented yet')
            return True

        return validate


    #
    # local methods
    #


    #
    # def check_incoming_call(self,role,validation,**kwargs):
    #     """
    #
    #
    #     """
    #
    #     # get the last INVITE received
    #     #last_invite_request = self.execute(role,'get_last_received_request',operation='INVITE')
    #     last_invite_request = self.get_last_received_sip_request(role,sip_method="INVITE")
    #     # convert lines to a sip Message object
    #     message = Message(last_invite_request)
    #     # perform validation
    #     r = validation(message)
    #     return r
    #
    #
    # #
    # # published operations
    # #
    #
    #
    # def op_call_user(self,item,**kwargs):
    #     """
    #
    #     /$agent_url/?/call_user  userX , format='universal
    #
    #     :param userA:
    #     :param userX:
    #     :param format:
    #     :return:
    #     """
    #     input= self.request.json
    #
    #     userX= input['userX']
    #     format= input.get('format','universal')
    #
    #
    #     sip_address= self.SessionClass.get_sip_address(user=userX,format=format)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #
    #
    # def op_call_feature_access_code(self,item,**kwargs):
    #     """
    #
    #        call Application server with a feature access code
    #
    #         eg  -CFA , -CFU
    #
    #     """
    #     input= self.request.json
    #
    #     call_feature_access_code= input['call_feature_access_code']
    #     userX= input.get(input['userX'],None)
    #     format= input.get('format',None)
    #
    #     sip_address= self.SessionClass.get_sip_address(user=userX,format=format,fac=call_feature_access_code)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #
    #
    #
    # def op_call_destination(self,item,**kwargs):
    #     """
    #
    #     :param user:
    #     :param destination:
    #     :return:
    #     """
    #     input= self.request.json
    #     destination= input['destination']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #
    #     resolved_destination= ptf.destination(destination)
    #     sip_address= resolved_destination
    #     #sip_address= self.SessionClass.get_sip_address(resolved_destination)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #sip_address= self.ptf.sip_address_for(resolved_destination)
    #     #print "pilot: user %s call destination %s  (%s)" % (user,destination,sip_address)
    #     #return self._call(user,sip_address=sip_address)
    #
    #
    # def op_call_sd(self,item,**kwargs):
    #     """
    #          user call with direct sd8 key sd ( 1..8 )
    #
    #          @user: eg Alice Bob
    #          @sd: the sd8 direct key on the terminal ( eg 1 , 2  ... 8)
    #
    #     :param user:
    #     :param sd:
    #     :return:
    #     """
    #     input= self.request.json
    #     sd= input['sd']
    #
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     sip_address= ptf.sip_address_for(sd)
    #
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #     #print "pilot: user %s call direct sd8 key %s  (%s)" % (user,sd,sip_address)
    #     #
    #     #return self._call(user,sip_address=sip_address)
    #
    #
    # def op_transfer_to_user(self,item,**kwargs):
    #     """
    #
    #        Unatented transfer call
    #
    #         *userA* transfer call to *userX* with *format* (universal,national ...)
    #
    #     :param userA:
    #     :param userX:
    #     :param format:
    #     :return:
    #     """
    #     # compute sip address for userX with format
    #     input= self.request.json
    #     userX= input['userX']
    #     format= input.get('format','universal')
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     sip_address = ptf.user(userX).sip_address_to_user(format)
    #
    #     #print  "pilot: user %s transfer call to user %s with format %s  (%s)" % ( userA, userX, format ,sip_address)
    #     #return self._transfer(userA,sip_address=sip_address)
    #
    #     return self._proxy_operation(item,'transfer',destination= sip_address)
    #
    # def op_transfer_to_destination(self,item,**kwargs):
    #     """
    #        transfer call (unatended) to a predefined destination (defined in platform.json)
    #
    #         *@user*: eg Alice , Bob
    #
    #         *@destination*:  the literal name of a destination defined in platform conf
    #
    #     :param user:
    #     :param destination:
    #     :return:
    #     """
    #     input= self.request.json
    #     destination= input['destination']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     # retrieve destination from configuration
    #     resolved_destination= ptf.destination(destination)
    #     sip_address= ptf.sip_address_for(resolved_destination)
    #     return self._proxy_operation(item,'transfer',destination= sip_address)
    #
    #
    #     #print "pilot: user %s transfer call to destination %s  (%s)" % (user,destination,sip_address)
    #     #return self._transfer(user,sip_address=sip_address)
    #
    # def op_activate_redirection_to_user(self,item,**kwargs):
    #     """
    #        call Application Server using a Feature Access Code to redirect userA to userB
    #
    #         *@userA*: the user to be redirected
    #
    #         *@userB*: target of the redirection
    #
    #         *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)
    #
    #         *@call_format*: site,national,international,universal
    #
    #         eg: activate redirection to user  Alice  Bob  CFA  national
    #
    #     :param userA:
    #     :param userB:
    #     :param redirect_kind:
    #     :param call_format:
    #     :return:
    #     """
    #     input= self.request.json
    #     userB= input['userB']
    #     redirect_kind= input['redirect_kind']
    #     call_format= input['call_format']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #
    #     fac_name = "+" + redirect_kind
    #     fac_code = ptf.fac(fac_name)
    #
    #     sip_address = ptf.sip_address_for( fac_code + ptf.user(userB).to_user(format_=call_format))
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #print "pilot: user %s ask a call forward via feature access code %s to user %s with format %s (%s) " % (
    #     #    userA,redirect_kind,userB,call_format,destination)
    #     # place the call
    #     #return self._call(userA,sip_address=destination)
    #
    # def op_cancel_redirection(self,item,**kwargs):
    #     """
    #        Convenience function to send a FAC sequence to AS to cancel a redirection
    #
    #         *@userA*: the user to be redirected
    #
    #         *@redirect_kind*: CFA,CFB,CFNA,CFNR  (Always,Busy,NotAnswered,NotReachable)
    #
    #     :param user:
    #     :param redirect_kind:
    #     :return:
    #     """
    #     input= self.request.json
    #     redirect_kind= input['redirect_kind']
    #
    #     conf = SypConfiguration.from_file(self.syprunner_configuration)
    #     ptf= conf.get_ptf(self.platform_version)
    #
    #     fac_name = "-" + redirect_kind
    #     fac_code = ptf.fac(fac_name)
    #     sip_address = ptf.sip_address_for(fac_code)
    #     return self._proxy_operation(item,'call',destination= sip_address)
    #
    #     #print "pilot: call AS to cancel a redirection of type %s for user %s (%s)" %(redirect_kind,user,destination)
    #     # place the call
    #     #return self._call(user,sip_address=destination)
    #
    # def op_check_incoming_call(self,item,**kwargs):
    #     """
    #         checks display name on FROM header of the incoming INVITE message
    #
    #         *@user*: str , the user receiving the call (eg Bob)
    #
    #         *@display_name*: the display name to compare with ( or unkwown )
    #
    #     :param user:
    #     :param display_name:
    #     :return:
    #     """
    #     input= self.request.json
    #     display_name= input['display_name']
    #
    #     user_data= self.backend.item_get(self.collection,item)
    #     user= user_data['alias']
    #
    #
    #     validation = self.check_caller(user,display_name)
    #
    #     r= self.check_incoming_call(item,validation)
    #     rc= { 'code':200, 'message': 'check OK' , 'logs':['Check_incoming call OK']}
    #     return rc
    #
    #
    #


