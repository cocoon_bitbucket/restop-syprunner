

DEVICE_NAME= "syprunner_agents"

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

from restop_platform.models import *
from pjsip_device.proxy.phoneparser import FrenchPhoneNumber
from pjsip_device.proxy.terminal import TerminalOptions,get_env_defaults
from pjsip_device.proxy.terminal import SYPRUNNER_MEDIA_DIR



SYPRUNNER_BLUEBOX_DOMAIN ='bluebox'



class SypUser(Model):
    """
        a temporary sypuser

    """
    collection= "sypuser"

    escape = "0"
    number_formats = ['international', 'national', 'escape_international', 'escape_national', 'sid_ext', 'ext',
                      'contact', 'universal']

    name = TextField(index=True)
    log_offset = IntegerField(default=1)
    data= JSONField()


    @classmethod
    def get_or_create(cls,alias,context=None):
        """

        :return:
        """
        context= context or {}
        try:
            user= cls.get(cls.name == alias)
        except ValueError:
            # no user create it
            device  = Device.get(Device.name == alias)
            profile_id = device.profile
            profile= Profile.get(Profile.name == profile_id)

            # take base from context
            data = context.copy()

            # update with profile info
            data.update(profile.data)
            # update with
            data["alias"]= device.name
            data.update(device.data)
            #data['collection']= profile.collection

            user= cls(name=alias,data=data)
            user.save()

        return user

    @classmethod
    def new(cls, alias, context=None):
        """
            create a new syp user ( delete it before if it exists )
        :return:
        """
        context = context or {}
        try:
            user = cls.get(cls.name == alias)
            user.delete()
        except ValueError:
            pass

        # create it
        device = Device.get(Device.name == alias)
        profile_id = device.profile
        profile = Profile.get(Profile.name == profile_id)

        # take base from context
        data = context.copy()

        # update with profile info
        data.update(profile.data)
        # update with
        data["alias"] = device.name
        data.update(device.data)
        # data['collection']= profile.collection

        user = cls(name=alias, data=data)
        user.save()

        return user


    @property
    def username(self):
        """
            return the private username ( the one password refers to)

        :return:
        """
        return self.data["username"]

    @property
    def id(self):
        """

        :return:    the public id
        """
        try:
            id= self.data['id']
            assert id
        except (KeyError,AssertionError) :
            # public id is not explicit , so take username as the public id
            id= self.username
        return id


    def site(self):
        """


        :return: return the Site object of the user or None
        """
        site = None
        site_id = self.data.get("site",None)
        if site_id:
            try:
                site = Site.get( Site.name == site_id)
            except ValueError:
                pass
        return site


    def registrar(self):
        """
        """
        registrar= self.data.get("registrar",None)
        if not registrar:
            registrar= self.data.get("domain", None)
        if registrar:
            if not registrar.startswith("sip:"):
                registrar= "sip:" + registrar
        # if self.data.has_key('registrar'):
        #     registrar = self.content['main']['registrar']
        # else:
        #     registrar = self.content['main']['domain']
        return registrar


    def proxy(self):
        """
        """
        proxy = self.data.get('proxy',None)
        if proxy:
            if not proxy.startswith("sip:"):
                proxy= "sip:" + proxy
        return proxy

    def user_profile(self):
        """

            return a dictionary of user info

                [Alice]
                LocationDialingCode: 11
                display: Alice
                domain: bluebox
                password: cocoon
                username: +33146502510

        :return:
        """
        user = dict(
            domain=self.data.get('domain',None),
            password=self.data.get('password',None),
            allias= self.data["alias"])

        user['agent_collection'] = self.data['collection']

        # set custom values
        for field in ['domain', 'password', 'number', 'target', 'virtual', 'profile', 'display']:
            if self.data.has_key(field):
                user[field] = self.data[field]

        user['display'] = self.data["alias"]

        # set full username
        if not '@' in self.data['username']:
            # handle simple username
            if user.get('domain',None):
                # full quaified username
                user['username'] = "%s@%s" % (self.data['username'], self.data['domain'])
            else:
                # simple username
                user['username'] = self.data['username']
        else:
            # handle fqdn username
            user['username'] = self.data['username']
            dummy, domain = user['username'].split('@', 1)
            user['domain'] = domain

        # set full id ( or public address )
        # handle inpu id public identity (id) , if not speficied default to username (private or inpi)
        user_id = self.data.get("id",None)
        if not user_id:
            # no explicit id
            if user['domain']:
                # complete with domain name
                if not '@' in user['username']:
                    user['id'] = user['username'] + "@" + user['domain']
                else:
                    # fqdn username: take it as id
                    user['id'] = user['username']

            elif self.data.has_key("registrar") and self.data['registrar']:
                # complete with registrar
                if not '@' in user['username']:
                    user['id'] = user['username'] + "@" + self.data['registrar']
                else:
                    # fqdn username: take it as id
                    user['id']= user['username']
            else:
                # no domain , no registrar : take username
                user['id'] = user['username']
        else:
            # explicit id
            if not '@' in self.data['id']:
                # make an fqdn id with domain or registrar
                dm = self.data.get("domain",None)
                if not dm:
                    dm = self.data.get("registrar",None)
                user['id'] = "%s@%s" % (self.data['id'], dm)
            else:
                # fqdn id given: keep it
                user['id'] = self.data['id']
        # check id begins with sip
        if not user['id'].startswith("sip:"):
            user['id']= "sip:" + user['id']

        # set sid
        site= self.site()
        if site:
            if site.data.has_key('LocationDialingCode'):
                user['LocationDialingCode'] = site.data['LocationDialingCode']
            else:
                assert site.data['name'] == 'virtual', "no LocationDialingCode, site name should be 'virtual' and is not"

        # add registrar and proxy
        if self.registrar():
            user['registrar'] = self.registrar()
        if self.proxy():
            user['proxy'] = self.proxy()
        return user

    def user_sda(self):
        """
            user has sda ?  check username is composed of [+0-9] and is longer tha 4 digits
        """
        is_number= True
        for indice,char in enumerate(self.id) :
            if not char in "0123456789":
                # not a digit
                if indice == 0 and char == "+":
                    # ok for + at first place
                    pass
                else:
                    is_number= False
                    break
        # we have a numeric
        return True
        # if is_number:
        #     if len(self.data["username"]) > 4 :
        #         return True
        # return False

    def user_contact(self):
        """

        """
        try:
            contact=self.data['contact']
        except KeyError:
            contact=self.id
        if contact.startswith('+'):
                contact=contact[1:]
        try:
            contact,domain=contact.split('@')
        except ValueError:
            # ValueError: need more than 1 value to unpack
            pass
        return contact

    def to_user(self,format_="universal"):
        """
            return the number to reach this user in a given format

            2525 , 122515 , +33146502515 , 033146502515

        """
        contact=self.user_contact()
        number= contact
        assert format_ in self.number_formats , "format '%s' is not on %s" %(format_,str(self.number_formats))
        #['international','escape_international','escape_national','sid_ext','ext','contact']
        if format_ == "contact" or format_=="c":
            return contact
        # is it a virtual number
        elif self.data.has_key('virtual'):
            # abbreviated number (the username)
            return self.username
        # has it sda or did
        elif self.user_sda() == False:
            # no sda user
            if format_=='contact' or format_=="c":
                return contact
            # test if short number is set for the use
            try:
                number=self.data['number']
            except KeyError:
                raise ValueError("no SDA for user %s" % contact)
            # sda with short number set
            if format_=="ext" or format_=="e":
                return number
            elif format_=="sid_ext" or format_=="s":
                return str(self.sid) + number
            else:
                raise ValueError("no SDA for user %s" % contact)
        # number format and sda OK
        phone_number = FrenchPhoneNumber.parse_from_sip(number)
        if format_=='universal' or format_=="u":
            #return self.data['username']
            #return "+" + contact
            return phone_number.to_universal()

        elif format_=='international' or format_=='i':
            #return contact
            return phone_number.to_international(self.escape_prefix)

        elif format_=="escape_international" or format_=='xi':
            #return self.escape + "00" + contact
            #return self.escape + phone_number.to_international(self.escape_prefix_explicit)
            return phone_number.to_international(self.escape_prefix_explicit)

        elif format_=="escape_national" or format_=="xn":
            #return self.escape + "0" + contact[-9:]
            #return self.escape + phone_number.to_national(self.escape_prefix_explicit)
            return phone_number.to_national(self.escape_prefix_explicit)

        elif format_ == 'national' or format_=='n':
            #return contact[-9:]
            return phone_number.to_national(self.escape_prefix)
        # elif format_== 'international':
        #     return contact
        elif format_.startswith("sid") or format_=="s":
            return str(self.sid) + contact[-4:]
        elif format_=="ext" or format_=='e':
            return contact[-4:]
        else:
            raise ValueError("number format not implemented: %s" % format_)

    def sip_address_to_user(self,format):
        """

        """
        number = self.to_user(format)
        return "sip:%s@%s" % (number,self.domain)

    @property
    def user_display(self):
        """
            return user display name
        """
        return self.data['display']

    @property
    def user_allias(self):
        """
            return user allias name
        """
        return self.data['allias']


    @property
    def escape_prefix(self):
        """
            return user allias name
        """
        return self.data.get("escape_prefix","")

    @property
    def escape_prefix_explicit(self):
        """
            return escape_prefix_explicit": "0"
        """
        return self.data.get("escape_prefix_ecplicit", "")

    @property
    def domain(self):
        """
            return: str domain  eg * blubox ,
        """
        try:
            # try explicit domain
            domain= self.data["domain"]
            assert domain
        except (KeyError,AssertionError):
            # no explicit domain
            try:
                # try registrar
                domain= self.data["registrar"]
                assert domain
            except (KeyError,AssertionError):
                # no registrar
                # cannot determine domain set it to local
                domain='local'
        return domain



class SypPlatform(Platform):
    """

        a platform configuration handler for syprunner


        add functions to handle phone destinations

    """

    collection= 'sypplatform'

    _hub_mode= None


    def user(self,alias):
        """
            get or create the syp user

        :param alias:
        :return:
        """
        try:
            user = SypUser.get( SypUser.name ==  alias)
        except ValueError as err:
            context = self.data.get('common', {})
            user = SypUser.new(alias, context)
        return user


    def user_profile(self, alias):
        """
            return a dictionary of users ( like platform.ini)

                [Alice]
                LocationDialingCode: 11
                display: Alice
                domain: bluebox
                password: cocoon
                username: +33146502510

        """
        user= self.user(alias=alias)
        #context= self.data.get('common',{})
        #user= SypUser.get_or_create(alias,context)

        return user.user_profile()

    @property
    def media_dir(self):
        """

        :return:
        """
        return self.data.get("media_dir",SYPRUNNER_MEDIA_DIR)


    def compute_pjterm_options(self, users):
        """

        :return:
        """
        # terminal_configurations=[]
        #
        # # common parameters for all agents ( codecs , play file
        # params = {}
        #
        # start_rtp_port= 20000
        # default_max_calls= 4
        #
        # device= 'default'
        # devices= {
        #     'default': {
        #
        #     },
        #     'ims': {
        #         '--no-tcp':'',
        #         '--auto-update-nat': 0,
        #         '--disable-via-rewrite':'',
        #         '--disable-rport':'',
        #         '--disable-stun':'',
        #         '--add-codec': 'PCMA',
        #         },
        # }

        # get default environment var (SYPRUNNER_*) , local_port,rtp_port, outbound, bound_addr,ip_addr,name_server
        defaults = get_env_defaults()

        terminals = []

        # create terminals with system,platform,device and account info
        indices = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for indice,alias in enumerate(users):
            # for each agent in the session
            context = self.data.get('common', {})
            user = SypUser.new(alias, context)
            #user = self.user(alias)

            # clone a basic terminal with system and platform info
            terminal = TerminalOptions().clone()

            # device options
            pjsip_options = user.data.get('options',{})
            if pjsip_options:
                terminal.set_device(pjsip_options)

            # platform options
            platform = {}


            registrar = user.registrar()
            proxy = user.proxy()

            # setup realm
            realm = user.data.get("realm",None)
            if realm is None:
                realm = user.data.get('domain', "*")

            if realm and realm != "*":
                platform['--realm'] = realm

            if registrar:
                # explicit registrar
                platform['--registrar'] = registrar
                if proxy:
                    platform["--proxy"] = proxy
                # if proxy.startswith("sip:"):
                #     platform['--proxy'] = proxy
                # else:
                #     platform['--proxy'] = "sip:%s" % proxy
            else:
                # non explicit registrar: use proxy as registar
                registrar = proxy
                if registrar:
                        platform['--registrar'] = registrar

            terminal.set_platform(platform)

            #  account options
            self.update_terminal_with_account(terminal, alias)

            #
            # session options
            # add customized rec-file  rec-user.wav, rec-user2.wav
            user_tag = 'user%s' % indices[indice]

            user_rec = 'rec-%s.wav' % user_tag
            terminal.set_session({'--rec-file': user_rec})

            # add customized play-file
            # media_dir= ptf.media_dir()
            #media_dir = self.data['media_dir']
            media_dir= self.media_dir

            sample_name = user_tag + ".wav"
            play_file = os.path.join(media_dir, sample_name)

            try:
                # check file exist
                os.stat(play_file)
                terminal.set_session({'--play-file': play_file})
            except OSError:
                print('WARNING cant open media file %s' % play_file)

            # handle terminals with no domain : no registrar, no realm, no username, no password
            #user_conf = users_conf[indice]
            #if user_conf['domain'] == 'local':
            domain= user.data.get("domain",None)
            if domain == "bluebox":
                del terminal.options['account']['--password']
                del terminal.options['account']['--username']
                del terminal.options['platform']['--password']
                del terminal.options['platform']['--realm']
                del terminal.options['platform']['--registrar']

            terminals.append(terminal)

        # update all terminals with session info
        local_port = defaults['local_port']
        rtp_port = defaults['rtp_port']
        self.update_terminal_with_session(terminals, local_port, rtp_port)

        # compute terminal options
        terminal_configurations = [" ".join(term.gen()) for term in terminals]

        # display terminal option
        for t in terminal_configurations:
            print(t)

        return terminal_configurations



    # def set_terminal_profile(self):
    #     """
    #         elaborate a base Terminal configuration (pjsip options) from platform.sjon
    #
    #         from
    #               "domain": "bluebox",
    #               "password": "1234",
    #               "profile": "default",
    #               "proxy": "192.168.1.50:5060",
    #
    #                   "username": "1000"
    #         towards:
    #
    #             --id sip:+33146511101@192.168.1.50
    #             --registrar sip:192.168.1.50:5060
    #             --realm 192.168.1.50
    #             --username +33146511101
    #             --password 1234
    #             --use-timer 1
    #
    #     :return:
    #     """
    #     self.terminal= TerminalOptions()
    #
    #     # device option
    #     device_profile= self.content['main'].get('device_profile',None)
    #     if device_profile:
    #         # a profile is specified
    #         device=self.device_section.get_device(device_profile)
    #     else:
    #         # use default device
    #         device= {}
    #     self.terminal.set_device(device)
    #
    #     # platform options
    #     platform = {
    #         "--realm":          self.content['main']['domain'],
    #         "--password":       self.content['main']['password'],
    #     }
    #
    #     #
    #     # interpret main section of platform.json to get platform config
    #     #
    #     registrar= self.content['main'].get('registrar',None)
    #     proxy= self.content['main'].get('proxy',None)
    #     realm= self.content['main'].get('domain',"*")
    #
    #     platform['--realm']= realm
    #
    #     if registrar:
    #         # explicit registrar
    #         platform['--registrar']= registrar
    #         if proxy.startswith("sip:"):
    #             platform['--proxy']= proxy
    #         else:
    #             platform['--proxy'] = "sip:%s" % proxy
    #     else:
    #         # non explicit registrar: use proxy as registar
    #         if proxy.startswith('sip:'):
    #             platform['--registrar'] = proxy
    #         else:
    #             platform['--registrar'] = "sip:%s" % proxy
    #
    #     self.terminal.set_platform(platform)

    def update_terminal_with_account(self, terminal, allias, configuration=None):
        """

        :param terminal:
        :param allias:
        :param configuration:
        :return:
        """

        # get agent info from platform.json
        agent = self.user_profile(allias)
        # agent= self._users._users[allias]

        # handle private identity
        #agent['id'] = agent.get('id', agent['username'])

        # modif for realm != registrar
        domain = terminal.options['platform'].get('--realm')
        #registrar = terminal.options['platform'].get('--registrar', None) or domain
        #if registrar.startswith('sip:'):
        #    registrar = registrar[4:]

        #agent_id = 'sip:%s@%s' % (agent['id'], registrar)
        #agent_username = '%s@%s' % (agent['username'], registrar)

        agent_id= agent["id"]
        agent_username= agent["username"]

        account = {
            '--id': agent_id,
            '--username': agent_username
        }

        # special bluebox domain
        if agent['domain'] == 'bluebox':
            # username does not include domain
            username, domain = agent['username'].split('@', 1)
            account['--username'] = username

        if agent.has_key('password'):
            account['--password'] = agent['password']

        # update account with custom configuration
        if configuration:
            assert isinstance(configuration, dict)
            account.update(configuration)

        # commit
        terminal.set_account(account)

        return terminal

    def get_terminal(self, alias, device=None, account=None):
        """
            obtain a terminal with system and platform info , optionaly with device and account info

        :param allias: str, agent name like Alice , Bob
        :param device: dict , config of the device eg:  { '--no-tcp':''}
        :param account: a dict of custom configuration  eg { '--max-calls': 2 }
        :return:
        """
        terminal= TerminalOptions().clone()


        # get a default terminal
        #terminal = self.terminal.clone()
        if device:
            terminal.set_device(device)
        if alias:
            if account is None:
                account = {}
            self.update_terminal_with_account(terminal, alias, account)
        return terminal

    def update_terminal_with_session(self, terminals, local_port=6000, rtp_port=20000):
        """

        """
        # local_port= 6000
        # rtp_port  = 20000

        # update terminals with session info ( local_port, rtp_port )
        for indice, terminal in enumerate(terminals):
            # set session
            session = {
                '--local-port': local_port + 1,
                '--rtp-port': rtp_port,
            }
            terminal.set_session(session)

            # get terminal options
            terminal_options = terminal.get_options()

            # update session parameters
            local_port = local_port + 1
            rtp_port = rtp_port + (int(terminal_options['--max-calls']) * 2)

        return

    def fac(self, name):
        """
            return feature access code

        """
        fac = FeatureAccessCode.get(FeatureAccessCode.name==name)
        return fac.code

    def auto_resolve(self,tag,content,step=None,sip=False):
        """
            resolve auto_?
            return to? parameter

            @tag: char eg: 1 A D
            @content: str  eg  +CFA,Alice:ext
            @step: dict  d={ userA: { role: ,auto: } ,userB: {..}}
            @sip: boolean : return number in sip format : sip:number@domain

            2 kind of auto_

            auto_1, ...auto_2:    auto destination
            auto_B , auto_C : auto user

        """
        auto_destination=content
        found_user= False

        # auto destination  +CFA,Bob:ext,%23  , wrongSid_E1S1 , Bob:universal , Bob:universal,%23
        sequence=[]
        parts=auto_destination.split(',')
        if auto_destination[0] in '+-?':
            # it is a fac
            fac_name=parts.pop(0)
            sequence.append(self.feature_access_code(fac_name))
        if len(parts):
            # next is a user or a destination  eg Bob:ext , wrong_Extension
            user_destination=parts.pop(0)

            # see if  auto_B case ( instead of auto_1 )
            if tag in 'ABCDE':
                # it is : resolve auto target role
                #destination_role= step['user%s'% tag]['role']
                destination_role= self.role_from_step(step,tag)
                # content can be  format:  sid_ext or format+ literal:  sid_ext,%23
                user_destination= destination_role + ':' + user_destination   # = Bob:Ext

            # user_destination is now a complete destination : role:format eg Bob:ext
            user_parts=user_destination.split(':')
            allias=user_parts[0]
            # search for user
            found_user=False
            try:
                target_user=self.user(allias)
                found_user=True
            except KeyError:
                # not a user , try a destination
                try:
                    target_user=self.destination(allias)
                except KeyError:
                    raise KeyError('not a valid user or destination: %s' % allias)

            if len(user_parts)>=2:
                # there is a format
                format_=user_parts[1].lower()  #  Bob:ext
            else:
                # no format set it to default
                format_="contact"
            if found_user:
                # it is a user
                user_number=target_user.to_user(format_)
                #if sip:
                #    if not '@' in user_number:
                #        # ADD USER DOMAIN
                #        user_number += "@" + target_user.data['domain']
                sequence.append(str(user_number))
            else:
                # it is a destination
                #if sip:
                #    if not '@' in target_user:
                #        # add default platform domain
                #        target_user += "@" + self.cp.get("platform","domain")
                sequence.append(str(target_user))

        if len(parts):
            # third part of auto destination
            # there is a literal , add it
            sequence.append(str(parts[0]))

        number="".join(sequence)

        if sip:
            # ask for sip format
            if found_user:
                if not '@' in number:
                    # ADD USER DOMAIN
                    number +=  "@" + target_user.data['domain']
            else:
                if not '@' in number:
                    # ADD platform DOMAIN
                    number += "@" + self.cp.get("platform","domain")
            number = "sip:" + number

        return number

    def resolve_address(self, user=None, fac=None, format=None, destination=None, to_sip=True):

        result = ""
        if not fac:
            # a simple user or destination
            if not user:
                # a destination
                result = self.destination(destination)
            else:
                if not format:
                    format = 'contact'
                # a simple user destination with format
                result = self.user(user).sip_address_to_user(format)
        else:
            # fac is not None
            if not user:
                # a fac alone or destination+ fac
                if not destination:
                    # a fac alone
                    result = self.fac(fac)
                else:
                    # a fac + destination
                    result = self.fac(fac) + self.destination(destination)
            else:
                # a user + fac
                result = self.fac(fac) + self.user(user).to_user(format)

        if not result:
            raise ValueError("cannot resolve destination")

        if to_sip and not result.startswith("sip:"):
            result= "sip:" + result

        return result


    def destination(self,name):
        """
            return a destination number  eg 122515 for wrong_Sid_E1S2
        """
        value=None
        try:
            destination= Tag.get(Tag.name==name)
            value= destination.value
        except :
            value="?"
        return value




class Syprunner_Agents(AgentModel):
    """


    """
    collection= DEVICE_NAME


    escape = "0"
    number_formats = ['international', 'national', 'escape_international', 'escape_national', 'sid_ext', 'ext',
                      'contact', 'universal']

    def setup(self):
        """

        :return:
        """
        pass

    def make_response(self,message,status=200,log_offset= 0):
        """

        :param status:
        :param message:
        :param logs:
        :return:
        """

        log_offset= int(log_offset) or  self.log_offset
        logs=self.stdlog[log_offset:]
        message= { 'result': int(status), 'message': message, 'logs': logs }
        self.log_offset= len(list(self.stdlog))
        self.save()
        return message



    @classmethod
    def get_or_create(cls,alias,context=None):
        """

        :return:
        """
        context= context or {}
        try:
            user= cls.get(cls.name == alias)
        except ValueError:
            # no user create it
            device  = Device.get(Device.name == alias)
            profile_id = device.profile
            profile= Profile.get(Profile.name == profile_id)

            # take base from context
            data = context.copy()

            # update with profile info
            data.update(profile.data)
            # update with
            data["alias"]= device.name
            data.update(device.data)
            #data['collection']= profile.collection

            user= cls(name=alias,data=data)
            user.save()

        return user

    @property
    def username(self):
        return self.data["username"]


    def site(self):
        """


        :return: return the Site object of the user or None
        """
        site = None
        site_id = self.data.get("site",None)
        if site_id:
            try:
                site = Site.get( Site.name == site_id)
            except ValueError:
                pass
        return site


    def registrar(self):
        """
        """
        registrar= self.data.get("registrar",None)
        if not registrar:
            registrar= self.data.get("domain", None)
        if registrar:
            if not registrar.startswith("sip:"):
                registrar= "sip:" + registrar
        # if self.data.has_key('registrar'):
        #     registrar = self.content['main']['registrar']
        # else:
        #     registrar = self.content['main']['domain']
        return registrar


    def proxy(self):
        """
        """
        proxy = self.data.get('proxy',None)
        if proxy:
            if not proxy.startswith("sip:"):
                proxy= "sip:" + proxy
        return proxy

    def user_profile(self):
        """

            return a dictionary of user info

                [Alice]
                LocationDialingCode: 11
                display: Alice
                domain: bluebox
                password: cocoon
                username: +33146502510

        :return:
        """
        user = dict(
            domain=self.data['domain'],
            password=self.data['password'],
            allias= self.data["alias"])

        user['agent_collection'] = self.data['collection']

        # set custom values
        for field in ['domain', 'password', 'number', 'target', 'virtual', 'profile', 'display']:
            if self.data.has_key(field):
                user[field] = self.data[field]

        user['display'] = self.data["alias"]

        # set full username
        if not '@' in self.data['username']:
            # complete with
            user['username'] = "%s@%s" % (self.data['username'], self.data['domain'])
        else:
            user['username'] = self.data['username']
            dummy, domain = user['username'].split('@', 1)
            user['domain'] = domain

        # set full id ( or public address )
        # handle inpu id public identity (id) , if not speficied default to username (private or inpi)
        user_id = self.data.get("id",None)
        if not user_id:
            # no explicit id: take the username
            user['id'] = user['username']
        else:
            # explicit id
            if not '@' in self.data['id']:
                # complete id with domain
                user['id'] = "%s@%s" % (self.data['id'], self.data['domain'])
            else:
                user['id'] = self.data['id']

        # set sid
        site= self.site()
        if site:
            if site.data.has_key('LocationDialingCode'):
                user['LocationDialingCode'] = site.data['LocationDialingCode']
            else:
                assert site.data['name'] == 'virtual', "no LocationDialingCode, site name should be 'virtual' and is not"

        # add registrar and proxy
        if self.registrar():
            user['registrar'] = self.registrar()
        if self.proxy():
            user['proxy'] = self.proxy()
        return user

    def user_sda(self):
        """
            user has sda ?
        """
        #if self.username.startswith('+'):
        if self.data["username"][0] in "+0123456789":
            sda=True
        else:
            sda=False
        return sda

    def user_contact(self):
        """

        """
        try:
            contact=self.data['contact']
        except KeyError:
            contact=self.data["username"]
        if contact.startswith('+'):
                contact=contact[1:]
        try:
            contact,domain=contact.split('@')
        except ValueError:
            # ValueError: need more than 1 value to unpack
            pass
        return contact

    def to_user(self,format_="universal"):
        """
            return the number to reach this user in a given format

            2525 , 122515 , +33146502515 , 033146502515

        """
        contact=self.contact
        assert format_ in self.number_formats , "format '%s' is not on %s" %(format_,str(self.number_formats))
        #['international','escape_international','escape_national','sid_ext','ext','contact']
        if format_ == "contact" or format_=="c":
            return contact
        # is it a virtual number
        elif self.data.has_key('virtual'):
            # abbreviated number (the username)
            return self.username
        # has it sda or did
        elif self.sda == False:
            # no sda user
            if format_=='contact' or format_=="c":
                return contact
            # test if short number is set for the use
            try:
                number=self.data['number']
            except KeyError:
                raise ValueError("no SDA for user %s" % contact)
            # sda with short number set
            if format_=="ext" or format_=="e":
                return number
            elif format_=="sid_ext" or format_=="s":
                return str(self.sid) + number
            else:
                raise ValueError("no SDA for user %s" % contact)
        # number format and sda OK
        phone_number = FrenchPhoneNumber.parse_from_sip(self.username)
        if format_=='universal' or format_=="u":
            #return self.data['username']
            #return "+" + contact
            return phone_number.to_universal()

        elif format_=='international' or format_=='i':
            #return contact
            return phone_number.to_international(self.escape_prefix)

        elif format_=="escape_international" or format_=='xi':
            #return self.escape + "00" + contact
            #return self.escape + phone_number.to_international(self.escape_prefix_explicit)
            return phone_number.to_international(self.escape_prefix_explicit)

        elif format_=="escape_national" or format_=="xn":
            #return self.escape + "0" + contact[-9:]
            #return self.escape + phone_number.to_national(self.escape_prefix_explicit)
            return phone_number.to_national(self.escape_prefix_explicit)

        elif format_ == 'national' or format_=='n':
            #return contact[-9:]
            return phone_number.to_national(self.escape_prefix)
        # elif format_== 'international':
        #     return contact
        elif format_.startswith("sid") or format_=="s":
            return str(self.sid) + contact[-4:]
        elif format_=="ext" or format_=='e':
            return contact[-4:]
        else:
            raise ValueError("number format not implemented: %s" % format_)

    def sip_address_to_user(self,format):
        """

        """
        number = self.to_user(format)
        return "sip:%s@%s" % (number,self.data['domain'])

    @property
    def user_display(self):
        """
            return user display name
        """
        return self.data['display']

    @property
    def user_allias(self):
        """
            return user allias name
        """
        return self.data['allias']
