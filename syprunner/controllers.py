"""

    The link between the model and the pjterm manager




"""

from syprunner.models import SypPlatform as Platform
from syprunner.models import SypUser
#from syprunner.models import Syprunner_Agents

from pjsip_device.api.full import PjtermLevelOne as Agent
#from agent import NewAgent


from pjsip_device.session import PjtermSession,PjtermAdapter


from syprunner.syprunner_device.terminal import PjtermLevelDb


import logging
log = logging.getLogger(__name__)



class SessionController(object):
    """

        open(members)    POST restop/api/v1/sessions
        close()

        start_terminal


    """
    def __init__(self,platform_name="default",root="pjterm"):
        """

        :param cnx:
        :param platform_name:
        """
        self.platform_name= platform_name
        self.root= root
        self.ptf = Platform.get(Platform.name == self.platform_name)

    def open(self,members):
        """

        :param members: list of session members ( eg [fanny,Forest]
        :return:
        """

        #ptf = Platform.get(Platform.name == self.platform_name)
        options = self.ptf.compute_pjterm_options(members)
        for index, alias in enumerate(members):
            # configuration[alias]["command_line"] = options[index]
            user = self.ptf.user(alias)
            user.data["command_line"] = options[index]
            user.save()
            # user.delete()

        # start a new pjterm session
        s = PjtermSession(cnx=self.ptf.database)
        s.open_session(members=" ".join(members))
        return options


    def close(self):
        """

        :return:
        """
        #ptf = Platform.get(Platform.name == self.platform_name)
        s = PjtermSession(self.ptf.database)
        s.close_session()


    def start_terminal(self,alias):
        """
        :param alias: string eg "Fanny"
        :return:
        """
        terminal= self.terminal(alias)
        options = terminal.model.data['command_line']

        result= terminal._start(options=options)

        logs = terminal.adapter.read_log(0)
        logs.append("**** agent started: %s ***" % alias)
        terminal.model.log_offset = terminal.adapter.log_len()
        terminal.model.save()

        if result is None:
            response = dict(result=500, logs=logs, message=result)
        else:
            response = dict(result=200, logs=logs, message=result)


        # cnx = self.ptf.database
        #
        # try:
        #     agent = SypUser.get(SypUser.name == alias)
        #     command_line = agent.data['command_line']
        # except ValueError as e:
        #     # not a member of session
        #     raise RuntimeError("no syp agent %s in this session " % alias)
        # except KeyError as e:
        #     raise RuntimeError("no command line for syp agent %s" % alias)
        #
        # session = PjtermSession(cnx)
        # result = session.start_terminal(alias, options=command_line)
        # # self.started = result
        # # time.sleep(0.1)
        #
        # # get logs
        # terminal = session.get_terminal(alias)
        # logs = terminal.adapter.read_log(0)
        # logs.append("**** agent started: %s ***" % alias)
        # agent.log_offset = terminal.adapter.log_len()
        # agent.save()
        # response = dict(result=200, logs=logs, message= result)

        return response

    def terminal(self,alias):
        """

        :return:  a NewAgent Terminal
        """
        terminal = PjtermLevelDb(alias)
        return terminal

    def terminal_controller(self, alias):
        """
                return a Terminalcontroler for alias ( aline, bob )
        :param alias:
        :return:
        """
        t = TerminalController(self,alias)
        return t



class TerminalController(object):
    """


    """

    def __init__(self,session,alias):
        """
        :param session:  instance of sessionController
        :param alias:
        """
        self.session = session
        self.alias=alias
        db = self.session.ptf.database

        self.terminal= session.terminal(alias)
        self.agent_model = self.terminal.model

        # # set agent,   agent is the db model
        # try:
        #     self.agent_model = SypUser.get(SypUser.name == alias)
        # except ValueError as e:
        #     # not a member of session
        #     raise RuntimeError("no such syp agent: [%s] in this session " % alias)
        #
        # # set terminal  ( communicate with device )
        # session = PjtermSession(db)
        # self.terminal = session.get_terminal(alias)

        return


    def _make_response(self, result=200, message=True, logs=None):
        """

            return a dict representing a response with result,message and logs (from pjterm:$:stdlog)

        :param result:
        :param message:
        :return:
        """
        last = self.agent_model.log_offset
        pjterm_logs = self.terminal.adapter.read_log(last)
        self.agent_model.log_offset = self.terminal.adapter.log_len()
        self.agent_model.save()

        response = dict(result=result, message=message ,logs=[])

        if pjterm_logs:
            response['logs'].extend(pjterm_logs)
        if logs:
            response['logs'].extend(logs)
        return response


    def run(self,operation,**parameters):
        """

        :param alias: string eg alice,bob
        :param operation: string , eg call ,wait_incoming_call
        :param parameters:
        :return:
        """

        ## check operation exists on terminal
        try:
            func= getattr(self.terminal,operation)
        except AttributeError:
            # operation does not exists on interface
            raise RuntimeError("invalid operation [%s] on item [%s]" % (operation,alias) , 404 )
        try:
            if parameters:
                rc = func(**parameters)
            else:
                rc = func()
            # return rc
            response = self._make_response(result=200, message=rc)

        except Exception as error:
            # error executing method
            response = self._make_response(result=500, message=str(repr(error)))
            response["logs"].append("error: %s" % error)


        return response





